#! /usr/bin/python

from paddlenlp import Taskflow
import socket
import json

if __name__ == '__main__':
    similarity_function = Taskflow('text_similarity')

    server_addr = ('127.0.0.1', 8000)
    
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.bind(server_addr)
    sock.listen(5)
    while True:
        try:
            conn, client_addr = sock.accept()

            # data format: [{'text1': 'xxx', 'text2': 'yyy'}, {}, ...]
            buf_size = 4096
            recv_buf = b''
            while True:
                data = conn.recv(buf_size)
                if len(data) > 0:
                    recv_buf += data
                # TODO 注意：如果接收数据的大小刚好是buf_size的倍数，会出问题，暂不作处理
                if len(data) < buf_size:
                    break
            recv_buf = recv_buf.decode('UTF-8')
            
            recv_data = json.loads(recv_buf)
            requests = []
            for request in recv_data:
                text1 = request['text1']
                text2 = request['text2']
                requests.append([text1, text2])

            result = similarity_function(requests)
            for idx in range(len(result)):
                result[idx]['similarity'] = float(result[idx]['similarity'])

            send_data = bytes(json.dumps(result), 'UTF-8')
            conn.sendall(send_data)
        finally:
            conn.close()