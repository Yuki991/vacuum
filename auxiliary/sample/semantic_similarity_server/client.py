#! /usr/bin/python

import socket
import json

if __name__ == '__main__':
    addr = ('127.0.0.1', 8000)
    
    data = [
        {'text1': '冬天', 'text2': '冬日'},
        {'text1': '冬天', 'text2': '夏日'},
    ]
    send_data = bytes(json.dumps(data), 'UTF-8')

    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect(addr)

    sock.sendall(send_data)
    recv_data = json.loads(sock.recv(1024).decode('UTF-8'))

    sock.close()

    print("Sent: {}".format(send_data))
    print("Received: {}".format(recv_data))