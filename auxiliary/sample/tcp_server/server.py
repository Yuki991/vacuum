#! /usr/bin/python

import socket
import json

if __name__ == '__main__':
    server_addr = ('127.0.0.1', 8000)
    
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.bind(server_addr)
    sock.listen(5)
    while True:
        try:
            conn, client_addr = sock.accept()
            recv_buf = conn.recv(1024).decode('UTF-8')
            recv_data = json.loads(recv_buf)
            for key in recv_data:
                print("key: {}, value: {}".format(key, recv_data[key]))
            
            data = {'response': 'hello world'}
            send_data = bytes(json.dumps(data), 'UTF-8')
            conn.sendall(send_data)
        finally:
            conn.close()