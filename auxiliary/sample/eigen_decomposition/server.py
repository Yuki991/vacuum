#! /usr/bin/python

import numpy as np
import socket
import json

if __name__ == '__main__':
    server_addr = ('127.0.0.1', 8001)
    
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.bind(server_addr)
    sock.listen(5)
    while True:
        try:
            conn, client_addr = sock.accept()

            # data format: [[...], ..., [...]]
            buf_size = 4096
            recv_buf = b''
            while True:
                data = conn.recv(buf_size)
                if len(data) > 0:
                    recv_buf += data
                # TODO 注意：如果接收数据的大小刚好是buf_size的倍数，会出问题，暂不作处理
                if len(data) < buf_size:
                    break
            print(recv_buf)
            recv_buf = recv_buf.decode('UTF-8')
            
            recv_data = json.loads(recv_buf)
            k = recv_data["number"]
            # 将list转为np.array
            arr = np.array(recv_data["array"])
            # 求特征值
            vals, vecs = np.linalg.eig(arr)
            # 从小到大排序
            idx = np.argsort(vals)
            vals = vals[idx]
            # 每一列是相应的特征向量
            vecs = vecs[:,idx]

            result = {}
            result["values"] = vals[0:k].tolist()
            result["vectors"] = vecs[:,0:k].tolist()

            send_data = bytes(json.dumps(result), 'UTF-8')
            conn.sendall(send_data)

            print(vals)
        finally:
            conn.close()