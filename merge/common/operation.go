package common

import "iomgm/graph"

type OpertionType uint8

const (
	OpAddEntity OpertionType = iota
	OpAddRelation
	OpDeleteEntity
	OpDeleteRelation
	OpModifyAttr
)

type AddEntityArgs struct {
	Gid   graph.GraphID
	Attrs map[string]string
	Name  string
	Type  string
}

type AddRelationArgs struct {
	Gid   graph.GraphID
	Nids  []graph.NodeID
	Roles []string
	Attrs map[string]string
	Name  string
	Type  string
}
