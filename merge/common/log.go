package common

import (
	"bufio"
	"fmt"
	"os"
	"path/filepath"
)

var f *os.File
var w *bufio.Writer

func init() {
	var err error
	f, err = os.Create(filepath.FromSlash("./logs.txt"))
	if err != nil {
		panic("create log file error")
	}
	w = bufio.NewWriter(f)
}

func Log(text string) {
	w.WriteString(fmt.Sprintf("%v\n", text))
	w.Flush()
}
