package bpmatch

import (
	"math"
)

// Kuhn-Munkres Algorithm

type KuhnMunkrasAlgo struct {
	// bp *Bipartite
	scores [][]float64
	n      int
	m      int

	linky []int
	lack  float64
	lx    []float64
	ly    []float64
	visx  []bool
	visy  []bool
	rev   bool // reverse n and m if n > m
}

func CreateKuhnMunkrasAlgo() *KuhnMunkrasAlgo {
	return &KuhnMunkrasAlgo{}
}

func (algo *KuhnMunkrasAlgo) Solve(bp Bipartite) ([][2]int, []float64, error) {
	if err := algo.initialize(bp); err != nil {
		return nil, nil, err
	}
	return algo.solve()
}

func (a *KuhnMunkrasAlgo) initialize(bp Bipartite) error {
	var err error
	a.n, a.m, a.scores, err = bp.GetAllSimilarity()
	if err != nil {
		return err
	}
	if a.n > a.m {
		a.rev = true
		scores := a.scores
		a.n, a.m = a.m, a.n
		a.scores = make([][]float64, a.n)
		for i := range a.scores {
			a.scores[i] = make([]float64, a.m)
			for j := range a.scores[i] {
				a.scores[i][j] = scores[j][i]
			}
		}
	} else {
		a.rev = false
	}

	a.linky = make([]int, a.m)
	a.lx = make([]float64, a.n)
	a.ly = make([]float64, a.m)
	a.visx = make([]bool, a.n)
	a.visy = make([]bool, a.m)
	a.lack = 0
	return nil
}

func (a *KuhnMunkrasAlgo) find(x int) bool {
	a.visx[x] = true
	for y := 0; y < a.m; y++ {
		if a.visy[y] {
			continue
		}

		t := a.lx[x] + a.ly[y] - a.scores[x][y]
		if math.Abs(t) < 1e-5 {
			a.visy[y] = true
			if a.linky[y] == -1 || a.find(a.linky[y]) {
				a.linky[y] = x
				return true
			}
		} else {
			a.lack = math.Min(a.lack, t)
		}
	}
	return false
}

func (a *KuhnMunkrasAlgo) solve() ([][2]int, []float64, error) {
	for i := 0; i < a.m; i++ {
		a.linky[i] = -1
		a.ly[i] = 0
	}
	for i := 0; i < a.n; i++ {
		a.lx[i] = -1e2
		for j := 0; j < a.m; j++ {
			a.lx[i] = math.Max(a.lx[i], a.scores[i][j])
		}
	}

	for x := 0; x < a.n; x++ {
		for {
			for i := 0; i < a.n; i++ {
				a.visx[i] = false
			}
			for i := 0; i < a.m; i++ {
				a.visy[i] = false
			}

			a.lack = 1e9
			if a.find(x) {
				break
			}

			for i := 0; i < a.n; i++ {
				if a.visx[i] {
					a.lx[i] -= a.lack
				}
			}
			for i := 0; i < a.m; i++ {
				if a.visy[i] {
					a.ly[i] += a.lack
				}
			}
		}
	}

	result := make([][2]int, 0)
	scores := make([]float64, 0)
	for i := 0; i < a.m; i++ {
		if a.linky[i] != -1 {
			result = append(result, [2]int{a.linky[i], i})
			scores = append(scores, a.scores[a.linky[i]][i])
		}
	}
	if a.rev {
		// 需要将结果反转
		for i := range result {
			result[i][0], result[i][1] = result[i][1], result[i][0]
		}
	}
	return result, scores, nil
}
