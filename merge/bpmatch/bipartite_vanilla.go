package bpmatch

import "fmt"

type VanillaBipartite struct {
	n, m int
	sims [][]float64 // similarity
}

// @Description: new VanillaBipartite，sims浅copy
func NewVanillaBipartite(n int, m int, sims [][]float64) *VanillaBipartite {
	return &VanillaBipartite{
		n:    n,
		m:    m,
		sims: sims,
	}
}

func (vb *VanillaBipartite) GetStatus() (int, int) {
	return vb.n, vb.m
}

func (vb *VanillaBipartite) GetSimilarity(i int, j int) (float64, error) {
	if i >= vb.n || j >= vb.m {
		return 0, fmt.Errorf("index %v, %v exceed %v, %v", i, j, vb.n, vb.m)
	}

	return vb.sims[i][j], nil
}

func (vb *VanillaBipartite) GetAllSimilarity() (int, int, [][]float64, error) {
	return vb.n, vb.m, vb.sims, nil
}
