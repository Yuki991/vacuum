package bpmatch

import "sort"

// 贪心法，根据scores从大到小排序，依次判断相应匹配是否可行（有无已有匹配）
type GreedMatchingAlgo struct {
	scores    [][]float64
	n         int
	m         int
	threshold float64
}

func CreateGreedMatchingAlgo() *GreedMatchingAlgo {
	return &GreedMatchingAlgo{}
}

// @Param threshold: 忽略score低于threshold的匹配
func (algo *GreedMatchingAlgo) Solve(bp Bipartite, threshold float64) ([][2]int, []float64, error) {
	algo.initialize(bp, threshold)
	return algo.solve()
}

func (algo *GreedMatchingAlgo) initialize(bp Bipartite, threshold float64) {
	algo.n, algo.m, algo.scores, _ = bp.GetAllSimilarity()
	algo.threshold = threshold
}

func (algo *GreedMatchingAlgo) solve() ([][2]int, []float64, error) {
	type TmpType struct {
		x, y int
		val  float64
	}

	arr := make([]TmpType, 0, algo.n*algo.m)
	for i := range algo.scores {
		for j := range algo.scores[i] {
			if algo.scores[i][j] >= algo.threshold {
				arr = append(arr, TmpType{i, j, algo.scores[i][j]})
			}
		}
	}

	sort.Slice(arr, func(i, j int) bool {
		return arr[i].val > arr[j].val
	})

	mp1 := make([]bool, algo.n)
	mp2 := make([]bool, algo.m)
	result := make([][2]int, 0)
	scores := make([]float64, 0)
	for i := range arr {
		if !mp1[arr[i].x] && !mp2[arr[i].y] {
			result = append(result, [2]int{arr[i].x, arr[i].y})
			scores = append(scores, arr[i].val)
			mp1[arr[i].x] = true
			mp2[arr[i].y] = true
		}
	}

	return result, scores, nil
}
