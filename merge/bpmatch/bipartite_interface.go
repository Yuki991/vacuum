package bpmatch

type Bipartite interface {
	GetStatus() (n int, m int)
	GetAllSimilarity() (n int, m int, sims [][]float64, err error)
}
