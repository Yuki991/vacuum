package bpmatch

type BipartiteMatchSolver interface {
	Solve(Bipartite) (results [][2]int, scores []float64, err error)
}
