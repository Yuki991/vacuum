package bpmatch

import (
	"iomgm/graph"
	"iomgm/utils"
	"math"
)

// 只对齐entity node，relation node不参与对齐
type TwoGraphBipartite struct {
	G1 *graph.Graph
	G2 *graph.Graph

	EntityNodeNum1 int
	EntityNodeNum2 int
	Nids1          []graph.NodeID
	Nids2          []graph.NodeID
}

func NewTwoGraphBipartite(g1 *graph.Graph, g2 *graph.Graph) *TwoGraphBipartite {
	entityNodeNum1 := 0
	for _, node := range g1.Nodes {
		if node.Label == graph.EntityNode {
			entityNodeNum1++
		}
	}
	entityNodeNum2 := 0
	for _, node := range g2.Nodes {
		if node.Label == graph.EntityNode {
			entityNodeNum2++
		}
	}

	return &TwoGraphBipartite{
		G1:             g1,
		G2:             g2,
		EntityNodeNum1: entityNodeNum1,
		EntityNodeNum2: entityNodeNum2,
		Nids1:          nil,
		Nids2:          nil,
	}
}

func (bp *TwoGraphBipartite) GetStatus() (int, int) {
	return bp.EntityNodeNum1, bp.EntityNodeNum2
}

func (bp *TwoGraphBipartite) GetSimilarityFromNodeID(nid1 graph.NodeID, nid2 graph.NodeID) (float64, error) {
	node1, node2 := bp.G1.Nodes[nid1], bp.G2.Nodes[nid2]
	if node1.Label != node2.Label {
		return 0, nil
	}

	switch node1.Label {
	case graph.EntityNode:
		return bp.getEntityNodeSimilarity(nid1, nid2)
	case graph.RelationNode:
		return bp.getRelationNodeSimilarity(nid1, nid2)
	case graph.AttrNode:
		return bp.getAttrNodeSimilarity(nid1, nid2)
	}
	return 0, nil
}

func (bp *TwoGraphBipartite) GetAllSimilarity() (int, int, [][]float64, error) {
	if bp.Nids1 == nil || bp.Nids2 == nil {
		bp.Nids1 = make([]graph.NodeID, 0, len(bp.G1.Nids))
		bp.Nids2 = make([]graph.NodeID, 0, len(bp.G2.Nids))
		for nid, node := range bp.G1.Nodes {
			if node.Label == graph.EntityNode {
				bp.Nids1 = append(bp.Nids1, nid)
			}
		}
		for nid, node := range bp.G2.Nodes {
			if node.Label == graph.EntityNode {
				bp.Nids2 = append(bp.Nids2, nid)
			}
		}
	}

	// 直接调用GetSimilarity，每次都需要重新计算，后面看要不要改一下
	n, m := bp.GetStatus()
	sims := make([][]float64, n)
	for i := 0; i < n; i++ {
		sims[i] = make([]float64, m)
		for j := 0; j < m; j++ {
			sims[i][j], _ = bp.GetSimilarityFromNodeID(bp.Nids1[i], bp.Nids2[j])
		}
	}
	return n, m, sims, nil
}

func (bp *TwoGraphBipartite) TranslateIndexToNodeID(match [][2]int) [][2]graph.NodeID {
	result := make([][2]graph.NodeID, len(match))
	for i := range match {
		result[i] = [2]graph.NodeID{
			bp.Nids1[match[i][0]],
			bp.Nids2[match[i][1]],
		}
	}
	return result
}

// @Description: （综合attribute和邻居信息）计算EntityNode相似度
func (bp *TwoGraphBipartite) getEntityNodeSimilarity(nid1, nid2 graph.NodeID) (float64, error) {
	node1, node2 := bp.G1.Nodes[nid1], bp.G2.Nodes[nid2]
	if node1.Type != node2.Type {
		return 0, nil
	}

	sim1, err := bp.GetNodeAttrSimilarity(nid1, nid2)
	if err != nil {
		return 0, err
	}

	sim2, err := bp.getEntityContextSimilarity(nid1, nid2)
	if err != nil {
		return 0, err
	}

	alpha := 0.9
	// alpha := 1.0
	sim := alpha*sim1 + (1-alpha)*sim2
	return sim, err
}

// @Description: 计算RelationNode相似度
func (bp *TwoGraphBipartite) getRelationNodeSimilarity(nid1, nid2 graph.NodeID) (float64, error) {
	// RelationNode只有out edge，并且目标节点为EntityNode
	// Type不一样为0，否则根据相关节点匹配情况计算

	node1, node2 := bp.G1.Nodes[nid1], bp.G2.Nodes[nid2]
	if node1.Type != node2.Type {
		return 0, nil
	}

	outEdge1, outEdge2 := bp.G1.OutEdge[nid1], bp.G2.OutEdge[nid2]
	n, m := len(*outEdge1), len(*outEdge2)
	neighbor1, neighbor2 := make([]graph.NodeID, 0, n), make([]graph.NodeID, 0, m)
	for eid := range *outEdge1 {
		neighbor1 = append(neighbor1, bp.G1.Edges[eid].Target())
	}
	for eid := range *outEdge2 {
		neighbor2 = append(neighbor2, bp.G2.Edges[eid].Target())
	}

	sims := make([][]float64, n)
	for i := 0; i < n; i++ {
		sims[i] = make([]float64, m)
		for j := 0; j < m; j++ {
			sims[i][j], _ = bp.GetNodeAttrSimilarity(neighbor1[i], neighbor2[j])
		}
	}

	vb := NewVanillaBipartite(n, m, sims)
	algo := CreateKuhnMunkrasAlgo()
	_, scores, _ := algo.Solve(vb)
	var sim float64 = 0
	for _, score := range scores {
		sim += score
	}
	sim = sim / math.Min(float64(n), float64(m))
	return sim, nil
}

// @Description: 计算AttrNode相似度
func (bp *TwoGraphBipartite) getAttrNodeSimilarity(nid1, nid2 graph.NodeID) (float64, error) {
	// 没有这种类型的节点，可以忽略这个函数
	return 0, nil
}

// @Description: 根据attribute计算EntityNode/RelationNode相似度
func (bp *TwoGraphBipartite) GetNodeAttrSimilarity(nid1, nid2 graph.NodeID) (float64, error) {
	// TODO 目前只计算name的相似度
	node1, node2 := bp.G1.Nodes[nid1], bp.G2.Nodes[nid2]
	if node1.Label != node2.Label {
		return 0, nil
	}

	switch node1.Label {
	case graph.EntityNode:
		sim, err := utils.GetSemanticSimilarity(node1.Name, node2.Name)
		return sim, err
	case graph.RelationNode:
		if node1.Type == node2.Type {
			return 1, nil
		} else {
			return 0, nil
		}
	}
	return 0, nil
}

// @Description: 根据邻居信息计算EntityNode相似度
func (bp *TwoGraphBipartite) getEntityContextSimilarity(nid1, nid2 graph.NodeID) (float64, error) {
	// EntityNode只有in edge，并且源节点为RelationNode

	inEdge1, inEdge2 := bp.G1.InEdge[nid1], bp.G2.InEdge[nid2]
	n, m := len(*inEdge1), len(*inEdge2)
	if n == 0 || m == 0 {
		// 如果其中一个节点没有邻居，该相似度为0
		return 0, nil
	}

	neighbor1, neighbor2 := make([]graph.NodeID, 0, n), make([]graph.NodeID, 0, m)
	for eid := range *inEdge1 {
		neighbor1 = append(neighbor1, bp.G1.Edges[eid].Source())
	}
	for eid := range *inEdge2 {
		neighbor2 = append(neighbor2, bp.G2.Edges[eid].Source())
	}

	sims := make([][]float64, n)
	for i := 0; i < n; i++ {
		sims[i] = make([]float64, m)
		for j := 0; j < m; j++ {
			sims[i][j], _ = bp.getRelationNodeSimilarity(neighbor1[i], neighbor2[j])
		}
	}

	vb := NewVanillaBipartite(n, m, sims)
	algo := CreateKuhnMunkrasAlgo()
	_, scores, _ := algo.Solve(vb)
	var sim float64 = 0
	for _, score := range scores {
		sim += score
	}
	sim = sim / math.Min(float64(n), float64(m))
	return sim, nil
}
