package graph

import "github.com/google/uuid"

type Edge struct {
	Eid  EdgeID
	Nids [2]NodeID
	Name string
}

func NewEdge(source NodeID, target NodeID, name string) *Edge {
	newEdge := &Edge{
		Eid:  EdgeID(uuid.New()),
		Nids: [2]NodeID{source, target},
		Name: name,
	}
	return newEdge
}

func (e *Edge) Source() NodeID {
	return e.Nids[0]
}

func (e *Edge) Target() NodeID {
	return e.Nids[1]
}
