package graph

import (
	"github.com/google/uuid"
)

type Graph struct {
	Gid   GraphID
	Nids  map[NodeID]bool
	Eids  map[EdgeID]bool
	Nodes map[NodeID]*Node
	Edges map[EdgeID]*Edge

	// 记录两个entity node是由哪个relation node连接在一起
	RelationMap map[[2]NodeID]NodeID

	// Entity Node只有InEdge，Relation Node只有OutEdge
	InEdge  map[NodeID]*map[EdgeID]bool // 某个点相关的入边
	OutEdge map[NodeID]*map[EdgeID]bool // 某个点相关的出边

	Gids           []GraphID                      // 由哪些graph融合得到
	GraphToNodeMap map[GraphID]*map[NodeID]NodeID // nid2 = map[gid1][nid1] 表示graph gid的node nid1融合到该graph的node nid2
	NodeToGraphMap map[NodeID]*map[GraphID]NodeID // nid1 = map[nid2][gid1] 表示graph gid的node nid1融合到该graph的node nid2
}

// @Description: 初始化一个空的图
func NewEmptyGraph() *Graph {
	return &Graph{
		Gid:            GraphID(uuid.New()),
		Nids:           make(map[NodeID]bool),
		Eids:           make(map[EdgeID]bool),
		Nodes:          make(map[NodeID]*Node),
		Edges:          make(map[EdgeID]*Edge),
		InEdge:         make(map[NodeID]*map[EdgeID]bool),
		OutEdge:        make(map[NodeID]*map[EdgeID]bool),
		Gids:           make([]GraphID, 0),
		GraphToNodeMap: make(map[GraphID]*map[NodeID]NodeID),
		NodeToGraphMap: make(map[NodeID]*map[GraphID]NodeID),
		RelationMap:    make(map[[2]NodeID]NodeID),
	}
}

// @Description: 以一个graph作为输入，初始化一个融合图
func NewMergedGraphFromInit(g *Graph) *Graph {
	newg := NewEmptyGraph()

	// nodeid映射表 & edgeid映射表，nodeMap[oldid] = newid
	nodeMap, edgeMap := make(map[NodeID]NodeID), make(map[EdgeID]EdgeID)
	// copy node
	for nid, node := range g.Nodes {
		newNode := NewNodeFromCopy(node)
		nodeMap[nid] = newNode.Nid
		newg.Nids[newNode.Nid] = true
		newg.Nodes[newNode.Nid] = newNode
		inedge, outedge := make(map[EdgeID]bool), make(map[EdgeID]bool)
		newg.InEdge[newNode.Nid] = &inedge
		newg.OutEdge[newNode.Nid] = &outedge
		newMap := make(map[GraphID]NodeID)
		newg.NodeToGraphMap[newNode.Nid] = &newMap
	}
	// copy edge
	for eid, edge := range g.Edges {
		newEdge := NewEdge(nodeMap[edge.Source()], nodeMap[edge.Target()], edge.Name)
		edgeMap[eid] = newEdge.Eid
		newg.Eids[newEdge.Eid] = true
		newg.Edges[newEdge.Eid] = newEdge
	}
	// copy inedge
	for nid, eids := range g.InEdge {
		newNodeID := nodeMap[nid]
		for eid := range *eids {
			(*newg.InEdge[newNodeID])[edgeMap[eid]] = true
		}
	}
	// copy outedge
	for nid, eids := range g.OutEdge {
		newNodeID := nodeMap[nid]
		for eid := range *eids {
			(*newg.OutEdge[newNodeID])[edgeMap[eid]] = true
		}
	}
	// update gids
	newg.Gids = append(newg.Gids, g.Gid)
	// update GraphtoNodeMap
	newg.GraphToNodeMap[g.Gid] = &nodeMap
	// update NodetoGraphMap
	for nid, newNodeID := range nodeMap {
		(*newg.NodeToGraphMap[newNodeID])[g.Gid] = nid
	}
	// update RelationMap
	for nids, nid := range g.RelationMap {
		newg.RelationMap[[2]NodeID{nodeMap[nids[0]], nodeMap[nids[1]]}] = nodeMap[nid]
	}

	return newg
}

func (g *Graph) AddNode(node *Node) {
	g.Nids[node.Nid] = true
	g.Nodes[node.Nid] = node
	inedge, outedge := make(map[EdgeID]bool), make(map[EdgeID]bool)
	g.InEdge[node.Nid] = &inedge
	g.OutEdge[node.Nid] = &outedge
	if g.NodeToGraphMap != nil {
		newMap := make(map[GraphID]NodeID)
		g.NodeToGraphMap[node.Nid] = &newMap
	}
}

// @Description: 新建二元边
func (g *Graph) AddRelation(relationNode *Node, edge1 *Edge, edge2 *Edge) {
	g.AddNode(relationNode)
	g.AddEdge(edge1)
	g.AddEdge(edge2)
	g.UpdateRelationMap(edge1.Target(), edge2.Target(), relationNode.Nid)
}

func (g *Graph) AddEdge(edge *Edge) {
	g.Eids[edge.Eid] = true
	g.Edges[edge.Eid] = edge
	(*g.InEdge[edge.Target()])[edge.Eid] = true
	(*g.OutEdge[edge.Source()])[edge.Eid] = true
}

func (g *Graph) UpdateRelationMap(nid1, nid2, relationNid NodeID) {
	g.RelationMap[[2]NodeID{nid1, nid2}] = relationNid
	g.RelationMap[[2]NodeID{nid2, nid1}] = relationNid
}

func (g *Graph) GetRelationRoles(nid NodeID) []NodeID {
	node := g.Nodes[nid]
	if node.Label != RelationNode {
		return nil
	}

	nids := make([]NodeID, 0)
	for eid := range *g.OutEdge[nid] {
		nids = append(nids, g.Edges[eid].Target())
	}
	return nids
}

func (g *Graph) GetNodeNum() int {
	return len(g.Nids)
}

func (g *Graph) GetEdgeNum() int {
	return len(g.Eids)
}

func (g *Graph) GetEntityNodeNum() int {
	num := 0
	for _, node := range g.Nodes {
		if node.Label == EntityNode {
			num++
		}
	}
	return num
}

func (g *Graph) GetRelationNodeNum() int {
	num := 0
	for _, node := range g.Nodes {
		if node.Label == RelationNode {
			num++
		}
	}
	return num
}
