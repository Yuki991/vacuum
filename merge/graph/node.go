package graph

import "github.com/google/uuid"

type Node struct {
	Nid   NodeID
	Label NodeLabel
	Attrs map[string]string
	Name  string
	Type  string
}

func NewNode(n string, t string, l NodeLabel, attrs map[string]string) *Node {
	newNode := &Node{
		Nid:   NodeID(uuid.New()),
		Label: l,
		Attrs: make(map[string]string),
		Name:  n,
		Type:  t,
	}

	for k, v := range attrs {
		newNode.Attrs[k] = v
	}

	return newNode
}

func NewNodeFromCopy(node *Node) *Node {
	newNode := &Node{
		Nid:   NodeID(uuid.New()),
		Label: node.Label,
		Attrs: make(map[string]string),
		Name:  node.Name,
		Type:  node.Type,
	}

	// copy attr
	for k, v := range node.Attrs {
		newNode.Attrs[k] = v
	}

	return newNode
}
