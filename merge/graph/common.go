package graph

import (
	"github.com/google/uuid"
)

// id type
type NodeID uuid.UUID
type EdgeID uuid.UUID
type GraphID uuid.UUID

// label
type NodeLabel int

const (
	EntityNode   NodeLabel = 0
	RelationNode NodeLabel = 1
	AttrNode     NodeLabel = 2
)

func (id NodeID) String() string {
	return uuid.UUID(id).String()
}

func (id EdgeID) String() string {
	return uuid.UUID(id).String()
}

func (id GraphID) String() string {
	return uuid.UUID(id).String()
}

func (l NodeLabel) String() string {
	switch l {
	case EntityNode:
		return "Entity"
	case RelationNode:
		return "Relation"
	case AttrNode:
		return "Attr"
	default:
		panic("undefined node label")
	}
}
