package utils

import "math"

func GetAllEditSimilarity(texts [][2]string) []float64 {
	result := make([]float64, len(texts))
	for i, t := range texts {
		result[i] = GetEditSimilarity(t[0], t[1])
	}
	return result
}

func GetEditSimilarity(text1, text2 string) float64 {
	return 1.0 - float64(GetEditDistance(text1, text2))/math.Max(float64(len(text1)), float64(len(text2)))
}

// @Description: 计算编辑距离
func GetEditDistance(text1, text2 string) int {
	// f(i, j) = min	f(i - 1, j) + 1
	//					f(i, j - 1) + 1
	//					f(i - 1, j - 1) + (int)(word1[i] != word2[j])

	n, m := len(text1), len(text2)
	f := make([][]int, n+1)
	for i := range f {
		f[i] = make([]int, m+1)
	}
	for i := 0; i <= n; i++ {
		f[i][0] = i
	}
	for i := 0; i <= m; i++ {
		f[0][i] = i
	}
	for i := 1; i <= n; i++ {
		for j := 1; j <= m; j++ {
			f[i][j] = MinInt(f[i-1][j]+1, f[i][j-1]+1)
			if text1[i-1] == text2[j-1] {
				f[i][j] = MinInt(f[i][j], f[i-1][j-1])
			} else {
				f[i][j] = MinInt(f[i][j], f[i-1][j-1]+1)
			}
		}
	}
	return f[n][m]
}
