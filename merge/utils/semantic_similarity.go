package utils

import (
	"encoding/json"
	"fmt"
	"net"
)

type SemanticSimilarityRequest struct {
	Text1 string `json:"text1"`
	Text2 string `json:"text2"`
}

type SemanticSimilarityResponse struct {
	Text1      string  `json:"text1"`
	Text2      string  `json:"text2"`
	Similarity float64 `json:"similarity"`
}

func GetSemanticSimilarity(text1 string, text2 string) (float64, error) {
	// 仿真实验使用编辑距离计算相似度，真实实验用语义向量计算相似度
	switch Mode {
	case `synthetic`:
		return GetEditSimilarity(text1, text2), nil
	default:
		// return getSemanticSimilarity(text1, text2)
		return GetEditSimilarity(text1, text2), nil
	}
}

func GetAllSemanticSimilarity(texts [][2]string) ([]float64, error) {
	// 仿真实验使用编辑距离计算相似度，真实实验用语义向量计算相似度
	switch Mode {
	case `synthetic`:
		return GetAllEditSimilarity(texts), nil
	default:
		// return getAllSemanticSimilarity(texts)
		return GetAllEditSimilarity(texts), nil
	}
}

func getSemanticSimilarity(text1 string, text2 string) (float64, error) {
	texts := [][2]string{{text1, text2}}
	result, err := getAllSemanticSimilarity(texts)
	if err != nil {
		fmt.Printf("get semantic similarity error: %v\n", err)
		return 0, err
	} else {
		return result[0], nil
	}
}

func getAllSemanticSimilarity(texts [][2]string) ([]float64, error) {
	reqs := make([]SemanticSimilarityRequest, 0, len(texts))
	for i := range texts {
		reqs = append(reqs, SemanticSimilarityRequest{
			Text1: texts[i][0],
			Text2: texts[i][1],
		})
	}
	sendData, _ := json.Marshal(reqs)

	// connect
	conn, err := net.Dial("tcp", "127.0.0.1:8000")
	if err != nil {
		return nil, fmt.Errorf("get semantic similarity error at connect, %v", err)
	}
	defer conn.Close()

	// send
	conn.Write(sendData)

	// get response
	bufSize := 4096
	buf := make([]byte, 0, bufSize)
	fixedBuf := make([]byte, bufSize)
	for {
		n, _ := conn.Read(fixedBuf)
		buf = append(buf, fixedBuf[0:n]...)
		if n < bufSize {
			break
		}
	}

	// parse response
	resp := make([]SemanticSimilarityResponse, 0, 100)
	if err = json.Unmarshal(buf, &resp); err != nil {
		return nil, fmt.Errorf("get semantic similarity error at parse data: %v", err)
	}

	result := make([]float64, len(resp))
	for i := range resp {
		result[i] = resp[i].Similarity
	}
	return result, nil
}
