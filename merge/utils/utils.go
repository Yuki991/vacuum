package utils

func MinInt(x int, y int) int {
	if x < y {
		return x
	} else {
		return y
	}
}

func MaxFloat64(x float64, y float64) float64 {
	if x > y {
		return x
	} else {
		return y
	}
}
