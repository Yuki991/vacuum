package utils

import (
	"encoding/json"
	"fmt"
	"net"
)

type EigenRequest struct {
	Num int         `json:"number"`
	Arr [][]float64 `json:"array"`
}

type EigenResponse struct {
	EigenValues  []float64   `json:"values"`
	EigenVectors [][]float64 `json:"vectors"`
}

// @Description: 特征值分解
// @Param num: 指定返回前num小的特征值及其特征向量
// @Return $0: 从小到大排序的特征值向量
// @Return $1: 特征向量组成的矩阵，每一列是相应特征值的特征向量
func GetEigenDecomposition(arr [][]float64, num int) ([]float64, [][]float64, error) {
	conn, err := net.Dial("tcp", "127.0.0.1:8001")
	if err != nil {
		return nil, nil, fmt.Errorf("connect error: %v", err)
	}
	defer conn.Close()

	// data
	req := EigenRequest{
		Num: num,
		Arr: arr,
	}
	data, err := json.Marshal(req)
	if err != nil {
		return nil, nil, fmt.Errorf("encode json error: %v", err)
	}

	// send data
	conn.Write(data)

	// receive response
	bufSize := 4096
	buf := make([]byte, 0, bufSize)
	fixedBuf := make([]byte, bufSize)
	for {
		n, _ := conn.Read(fixedBuf)
		buf = append(buf, fixedBuf[0:n]...)
		if n < bufSize {
			break
		}
	}

	// parse response
	resp := EigenResponse{}
	err = json.Unmarshal(buf, &resp)
	if err != nil {
		return nil, nil, fmt.Errorf("parse error: %v", err)
	}

	return resp.EigenValues, resp.EigenVectors, nil
}
