package mtree

import "iomgm/graph"

// func printMergeTreeStructure(treenode *MergeTreeNode) {
// 	fmt.Printf("%v: %v\n", treenode, treenode.Childs)
// 	if treenode.Childs != nil {
// 		for _, child := range treenode.Childs {
// 			printMergeTreeStructure(child)
// 		}
// 	}
// }

func (mt *MergeTree) SelfCheck() {
	checkfunc := func(g1 *graph.Graph, g2 *graph.Graph) {
		for nid1 := range g1.Nids {
			if nid2, ok := (*g2.GraphToNodeMap[g1.Gid])[nid1]; !ok {
				panic("error")
			} else {
				if nid3, ok := (*g2.NodeToGraphMap[nid2])[g1.Gid]; !ok || nid1 != nid3 {
					panic("error")
				}
			}
		}
	}

	var dfs func(mtn *MergeTreeNode, fa *MergeTreeNode)
	dfs = func(mtn *MergeTreeNode, fa *MergeTreeNode) {
		if fa != nil {
			for i := range mtn.Graphs {
				checkfunc(mtn.Graphs[i], mtn.MergedGraph)
			}
		}

		if mtn.Childs != nil {
			for i := range mtn.Childs {
				dfs(mtn.Childs[i], mtn)
			}
		}
	}

	dfs(mt.Root, nil)
}

func checkRelationOfChildAndFather(treenode *MergeTreeNode) {
	if treenode == nil || treenode.Childs == nil {
		return
	}

	for _, child := range treenode.Childs {
		if child.Father != treenode {
			panic("error")
		}
	}
}
