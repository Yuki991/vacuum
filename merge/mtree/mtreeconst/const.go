package mtreeconst

const (
	// 全量融合的融合阈值，忽略低于threshold的匹配对
	FullMergeThreshold float64 = 0.7
	// 增量融合的融合阈值，忽略低于threshold的匹配对
	IncrementalMergeThreshold float64 = 0.7
	// 增量融合孤立节点的融合阈值
	IncrementalMergeThresholdForIsolatedNode float64 = 0.7
	// relation节点相应的roles的拆分阈值
	NodeSplitThreshold float64 = 0.5
)
