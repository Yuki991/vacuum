package mtree

import (
	"fmt"
	"iomgm/graph"
	"iomgm/mtree/merge"
)

// @Description: 计算merge tree中的一个graph所有节点的熵值
func (mt *MergeTree) GetAllNodeEntropy(g *graph.Graph) ([]merge.NodeEntropy, error) {
	if _, ok := mt.GidToGraphMap[g.Gid]; !ok {
		return nil, fmt.Errorf("graph is not in merge tree")
	}
	return merge.GetAllNodeEntropy(g, mt.GidToGraphMap), nil
}
