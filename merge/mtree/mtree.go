package mtree

import (
	"fmt"
	"iomgm/common"
	"iomgm/graph"
	"iomgm/mtree/merge"
	"iomgm/utils"

	"github.com/google/uuid"
)

type MergeTreeNode struct {
	Nid         MergeTreeNodeID
	Graphs      []*graph.Graph   // tree node中包含的所有graphs
	MergedGraph *graph.Graph     // 由tree node中的graphs融合得到的graph
	Childs      []*MergeTreeNode // 除了叶子节点，childs的数目应当与graphs数目相同
	Father      *MergeTreeNode
}

// TODO 在合适的地方加并行
type MergeTree struct {
	GraphNum               int
	ExpectedGraphNumInNode int // 每个node期望包含graphs的数目
	Root                   *MergeTreeNode
	GidToMergeTreeNodeMap  map[graph.GraphID]*MergeTreeNode // 记录每个graph在哪里TreeNode
	GidToGraphMap          map[graph.GraphID]*graph.Graph
}

// @Description: 创建一棵merge tree
// @Param graphs:
// @Param init: build merge tree的方式，暂时有两种可用的方式`simple`和`cluster`
func NewMergeTree(graphs []*graph.Graph, init string) (*MergeTree, error) {
	if len(graphs) == 0 {
		return nil, fmt.Errorf("empty input")
	}

	mt := &MergeTree{
		GraphNum:               len(graphs),
		ExpectedGraphNumInNode: 3,
		Root:                   nil,
		GidToMergeTreeNodeMap:  make(map[graph.GraphID]*MergeTreeNode),
		GidToGraphMap:          make(map[graph.GraphID]*graph.Graph),
	}

	// build tree
	switch init {
	case "simple":
		mt.initializeMergeTreeSimple(graphs)
	case "cluster":
		mt.initializeMergeTreeWithClutering(graphs)
	default:
		return nil, fmt.Errorf("error init value, please use 'simple' or 'cluster'")
	}

	// printMergeTreeStructure(mt.Root)
	common.Log("finish build merge tree")
	common.Log(fmt.Sprintf("input graph num: %v, total graph num: %v", len(graphs), len(mt.GidToMergeTreeNodeMap)))
	common.Log("")

	return mt, nil
}

// @Description: 用`cluster`方式自下而上建树，通过谱聚类的方式进行分组
func (mt *MergeTree) initializeMergeTreeWithClutering(graphs []*graph.Graph) {
	// 初始化GidToGraphMap
	for i := range graphs {
		mt.GidToGraphMap[graphs[i].Gid] = graphs[i]
	}

	// 初始化叶子节点
	cluster := merge.GraphSpectralClustering(graphs, (len(graphs)-1)/mt.ExpectedGraphNumInNode+1)

	fmt.Printf("cluster result: %v\n", cluster)

	nodes := make([]*MergeTreeNode, len(cluster))
	for i := 0; i < len(cluster); i++ {
		nodes[i] = &MergeTreeNode{
			Nid:         MergeTreeNodeID(uuid.New()),
			Graphs:      make([]*graph.Graph, len(cluster[i])),
			MergedGraph: nil,
			Childs:      nil,
			Father:      nil,
		}
		for j := 0; j < len(cluster[i]); j++ {
			nodes[i].Graphs[j] = graphs[cluster[i][j]]
			// update GidToMergeTreeNodeMap
			mt.GidToMergeTreeNodeMap[graphs[cluster[i][j]].Gid] = nodes[i]
		}
	}

	// 自下而上建树
	for len(cluster) != 1 || len(nodes[0].Graphs) > 1 {
		// merge
		graphs = make([]*graph.Graph, len(cluster))
		for i := 0; i < len(nodes); i++ {
			graphs[i] = merge.FullMergeGraphs(nodes[i].Graphs, mt.GidToGraphMap)
			nodes[i].MergedGraph = graphs[i]
			// update GidToGraphMap
			mt.GidToGraphMap[graphs[i].Gid] = graphs[i]
		}

		// cluster
		cluster = merge.GraphSpectralClustering(graphs, (len(graphs)-1)/mt.ExpectedGraphNumInNode+1)

		fmt.Printf("graph number: %v\n", len(graphs))
		fmt.Printf("cluster result: %v\n", cluster)

		// create tree node
		newNodes := make([]*MergeTreeNode, len(cluster))
		for i := 0; i < len(cluster); i++ {
			newNodes[i] = &MergeTreeNode{
				Nid:         MergeTreeNodeID(uuid.New()),
				Graphs:      make([]*graph.Graph, len(cluster[i])),
				MergedGraph: nil,
				Childs:      make([]*MergeTreeNode, len(cluster[i])),
				Father:      nil,
			}

			for j := 0; j < len(cluster[i]); j++ {
				newNodes[i].Graphs[j] = graphs[cluster[i][j]]
				newNodes[i].Childs[j] = nodes[cluster[i][j]]
				nodes[cluster[i][j]].Father = newNodes[i]
				// update GidToMergeTreeNodeMap
				mt.GidToMergeTreeNodeMap[newNodes[i].Graphs[j].Gid] = newNodes[i]
			}
		}

		nodes = newNodes
	}
	mt.Root = nodes[0]
	mt.Root.MergedGraph = mt.Root.Graphs[0]
}

// TODO debug
// @Description: 朴素建树方法，尽量每个node中放ExpectedGraphNumInNode个graphs
func (mt *MergeTree) initializeMergeTreeSimple(graphs []*graph.Graph) {
	// 初始化GidToGraphMap
	for i := range graphs {
		mt.GidToGraphMap[graphs[i].Gid] = graphs[i]
	}

	// 随机设置graphs顺序
	//

	// 初始化叶子节点
	nodes := make([]*MergeTreeNode, 0, (len(graphs)-1)/mt.ExpectedGraphNumInNode+1)
	for i := 0; i < len(graphs); i += mt.ExpectedGraphNumInNode {
		j := utils.MinInt(i+mt.ExpectedGraphNumInNode, len(graphs))
		node := &MergeTreeNode{
			Nid:         MergeTreeNodeID(uuid.New()),
			Graphs:      graphs[i:j],
			MergedGraph: nil,
			Childs:      nil,
			Father:      nil,
		}
		// update GidToMergeTreeNodeMap
		for k := i; k < j; k++ {
			mt.GidToMergeTreeNodeMap[graphs[k].Gid] = node
		}
		nodes = append(nodes, node)
	}

	// 自下而上建树
	for len(nodes) != 1 || len(nodes[0].Graphs) > 1 {
		newNodes := make([]*MergeTreeNode, 0, (len(nodes)-1)/mt.ExpectedGraphNumInNode+1)
		for i := 0; i < len(nodes); i += mt.ExpectedGraphNumInNode {
			j := utils.MinInt(i+mt.ExpectedGraphNumInNode, len(nodes))
			node := &MergeTreeNode{
				Nid:         MergeTreeNodeID(uuid.New()),
				Graphs:      make([]*graph.Graph, j-i),
				MergedGraph: nil,
				Childs:      make([]*MergeTreeNode, j-i),
				Father:      nil,
			}
			for k := 0; k < j-i; k++ {
				node.Childs[k] = nodes[i+k]
				nodes[i+k].Father = node
				node.Graphs[k] = merge.FullMergeGraphs(node.Childs[k].Graphs, mt.GidToGraphMap)
				node.Childs[k].MergedGraph = node.Graphs[k]

				// update GidToGraphMap & GidToMergeTreeNodeMap
				mt.GidToGraphMap[node.Graphs[k].Gid] = node.Graphs[k]
				mt.GidToMergeTreeNodeMap[node.Graphs[k].Gid] = node
			}
			newNodes = append(newNodes, node)
		}
		nodes = newNodes
	}
	mt.Root = nodes[0]
	mt.Root.MergedGraph = mt.Root.Graphs[0]
}
