package merge

import (
	"fmt"
	"iomgm/common"
	"iomgm/graph"
)

func printSplitedNodesName(splitedNodes SplitedNodes, graphMap map[graph.GraphID]*graph.Graph) {
	s := make(map[graph.GraphID][]string)
	for gid, nids := range splitedNodes {
		g := graphMap[gid]
		for _, nid := range nids {
			node := g.Nodes[nid]
			s[gid] = append(s[gid], node.Name)
		}
	}
	common.Log(fmt.Sprintf("splited nodes: %v", s))
}

func printMatrix(mat [][]float64) {
	common.Log(fmt.Sprintf("%v", mat))
}

func printNodesEntropy(nodeEntropy []NodeEntropy) {
	e := make([]float64, 0, len(nodeEntropy))
	for i := range nodeEntropy {
		e = append(e, nodeEntropy[i].E)
	}
	common.Log(fmt.Sprintf("node entropy: %v", e))
}

func printAllNodesNameOfAllParts(g *graph.Graph, graphMap map[graph.GraphID]*graph.Graph) {
	nodesName := make([][]string, 0)
	for nid := range g.Nids {
		if g.Nodes[nid].Label == graph.EntityNode {
			s := make([]string, 0)
			for gid2, nid2 := range *g.NodeToGraphMap[nid] {
				g2 := graphMap[gid2]
				s = append(s, g2.Nodes[nid2].Name)
			}
			nodesName = append(nodesName, s)
		}
	}
	common.Log(fmt.Sprintf("merge graph nodes name: %v", nodesName))
}

func printAllNodesName(g *graph.Graph) {
	nodesName := make([]string, 0, len(g.Nids))
	for _, node := range g.Nodes {
		if node.Label == graph.EntityNode {
			nodesName = append(nodesName, node.Name)
		}
	}
	common.Log(fmt.Sprintf("nodes name: %v", nodesName))
}

func printMatchingResult(g1 *graph.Graph, g2 *graph.Graph, match [][2]graph.NodeID, score []float64) {
	matchName := make([][5]string, len(match))
	for i := range match {
		nid1, nid2 := match[i][0], match[i][1]
		node1, node2 := g1.Nodes[nid1], g2.Nodes[nid2]
		matchName[i] = [5]string{node1.Name, node2.Name, node1.Type, node2.Type, fmt.Sprintf("%v", score[i])}
	}
	common.Log(fmt.Sprintf("matching result: %v", matchName))
}
