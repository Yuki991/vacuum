package merge

import "iomgm/graph"

type SplitedNodes map[graph.GraphID][]graph.NodeID

// @Description: 删除graph中nids这些节点以及相连的edges，并删除相关信息，并返回拆分出来的节点信息
func splitNodeFromMergedGraph(g *graph.Graph, nids []graph.NodeID) SplitedNodes {
	// 如果拆分了一个“Entity节点”，相关的“Relation节点”不会在该函数中拆分，需要另行处理

	// 更新relationmap
	for _, nid := range nids {
		node := g.Nodes[nid]
		if node.Label == graph.RelationNode {
			roles := g.GetRelationRoles(nid)
			delete(g.RelationMap, [2]graph.NodeID{roles[0], roles[1]})
			delete(g.RelationMap, [2]graph.NodeID{roles[1], roles[0]})
		}
	}

	// delete related edges
	for _, nid := range nids {
		for eid := range *g.InEdge[nid] {
			if _, ok := g.Eids[eid]; ok {
				// 从inedge和outedge中删除
				source, target := g.Edges[eid].Source(), g.Edges[eid].Target()
				delete(*g.InEdge[target], eid)
				delete(*g.OutEdge[source], eid)
				// 从eids和edges中删除
				delete(g.Eids, eid)
				delete(g.Edges, eid)
			}
		}
		for eid := range *g.OutEdge[nid] {
			if _, ok := g.Eids[eid]; ok {
				// 从inedge和outedge中删除
				source, target := g.Edges[eid].Source(), g.Edges[eid].Target()
				delete(*g.InEdge[target], eid)
				delete(*g.OutEdge[source], eid)
				// 从eids和edges中删除
				delete(g.Eids, eid)
				delete(g.Edges, eid)
			}
		}
	}

	// delete nodes
	for _, nid := range nids {
		delete(g.Nids, nid)
		delete(g.Nodes, nid)
		delete(g.InEdge, nid)
		delete(g.OutEdge, nid)
	}

	// delete related info
	spnodes := make(SplitedNodes)
	for _, nid := range nids {
		for gid2, nid2 := range *g.NodeToGraphMap[nid] {
			if _, ok := spnodes[gid2]; !ok {
				spnodes[gid2] = make([]graph.NodeID, 0)
			}
			spnodes[gid2] = append(spnodes[gid2], nid2)

			delete(*g.GraphToNodeMap[gid2], nid2)
		}
		delete(g.NodeToGraphMap, nid)
	}
	return spnodes
}
