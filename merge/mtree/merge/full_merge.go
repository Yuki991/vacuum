package merge

import (
	"fmt"
	"iomgm/bpmatch"
	"iomgm/common"
	"iomgm/graph"
	"iomgm/mtree/mtreeconst"
)

// TODO relation node不进行二分图匹配

// @Description: 全量融合
func FullMergeGraphs(graphs []*graph.Graph, graphMap map[graph.GraphID]*graph.Graph) *graph.Graph {
	common.Log("start full merge")
	common.Log(fmt.Sprintf("graph num: %v", len(graphs)))
	common.Log(fmt.Sprintf("graphs: %v", graphMap))

	// 融合阈值
	mergeThreshold := mtreeconst.FullMergeThreshold

	// 以graphs[0]初始化融合图mgraph(merged graph)
	mgraph := graph.NewMergedGraphFromInit(graphs[0])

	// printAllNodesNameOfAllParts(mgraph, graphMap)
	// printAllNodesName(mgraph)

	common.Log("finish init mgraph")

	// 从左到右依次执行两图融合得到初始融合图谱
	for i := 1; i < len(graphs); i++ {
		// match，注意只对齐entity node，relation node需要另行处理
		bipartite := bpmatch.NewTwoGraphBipartite(mgraph, graphs[i])
		matchIndex, score, _ := bpmatch.CreateKuhnMunkrasAlgo().Solve(bipartite)
		match := bipartite.TranslateIndexToNodeID(matchIndex)
		// merge
		mergeGraphAccordingToMatch(mgraph, graphs[i], match, score, mergeThreshold)
	}

	common.Log("finish init merge")

	printAllNodesNameOfAllParts(mgraph, graphMap)
	// printAllNodesName(mgraph)

	common.Log("start iterate")

	// 迭代，拆分熵高的节点重新融合
	// 最大迭代轮次
	maxIterRound := 3
	// 将熵从大到小排序后，拆分前splitRatio的节点重新排序
	splitRatio := 0.05
	for ; maxIterRound > 0; maxIterRound-- {
		// 计算entropy，由大到小排序
		nodeEntropy := GetAllNodeEntropy(mgraph, graphMap)

		printNodesEntropy(nodeEntropy)

		// 取排名靠前的"Entity节点"作为待拆分节点
		splitNum := int(splitRatio * float64(len(mgraph.Nids)))
		toSplitNids := make([]graph.NodeID, 0, splitNum)
		tmpMap := make(map[graph.NodeID]bool)
		entityNodeCount := 0
		for i := 0; i < splitNum; i++ {
			if entityNodeCount < splitNum && mgraph.Nodes[nodeEntropy[i].Nid].Label == graph.EntityNode {
				toSplitNids = append(toSplitNids, nodeEntropy[i].Nid)
				tmpMap[nodeEntropy[i].Nid] = true
				entityNodeCount++
			}
		}
		// 与”Entity节点“相连的”Relation节点“也需拆分
		for i := 0; i < entityNodeCount; i++ {
			nid := toSplitNids[i]
			// Entity节点都是InEdge
			for eid := range *mgraph.InEdge[nid] {
				edge := mgraph.Edges[eid]
				if _, ok := tmpMap[edge.Source()]; !ok {
					// 将该“Relation节点”放到toSplitNid中
					toSplitNids = append(toSplitNids, edge.Source())
					tmpMap[edge.Source()] = true
				}
			}
		}

		common.Log(fmt.Sprintf("split num: %v", splitNum))
		common.Log(fmt.Sprintf("toSplitNids num: %v", len(toSplitNids)))

		// 拆分节点
		splitedNodes := splitNodeFromMergedGraph(mgraph, toSplitNids)

		printSplitedNodesName(splitedNodes, graphMap)

		// rematch and merge
		rematchAndMerge(splitedNodes, mgraph, graphMap, mergeThreshold)
	}
	common.Log("finish iterate")
	return mgraph
}

// @Description: 根据match结果将g合并到mgraph中。忽略score低于threshold的匹配，将g中相应的节点作为新增节点
// @Param match: 对齐方案，注意match中只包含entity node的对齐方案
func mergeGraphAccordingToMatch(mgraph *graph.Graph, g *graph.Graph, match [][2]graph.NodeID, score []float64, threshold float64) {
	// 注意可能存在没有出现在match中的节点

	// nodeid映射表，nodeMap[oldid] = newid
	nodeMap := make(map[graph.NodeID]graph.NodeID)

	// update entity node
	// 记录g中哪些节点被merge
	mp := make(map[graph.NodeID]bool)
	for i, m := range match {
		nid1, nid2 := m[0], m[1]
		if score[i] >= threshold {
			// 大于阈值，合并node
			nodeMap[nid2] = nid1
			mp[nid2] = true
		}
	}
	// 对于g中没有对齐的节点，新建一个融合节点
	for nid2, node := range g.Nodes {
		if node.Label == graph.EntityNode {
			if _, ok := mp[nid2]; !ok {
				// TODO 注意，目前融合节点的信息与“第一个节点”相同
				newNode := graph.NewNodeFromCopy(node)
				nodeMap[nid2] = newNode.Nid
				mgraph.AddNode(newNode)
			}
		}
	}
	// update relation node & edges
	for nid2, node := range g.Nodes {
		if node.Label == graph.RelationNode {
			nids := g.GetRelationRoles(nid2)
			if nid1, ok := mgraph.RelationMap[[2]graph.NodeID{nodeMap[nids[0]], nodeMap[nids[1]]}]; ok {
				// 该relation node在mgraph中有相应的relation node，直接融合node，不需要添加edge
				nodeMap[nid2] = nid1
			} else {
				// 该relation node在mgraph中没有相应的relation node，新建node，添加edge
				newNode := graph.NewNodeFromCopy(node)
				nodeMap[nid2] = newNode.Nid
				mgraph.AddNode(newNode)
				// update relationmap
				mgraph.UpdateRelationMap(nodeMap[nids[0]], nodeMap[nids[1]], newNode.Nid)
				// 添加edges
				for eid := range *g.OutEdge[nid2] {
					edge := g.Edges[eid]
					s, t := nodeMap[edge.Source()], nodeMap[edge.Target()]
					newEdge := graph.NewEdge(s, t, edge.Name)
					mgraph.AddEdge(newEdge)
				}
			}
		}
	}

	// update gids
	mgraph.Gids = append(mgraph.Gids, g.Gid)
	// update GraphToNodeMap
	mgraph.GraphToNodeMap[g.Gid] = &nodeMap
	// update NodeToGraphMap
	for nid1, nid2 := range nodeMap {
		(*mgraph.NodeToGraphMap[nid2])[g.Gid] = nid1
	}
}
