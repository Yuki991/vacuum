package merge

import (
	"iomgm/bpmatch"
	"iomgm/graph"
	"math"
	"sort"
)

type NodeEntropy struct {
	Nid graph.NodeID
	E   float64
}

// @Description: 计算g中所有node的entropy，由大到小排序
func GetAllNodeEntropy(g *graph.Graph, graphMap map[graph.GraphID]*graph.Graph) []NodeEntropy {
	result := make([]NodeEntropy, 0, len(g.Nids))
	for nid := range g.Nids {
		result = append(result, NodeEntropy{
			Nid: nid,
			E:   getNodeEntropy(g, nid, graphMap),
		})
	}

	sort.Slice(result, func(i int, j int) bool {
		return result[i].E > result[j].E
	})
	return result
}

func getNodeEntropy(g *graph.Graph, nid graph.NodeID, graphMap map[graph.GraphID]*graph.Graph) float64 {
	// entropy = - sigma { p(x) * log { sigma { p(y) * s(x, y) } } }

	gids := make([]graph.GraphID, 0)
	nids := make([]graph.NodeID, 0)
	for gid2, nid2 := range *g.NodeToGraphMap[nid] {
		gids = append(gids, gid2)
		nids = append(nids, nid2)
	}
	n := len(gids)

	scores := make([][]float64, n)
	for i := 0; i < n; i++ {
		scores[i] = make([]float64, n)
	}
	for i := 0; i < n; i++ {
		scores[i][i] = 1
		for j := i + 1; j < n; j++ {
			bipartite := bpmatch.NewTwoGraphBipartite(graphMap[gids[i]], graphMap[gids[j]])
			// scores[i][j], _ = bipartite.GetSimilarityFromNodeID(nids[i], nids[j])
			scores[i][j], _ = bipartite.GetNodeAttrSimilarity(nids[i], nids[j])
			scores[j][i] = scores[i][j]
		}
	}

	// entropy = - sigma { 1/N * log { sigma { 1/N * s(x, y) } } }
	var entropy float64 = 0
	for i := 0; i < n; i++ {
		var sum float64 = 0
		for j := 0; j < n; j++ {
			sum += scores[i][j]
		}
		sum = 1.0 / float64(n) * sum
		entropy += -1.0 / float64(n) * math.Log2(sum)
	}
	return entropy
}
