package merge

import (
	"iomgm/graph"
	"iomgm/utils"
	"math"
)

// @Description: 谱聚类
// @Param graph: 输入的一组graph
// @Param clusterNum: 类别数目
// @Return: 聚类结果，返回index
func GraphSpectralClustering(graphs []*graph.Graph, clusterNum int) [][]int {
	if clusterNum == 1 {
		result := make([]int, len(graphs))
		for i := range result {
			result[i] = i
		}
		results := make([][]int, 1)
		results[0] = result
		return results
	}

	// 计算两两之间的“平均语义相似度”构建权值矩阵
	laplacian := make([][]float64, len(graphs))
	deg := make([]float64, len(graphs))
	degSqrt := make([]float64, len(graphs))
	for i := 0; i < len(graphs); i++ {
		laplacian[i] = make([]float64, len(graphs))
	}
	for i := 0; i < len(graphs); i++ {
		laplacian[i][i] = 0.0
		for j := i + 1; j < len(graphs); j++ {
			laplacian[i][j] = getGraphAverageEntityNodeSemnaticSimilarity(graphs[i], graphs[j])
			laplacian[j][i] = laplacian[i][j]
		}
	}
	for i := 0; i < len(graphs); i++ {
		deg[i] = 0
		for j := 0; j < len(graphs); j++ {
			deg[i] += laplacian[i][j]
		}
		degSqrt[i] = math.Sqrt(deg[i])
	}

	// 计算拉普拉斯矩阵，有两种拉普拉斯矩阵的计算方案，分别对应两种谱聚类的方案。这里采取第二种。
	// 1. Unnormalize(C1, ..., Ck) = 1/2 * sum { W(Ci, Ci') / |Ci| }
	// 	  L = D - W
	// 2. Normalize(C1, ..., Ck) = 1/2 * sum { W(Ci, Ci') / sum { di | i in Ci } }
	//    L' = D^{-1/2}*L*D^{-1/2}
	for i := 0; i < len(graphs); i++ {
		for j := 0; j < len(graphs); j++ {
			if laplacian[i][j] > 0 {
				laplacian[i][j] = -laplacian[i][j] / (degSqrt[i] * degSqrt[j])
			}
		}
		laplacian[i][i] = 1
	}

	// 特征值分解，行向量作为n个节点的向量表示
	_, points, err := utils.GetEigenDecomposition(laplacian, clusterNum)
	if err != nil {
		panic(err)
	}

	// kmeans
	cluster := kMeans(points, clusterNum)

	return cluster
}

func kMeans(points [][]float64, clusterNum int) [][]int {
	// 迭代最大轮数
	iter := 10

	cluster := make([]int, len(points))
	center := make([][]float64, clusterNum)
	// 初始化前clusterNum个作为初始中心
	for i := 0; i < clusterNum; i++ {
		center[i] = make([]float64, clusterNum)
		for j := 0; j < clusterNum; j++ {
			center[i][j] = points[i][j]
		}
	}
	for i := 0; i < clusterNum; i++ {
		cluster[i] = -1
	}

	for ; iter > 0; iter-- {
		// 统计cluster改变的数目，如果没有变化就退出迭代
		count := 0

		for i := 0; i < len(points); i++ {
			var minDistance float64 = getEuclideanDistance(points[i], center[0])
			var idx int = 0
			for j := 1; j < clusterNum; j++ {
				dis := getEuclideanDistance(points[i], center[j])
				if dis < minDistance {
					minDistance = dis
					idx = j
				}
			}

			if idx != cluster[i] {
				cluster[i] = idx
				count++
			}
		}

		if count == 0 {
			break
		}

		// 记录每个cluster有多少个节点
		clusterCnt := make([]int, clusterNum)
		// 重新计算各个类别的中心
		for i := 0; i < clusterNum; i++ {
			clusterCnt[i] = 0
			for j := 0; j < clusterNum; j++ {
				center[i][j] = 0
			}
		}
		for i := 0; i < len(points); i++ {
			clusterCnt[cluster[i]]++
			for j := 0; j < clusterNum; j++ {
				center[cluster[i]][j] += points[i][j]
			}
		}
		for i := 0; i < clusterNum; i++ {
			for j := 0; j < clusterNum; j++ {
				center[i][j] /= float64(clusterCnt[i])
			}
		}
	}

	result := make([][]int, clusterNum)
	for i := 0; i < clusterNum; i++ {
		result[i] = make([]int, 0)
	}
	for i := 0; i < len(points); i++ {
		result[cluster[i]] = append(result[cluster[i]], i)
	}
	return result
}

// @Description: 计算“图平均语义相似度”，根据实体名称语义相似度计算，不涉及“边节点”
func getGraphAverageEntityNodeSemnaticSimilarity(g1, g2 *graph.Graph) float64 {
	// TODO 效率上可以优化一下

	var sum float64 = 0
	var entityNodeNum float64 = 0

	for _, node1 := range g1.Nodes {
		if node1.Label == graph.EntityNode {
			entityNodeNum += 1

			var maxValue float64 = 0
			for _, node2 := range g2.Nodes {
				if node2.Label == graph.EntityNode && node1.Type == node2.Type {
					similarity, _ := utils.GetSemanticSimilarity(node1.Name, node2.Name)
					maxValue = utils.MaxFloat64(maxValue, similarity)
				}
			}

			sum += maxValue
		}
	}

	for _, node2 := range g2.Nodes {
		if node2.Label == graph.EntityNode {
			entityNodeNum += 1

			var maxValue float64 = 0
			for _, node1 := range g1.Nodes {
				if node1.Label == graph.EntityNode && node1.Type == node2.Type {
					similarity, _ := utils.GetSemanticSimilarity(node1.Name, node2.Name)
					maxValue = utils.MaxFloat64(maxValue, similarity)
				}
			}

			sum += maxValue
		}
	}

	return sum / entityNodeNum
}

func getEuclideanDistance(x, y []float64) float64 {
	var sum float64 = 0
	for i := 0; i < len(x); i++ {
		sum += (x[i] - y[i]) * (x[i] - y[i])
	}
	return math.Sqrt(sum)
}
