package merge

import (
	"iomgm/bpmatch"
	"iomgm/graph"
)

// @Description: 重新对齐被拆分的节点spnodes，并融合到mgraph中
// @Return $0: mgraph中新增的节点
func rematchAndMerge(spnodes SplitedNodes, mgraph *graph.Graph, graphMap map[graph.GraphID]*graph.Graph, threshold float64) []graph.NodeID {
	type SingleNode struct {
		Gid graph.GraphID
		Nid graph.NodeID
	}
	type RematchResult []SingleNode
	type RematchResults []RematchResult

	if len(spnodes) == 0 {
		return nil
	}

	gids := make([]graph.GraphID, 0, len(spnodes))
	for gid := range spnodes {
		gids = append(gids, gid)
	}
	// 只包含splited entity nodes
	spenodes := make(SplitedNodes)
	for gid, nids := range spnodes {
		g := graphMap[gid]
		for _, nid := range nids {
			if g.Nodes[nid].Label == graph.EntityNode {
				spenodes[gid] = append(spenodes[gid], nid)
			}
		}
	}

	// 以gids[0]对应的graph进行初始化
	rematch := make(RematchResults, len(spenodes[gids[0]]))
	for i, nid := range spenodes[gids[0]] {
		rematch[i] = make(RematchResult, 1)
		rematch[i][0] = SingleNode{gids[0], nid}
	}

	// 只对齐entity node
	// 按照gids的顺序从左到右rematch，注意下标从1开始
	for k := 1; k < len(gids); k++ {
		// 将gids[k]与rematch的“融合节点”进行对齐，相似度计算时将“融合节点”下标0对应的节点作为代表
		gid2 := gids[k]
		sims := make([][]float64, len(rematch))
		for i := 0; i < len(rematch); i++ {
			sims[i] = make([]float64, len(spenodes[gid2]))
			gid1 := rematch[i][0].Gid
			bipartite := bpmatch.NewTwoGraphBipartite(graphMap[gid1], graphMap[gid2])
			for j := 0; j < len(spenodes[gid2]); j++ {
				nid1, nid2 := rematch[i][0].Nid, spenodes[gid2][j]
				sims[i][j], _ = bipartite.GetSimilarityFromNodeID(nid1, nid2)
			}
		}

		// match
		bipartite := bpmatch.NewVanillaBipartite(len(rematch), len(spenodes[gid2]), sims)
		// TODO 使用不同的match策略
		matchIndex, score, _ := bpmatch.CreateGreedMatchingAlgo().Solve(bipartite, threshold)
		// merge
		// 记录spenodes[gids[k]]中的哪些节点被merge
		mp := make(map[int]bool)
		for i, m := range matchIndex {
			singleNode := SingleNode{gid2, spenodes[gid2][m[1]]}
			if score[i] >= threshold {
				// 大于阈值，合并节点
				rematch[m[0]] = append(rematch[m[0]], singleNode)
				mp[m[1]] = true
			}
		}
		// 对于没有出现在match中，或者match score较低的节点，即没有节点与之对齐，新建一个融合节点
		for i := range spenodes[gid2] {
			singleNode := SingleNode{gid2, spenodes[gid2][i]}
			if _, ok := mp[i]; !ok {
				tmp := make(RematchResult, 1)
				tmp[0] = singleNode
				rematch = append(rematch, tmp)
			}
		}
	}

	// 根据rematch结果，将splited entity node融合到mgraph当中
	// update node & GraphToNodeMap & NodeToGraphMap
	// mgraph中新增的节点
	newNodes := make([]graph.NodeID, 0)
	for i := range rematch {
		// 所有节点都是新建而得，节点信息由融合节点的“第一个节点”而来
		gid, nid := rematch[i][0].Gid, rematch[i][0].Nid
		newNode := graph.NewNodeFromCopy(graphMap[gid].Nodes[nid])
		newNodes = append(newNodes, newNode.Nid)

		// update node info
		mgraph.AddNode(newNode)

		// update GraphToNodeMap & NodeToGraphMap
		for j := range rematch[i] {
			gid, nid := rematch[i][j].Gid, rematch[i][j].Nid
			(*mgraph.GraphToNodeMap[gid])[nid] = newNode.Nid
			(*mgraph.NodeToGraphMap[newNode.Nid])[gid] = nid
		}
	}

	// 添加relation node & edges
	for gid, nids := range spnodes {
		g := graphMap[gid]
		for _, nid2 := range nids {
			node := g.Nodes[nid2]
			if node.Label == graph.RelationNode {
				roles := g.GetRelationRoles(nid2)
				r1, r2 := (*mgraph.GraphToNodeMap[gid])[roles[0]], (*mgraph.GraphToNodeMap[gid])[roles[1]]
				if nid1, ok := mgraph.RelationMap[[2]graph.NodeID{r1, r2}]; ok {
					// 相应的relation node已经添加到mgraph中，融合node
					(*mgraph.GraphToNodeMap[gid])[nid2] = nid1
					(*mgraph.NodeToGraphMap[nid1])[gid] = nid2
				} else {
					// 新建一个relation node，并添加相应的edges
					newNode := graph.NewNodeFromCopy(node)
					mgraph.AddNode(newNode)
					newNodes = append(newNodes, newNode.Nid)
					// update relationmap
					mgraph.UpdateRelationMap(r1, r2, newNode.Nid)
					// update GraphToNodeMap & NodeToGraphMap
					(*mgraph.GraphToNodeMap[gid])[nid2] = newNode.Nid
					(*mgraph.NodeToGraphMap[newNode.Nid])[gid] = nid2
					// 添加 edges
					for eid := range *g.OutEdge[nid2] {
						edge := g.Edges[eid]
						s, t := (*mgraph.GraphToNodeMap[gid])[edge.Source()], (*mgraph.GraphToNodeMap[gid])[edge.Target()]
						newEdge := graph.NewEdge(s, t, edge.Name)
						mgraph.AddEdge(newEdge)
					}
				}
			}
		}
	}

	return newNodes
}
