package merge

import (
	"iomgm/bpmatch"
	"iomgm/graph"
	"iomgm/mtree/mtreeconst"
)

// TODO 优化增量融合的做法（或许就改成只拆分一个图的节点，然后与融合图的部分节点进行融合，这样的话效率就会高很多，因为有很多是无用拆分）
// 现在的做法问题在于，随着层次增加，需要拆分的节点会越来越多

// @Description: 增量融合
// @Param mgraph: 待增量修改的融合图
// @Param graphs: mgraph由graphs融合而得
// @Param modifiedGraph: 进行增量修改的graph，graphs中的一个
// @Param relationNodeID: modifiedGraph中，与增量操作中的的relation node id相应的node id
// @Param newNodesOfModifiedGraph: 增量修改的graph中新增的节点（已添加到modifiedGraph中）
// @Param deletedNodesOfModifiedGraph: 增量修改的graph中删除的节点（已从modifiedGraph中删除）
// @Param rematchNodesSelectStrategy: 选取重新融合节点的策略，有两个选项“simple”和“spread”
// @Return newNodesOfmgraph: 增量融合后mgraph中新增的节点
// @Return deletedNodesOfmgraph: 增量融合后mgraph中删除的节点
func IncrementalMergeGraph(mgraph *graph.Graph, graphs map[graph.GraphID]*graph.Graph, modifiedGraph *graph.Graph, relationNodeID graph.NodeID, newNodesOfModifiedGraph []graph.NodeID, deletedNodesOfModifiedGraph []graph.NodeID, rematchNodesSelectStrategy string) (newRelationNodeID graph.NodeID, newNodesOfmgraph []graph.NodeID, deletedNodesOfmgraph []graph.NodeID) {
	// 对mgraph中的节点进行拆分后得到的各个graph中的节点
	var splitedNodes SplitedNodes

	// 拆分mgraph中的相关节点
	switch rematchNodesSelectStrategy {
	case "simple":
		deletedNodesOfmgraph, splitedNodes = selectAndSplitMergedGraphSimple(mgraph, graphs, modifiedGraph, relationNodeID, newNodesOfModifiedGraph, deletedNodesOfModifiedGraph)
	case "spread":
		deletedNodesOfmgraph, splitedNodes = selectAndSplitMergedGraphSpread(mgraph, graphs, modifiedGraph, relationNodeID, newNodesOfModifiedGraph, deletedNodesOfModifiedGraph)
	default:
		panic("error rematch_nodes_select_strategy, please use `simple` or `spread`")
	}

	// rematch and merge
	newNodesOfmgraph = rematchAndMerge(splitedNodes, mgraph, graphs, mtreeconst.IncrementalMergeThreshold)
	newRelationNodeID = (*mgraph.GraphToNodeMap[modifiedGraph.Gid])[relationNodeID]

	// TODO 迭代拆分熵高的节点
	return
}

// @Description: 对一个孤立（新增）节点进行增量融合
// @Param mgraph: 与modifiedGraph相关的融合图，会将孤立节点新建或融合到mgraph中的一个融合节点
// @Param graphs: mgraph由graphs融合而得
// @Param modifiedGraph: 被修改的graph
// @Param isolatedNode: 添加到modifiedGraph的待融合的孤立节点，一定是entity node
// @Param mergeThreshold: 高于该阈值则进行融合
// @Return $0: 增量融合后，mgraph中与isolatedNode相对应的融合节点
// @Return $1: True表示新建了一个融合节点，False表示isoloatedNode被融合到一个已有的融合节点
func IncrementalMergeGraphForIsolatedNode(mgraph *graph.Graph, graphs map[graph.GraphID]*graph.Graph, modifiedGraph *graph.Graph, isolatedNode *graph.Node, mergeThreshold float64) (*graph.Node, bool) {
	// 按照Name相似度进行融合，计算与融合图中所有entity node的相似度，
	// 如果最高值 < threshold，则在融合图中新建一个节点，然后向上一层继续该操作
	// 如果最高值 > threshold，则将该节点融合到相应节点中，终止该过程

	var maxValue float64 = 0
	var maxValueNode *graph.Node = nil

	bp := bpmatch.NewTwoGraphBipartite(mgraph, modifiedGraph)
	for _, node := range mgraph.Nodes {
		if _, ok := (*mgraph.NodeToGraphMap[node.Nid])[modifiedGraph.Gid]; !ok {
			// ok = true 表示已经有该图谱的节点融合到该融合节点上

			value, _ := bp.GetSimilarityFromNodeID(node.Nid, isolatedNode.Nid)
			if value > maxValue {
				maxValue = value
				maxValueNode = node
			}
		}
	}

	if maxValue > mergeThreshold {
		// 将isolated node融合到max value对应的node上

		// 如果modifiedGraph中已经有节点与maxValueNode对齐，不做处理，不清楚实际上会不会有这种情况发生
		if _, ok := (*mgraph.NodeToGraphMap[maxValueNode.Nid])[modifiedGraph.Gid]; ok {
			// 新建一个node
			newNode := graph.NewNodeFromCopy(isolatedNode)
			mgraph.AddNode(newNode)
			(*mgraph.GraphToNodeMap[modifiedGraph.Gid])[isolatedNode.Nid] = newNode.Nid
			(*mgraph.NodeToGraphMap[newNode.Nid])[modifiedGraph.Gid] = isolatedNode.Nid
			return newNode, true
		}

		(*mgraph.GraphToNodeMap[modifiedGraph.Gid])[isolatedNode.Nid] = maxValueNode.Nid
		(*mgraph.NodeToGraphMap[maxValueNode.Nid])[modifiedGraph.Gid] = isolatedNode.Nid
		return maxValueNode, false
	} else {
		// 新建一个node对应isolated node
		newNode := graph.NewNodeFromCopy(isolatedNode)
		mgraph.AddNode(newNode)
		(*mgraph.GraphToNodeMap[modifiedGraph.Gid])[isolatedNode.Nid] = newNode.Nid
		(*mgraph.NodeToGraphMap[newNode.Nid])[modifiedGraph.Gid] = isolatedNode.Nid
		return newNode, true
	}
}

// @Description: 拆分待更新融合图mgraph中的相关节点，simple策略
// @Param relationNodeID: modifiedGraph中，与增量操作中的的relation node id相应的node id
// @Return deletedNodesOfmgraph: mgraph中被拆分（删除）的节点
// @Return splitedNodes: 拆分出来的各个graphs中的节点
func selectAndSplitMergedGraphSimple(mgraph *graph.Graph, graphs map[graph.GraphID]*graph.Graph, modifiedGraph *graph.Graph, relationNodeID graph.NodeID, newNodesOfModifiedGraph []graph.NodeID, deletedNodesOfModifiedGraph []graph.NodeID) (deletedNodesOfmgraph []graph.NodeID, splitedNodes SplitedNodes) {
	// simple策略：拆分所有与deletedNodesOfModifiedGraph以及newNodesOfModifiedGraph相关的节点
	// 如果节点是entity node，所有相连的relation node都需要拆分
	// 如果节点是relation node，只拆分该relation node，不拆分相连的entity node
	// 特别地，newNodesOfModifiedGraph只拆分relation node相关的节点
	// TODO 考虑加入熵高的节点

	// mgraph中需要拆分的节点
	toSplitNids := make([]graph.NodeID, 0)
	toSplitNidsMap := make(map[graph.NodeID]bool)
	for _, nid := range deletedNodesOfModifiedGraph {
		nidInMgraph := (*mgraph.GraphToNodeMap[modifiedGraph.Gid])[nid]

		if _, ok := toSplitNidsMap[nidInMgraph]; !ok {
			toSplitNids = append(toSplitNids, nidInMgraph)
			toSplitNidsMap[nidInMgraph] = true
		}

		if mgraph.Nodes[nidInMgraph].Label == graph.EntityNode {
			// 遍历相连的relation node并加入到待拆分节点序列中
			for eid := range *mgraph.InEdge[nidInMgraph] {
				relationNodeNid := mgraph.Edges[eid].Source()
				if _, ok := toSplitNidsMap[relationNodeNid]; !ok {
					toSplitNids = append(toSplitNids, relationNodeNid)
					toSplitNidsMap[relationNodeNid] = true
				}
			}
		}
	}

	// 拆分与增量操作中的relation的roles相似度高的节点
	// roles := modifiedGraph.GetRelationRoles(relationNodeID)
	// bp := bpmatch.NewTwoGraphBipartite(mgraph, modifiedGraph)
	// for _, role := range roles {

	// 	for _, node := range mgraph.Nodes {
	// 		nid := node.Nid
	// 		value, _ := bp.GetSimilarityFromNodeID(nid, role)
	// 		if value > mtreeconst.NodeSplitThreshold {
	// 			if _, ok := toSplitNidsMap[nid]; !ok {
	// 				toSplitNids = append(toSplitNids, nid)
	// 				toSplitNidsMap[nid] = true
	// 				// 将所有相关的relation node也加进去
	// 				for eidInMgraph := range *mgraph.InEdge[nid] {
	// 					relationNodeNid := mgraph.Edges[eidInMgraph].Source()
	// 					if _, ok := toSplitNidsMap[relationNodeNid]; !ok {
	// 						toSplitNids = append(toSplitNids, relationNodeNid)
	// 						toSplitNidsMap[relationNodeNid] = true
	// 					}
	// 				}
	// 			}
	// 		}
	// 	}
	// }
	roles := modifiedGraph.GetRelationRoles(relationNodeID)
	bp := bpmatch.NewTwoGraphBipartite(mgraph, modifiedGraph)
	for i, role0 := range roles {
		for j, role1 := range roles {
			if i == j {
				continue
			}

			role1, ok := (*mgraph.GraphToNodeMap[modifiedGraph.Gid])[role1]
			if !ok {
				continue
			}

			for eid := range *mgraph.InEdge[role1] {
				relationNid := mgraph.Edges[eid].Source()

				for eid2 := range *mgraph.OutEdge[relationNid] {
					nid := mgraph.Edges[eid2].Target()
					value, _ := bp.GetSimilarityFromNodeID(nid, role0)
					if value > mtreeconst.NodeSplitThreshold {
						if _, ok := toSplitNidsMap[nid]; !ok {
							toSplitNids = append(toSplitNids, nid)
							toSplitNidsMap[nid] = true
							// 将所有相关的relation node也加进去
							for eidInMgraph := range *mgraph.InEdge[nid] {
								relationNodeNid := mgraph.Edges[eidInMgraph].Source()
								if _, ok := toSplitNidsMap[relationNodeNid]; !ok {
									toSplitNids = append(toSplitNids, relationNodeNid)
									toSplitNidsMap[relationNodeNid] = true
								}
							}
						}
					}
				}
			}
		}
	}

	deletedNodesOfmgraph = toSplitNids
	splitedNodes = splitNodeFromMergedGraph(mgraph, toSplitNids)

	// 对splitedNodes中与modifiedGraph相关的部分进行修改，删除deletedNodesOfModifiedGraph中的部分，添加newNodesOfModifiedGraph中的部分
	{
		deletedNodeMap := make(map[graph.NodeID]bool)
		splitedNodeMap := make(map[graph.NodeID]bool)
		for _, nid := range deletedNodesOfModifiedGraph {
			deletedNodeMap[nid] = true
		}
		for _, nid := range splitedNodes[modifiedGraph.Gid] {
			if _, ok := deletedNodeMap[nid]; !ok {
				splitedNodeMap[nid] = true
			}
		}
		for _, nid := range newNodesOfModifiedGraph {
			splitedNodeMap[nid] = true
		}

		tmp := make([]graph.NodeID, 0, len(splitedNodeMap))
		for nid := range splitedNodeMap {
			tmp = append(tmp, nid)
		}
		splitedNodes[modifiedGraph.Gid] = tmp
	}

	return
}

// @Description: 拆分待更新融合图mgraph中的相关节点，spread策略
// @Param relationNodeID: modifiedGraph中，与增量操作中的的relation node id相应的node id
// @Return deletedNodesOfmgraph: mgraph中被拆分（删除）的节点
// @Return splitedNodes: 拆分出来的各个graphs中的节点
func selectAndSplitMergedGraphSpread(mgraph *graph.Graph, graphs map[graph.GraphID]*graph.Graph, modifiedGraph *graph.Graph, relationNodeID graph.NodeID, newNodesOfModifiedGraph []graph.NodeID, deletedNodesOfModifiedGraph []graph.NodeID) (deletedNodesOfmgraph []graph.NodeID, splitedNodes SplitedNodes) {
	// spread策略：拆分所有与deletedNodesOfModifiedGraph以及newNodesOfModifiedGraph相关的节点以及其一跳邻居
	// 如果节点是entity node，所有相连的relation node都需要拆分
	// 如果节点是relation node，所有相连的entity node以及与这些entity node相连的relation node都需要拆分
	// 特别地，newNodesOfModifiedGraph只拆分relation node相关的节点

	// mgraph中需要拆分的节点
	toSplitNids := make([]graph.NodeID, 0)
	toSplitNidsMap := make(map[graph.NodeID]bool)
	// 拆分与deletedNodesOfModifiedGraph相关的节点
	for _, nid := range deletedNodesOfModifiedGraph {
		nidInMgraph := (*mgraph.GraphToNodeMap[modifiedGraph.Gid])[nid]

		if _, ok := toSplitNidsMap[nidInMgraph]; !ok {
			toSplitNids = append(toSplitNids, nidInMgraph)
			toSplitNidsMap[nidInMgraph] = true

			switch mgraph.Nodes[nidInMgraph].Label {
			case graph.EntityNode:
				// 遍历与entity node相连的relation node，并加入到待拆分节点序列中
				for eid := range *mgraph.InEdge[nidInMgraph] {
					relationNodeNid := mgraph.Edges[eid].Source()
					if _, ok := toSplitNidsMap[relationNodeNid]; !ok {
						toSplitNids = append(toSplitNids, relationNodeNid)
						toSplitNidsMap[relationNodeNid] = true
					}
				}
			case graph.RelationNode:
				// 遍历与relation node相连的entity node，以及与这些entity node相连的relation node
				for eid := range *mgraph.OutEdge[nidInMgraph] {
					entityNodeNid := mgraph.Edges[eid].Target()
					if _, ok := toSplitNidsMap[entityNodeNid]; !ok {
						toSplitNids = append(toSplitNids, entityNodeNid)
						toSplitNidsMap[entityNodeNid] = true

						// 遍历相连的relation node
						for eid2 := range *mgraph.InEdge[entityNodeNid] {
							relationNodeNid := mgraph.Edges[eid2].Source()
							if _, ok := toSplitNidsMap[relationNodeNid]; !ok {
								toSplitNids = append(toSplitNids, relationNodeNid)
								toSplitNidsMap[relationNodeNid] = true
							}
						}
					}
				}
			default:
				panic("not expected type of node")
			}
		}
	}

	// 目前该函数只在第一次使用，只有new节点，没有delete节点
	// 拆分与newNodesOfModifiedGraph相关的节点，只拆分relation node相关的节点
	for _, nid1 := range newNodesOfModifiedGraph {
		if modifiedGraph.Nodes[nid1].Label == graph.RelationNode {
			// 注意nid节点本身在mgraph中没有节点与之对应，因为nid节点是新增节点
			for eid := range *modifiedGraph.OutEdge[nid1] {
				nid2 := modifiedGraph.Edges[eid].Target()
				if nidInMgraph, ok := (*mgraph.GraphToNodeMap[modifiedGraph.Gid])[nid2]; ok {
					// mgraph中有与nid2对应的entity node节点nid
					if _, ok := toSplitNidsMap[nidInMgraph]; !ok {
						toSplitNids = append(toSplitNids, nidInMgraph)
						toSplitNidsMap[nidInMgraph] = true
						// 将所有相关的relation node也加进去
						for eidInMgraph := range *mgraph.InEdge[nidInMgraph] {
							relationNodeNid := mgraph.Edges[eidInMgraph].Source()
							if _, ok := toSplitNidsMap[relationNodeNid]; !ok {
								toSplitNids = append(toSplitNids, relationNodeNid)
								toSplitNidsMap[relationNodeNid] = true
							}
						}
					}
				}
			}
		}
	}

	// 拆分与增量操作中的relation的roles相似度高的节点
	roles := modifiedGraph.GetRelationRoles(relationNodeID)
	bp := bpmatch.NewTwoGraphBipartite(mgraph, modifiedGraph)
	for i, role0 := range roles {
		for j, role1 := range roles {
			if i == j {
				continue
			}

			role1, ok := (*mgraph.GraphToNodeMap[modifiedGraph.Gid])[role1]
			if !ok {
				continue
			}

			for eid := range *mgraph.InEdge[role1] {
				relationNid := mgraph.Edges[eid].Source()

				for eid2 := range *mgraph.OutEdge[relationNid] {
					nid := mgraph.Edges[eid2].Target()
					value, _ := bp.GetSimilarityFromNodeID(nid, role0)
					if value > mtreeconst.NodeSplitThreshold {
						if _, ok := toSplitNidsMap[nid]; !ok {
							toSplitNids = append(toSplitNids, nid)
							toSplitNidsMap[nid] = true
							// 将所有相关的relation node也加进去
							for eidInMgraph := range *mgraph.InEdge[nid] {
								relationNodeNid := mgraph.Edges[eidInMgraph].Source()
								if _, ok := toSplitNidsMap[relationNodeNid]; !ok {
									toSplitNids = append(toSplitNids, relationNodeNid)
									toSplitNidsMap[relationNodeNid] = true
								}
							}
						}
					}
				}
			}
		}
	}

	deletedNodesOfmgraph = toSplitNids
	splitedNodes = splitNodeFromMergedGraph(mgraph, toSplitNids)

	// 对splitedNodes中与modifiedGraph相关的部分进行修改，删除deletedNodesOfModifiedGraph中的部分，添加newNodesOfModifiedGraph中的部分
	{
		deletedNodeMap := make(map[graph.NodeID]bool)
		splitedNodeMap := make(map[graph.NodeID]bool)
		for _, nid := range deletedNodesOfModifiedGraph {
			deletedNodeMap[nid] = true
		}
		for _, nid := range splitedNodes[modifiedGraph.Gid] {
			if _, ok := deletedNodeMap[nid]; !ok {
				splitedNodeMap[nid] = true
			}
		}
		for _, nid := range newNodesOfModifiedGraph {
			splitedNodeMap[nid] = true
		}

		tmp := make([]graph.NodeID, 0, len(splitedNodeMap))
		for nid := range splitedNodeMap {
			tmp = append(tmp, nid)
		}
		splitedNodes[modifiedGraph.Gid] = tmp
	}

	return
}
