package mtree

import "github.com/google/uuid"

// id type
type MergeTreeNodeID uuid.UUID

func (id MergeTreeNodeID) String() string {
	return uuid.UUID(id).String()
}
