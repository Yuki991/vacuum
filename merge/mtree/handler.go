package mtree

import (
	"iomgm/common"
	"iomgm/graph"
	"iomgm/mtree/merge"
	"iomgm/mtree/mtreeconst"
)

// @Return $0: 返回新建的entity node的id
func (mt *MergeTree) AddEntity(args *common.AddEntityArgs) (newEntityNodeID graph.NodeID) {
	// args.Gid所在tree node
	mtn := mt.GidToMergeTreeNodeMap[args.Gid]
	// args.Gid相应的graph
	g := mt.GidToGraphMap[args.Gid]

	// common.Log(fmt.Sprintf("%v", mt.GidToGraphMap))
	// common.Log(fmt.Sprintf("%v", mt.GidToMergeTreeNodeMap))
	// common.Log(fmt.Sprintf("%v", args))

	// 新建entity node
	node := graph.NewNode(args.Name, args.Type, graph.EntityNode, args.Attrs)
	g.AddNode(node)
	newEntityNodeID = node.Nid

	// 自下而上在所有相关的graph上新建一个node
	for mtn.Father != nil {
		graphs := make(map[graph.GraphID]*graph.Graph)
		for i := range mtn.Graphs {
			graphs[mtn.Graphs[i].Gid] = mtn.Graphs[i]
		}

		// 增量融合
		newNode, flag := merge.IncrementalMergeGraphForIsolatedNode(mtn.MergedGraph, graphs, g, node, mtreeconst.IncrementalMergeThresholdForIsolatedNode)
		if !flag {
			// node被融合到merged graph中一个已有的节点，可以直接退出
			break
		}

		// 往上一层
		node = newNode
		g = mtn.MergedGraph
		mtn = mtn.Father
	}

	return
}

// TODO relation node对齐较差
// @Return $0: 返回新建的relation node的id
func (mt *MergeTree) AddRelation(args *common.AddRelationArgs) graph.NodeID {
	// args.Gid所在的tree node
	mtn := mt.GidToMergeTreeNodeMap[args.Gid]
	// args.Gid相应的graph
	g := mt.GidToGraphMap[args.Gid]
	// 记录graph增量修改后新增的节点以删除的节点
	newNodes := make([]graph.NodeID, 0)
	deletedNodes := make([]graph.NodeID, 0)
	// 增量融合中选取待重新融合节点的策略
	rematchNodesSelectStrategy := "spread"

	// 新建relation node，并添加相关的edges到graph中
	node := graph.NewNode(args.Name, args.Type, graph.RelationNode, args.Attrs)
	edge1 := graph.NewEdge(node.Nid, args.Nids[0], args.Roles[0])
	edge2 := graph.NewEdge(node.Nid, args.Nids[1], args.Roles[1])
	g.AddRelation(node, edge1, edge2)

	newRelationNodeID, corrRelationNodeID := node.Nid, node.Nid
	newNodes = append(newNodes, node.Nid)

	// 自下而上增量融合
	for mtn.Father != nil {
		graphs := make(map[graph.GraphID]*graph.Graph)
		for i := range mtn.Graphs {
			graphs[mtn.Graphs[i].Gid] = mtn.Graphs[i]
		}

		// 增量融合
		relationNodeID, nextNewNodes, nextDeletedNodes := merge.IncrementalMergeGraph(mtn.MergedGraph, graphs, g, corrRelationNodeID, newNodes, deletedNodes, rematchNodesSelectStrategy)

		// 往上一层
		corrRelationNodeID, newNodes, deletedNodes = relationNodeID, nextNewNodes, nextDeletedNodes
		g = mtn.MergedGraph
		mtn = mtn.Father
		// 切换strategy
		rematchNodesSelectStrategy = "simple"
	}

	return newRelationNodeID
}
