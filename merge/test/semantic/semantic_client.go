package main

import (
	"encoding/json"
	"fmt"
	"net"
)

type SemanticRequest struct {
	Text1 string `json:"text1"`
	Text2 string `json:"text2"`
}

type SemanticResponse struct {
	Text1      string  `json:"text1"`
	Text2      string  `json:"text2"`
	Similarity float64 `json:"similarity"`
}

func main() {
	conn, err := net.Dial("tcp", "127.0.0.1:8000")
	if err != nil {
		fmt.Println("Connect Error: ", err)
		return
	}
	defer conn.Close()

	// data
	reqs := make([]SemanticRequest, 2)
	reqs[0] = SemanticRequest{
		Text1: "冬天",
		Text2: "冬日",
	}
	reqs[1] = SemanticRequest{
		Text1: "冬天",
		Text2: "夏日",
	}
	data, err := json.Marshal(reqs)
	if err != nil {
		fmt.Println("Encode Json Error: ", err)
		return
	}

	// send data
	conn.Write(data)

	// receive response
	buf := make([]byte, 1024)
	n, err := conn.Read(buf)
	if err != nil {
		fmt.Println("Receive Data Error: ", err)
		return
	}

	// parse response
	resp := make([]SemanticResponse, 0, 100)
	err = json.Unmarshal(buf[0:n], &resp)
	if err != nil {
		fmt.Println("Parse Error: ", err)
		return
	}

	fmt.Printf("Response: %v", resp)
}
