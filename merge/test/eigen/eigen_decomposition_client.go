package main

import (
	"encoding/json"
	"fmt"
	"net"
)

type EigenRequest struct {
	Num int         `json:"number"`
	Arr [][]float64 `json:"array"`
}

type EigenResponse struct {
	EigenValues  []float64   `json:"values"`
	EigenVectors [][]float64 `json:"vectors"`
}

func main() {
	conn, err := net.Dial("tcp", "127.0.0.1:8001")
	if err != nil {
		fmt.Println("Connect Error: ", err)
		return
	}
	defer conn.Close()

	// data
	req := EigenRequest{
		Num: 3,
		Arr: [][]float64{{1, 2, 3}, {2, 1, 3}, {3, 3, 1}},
	}
	data, err := json.Marshal(req)
	if err != nil {
		fmt.Println("Encode Json Error: ", err)
		return
	}

	// send data
	conn.Write(data)

	// receive response
	buf := make([]byte, 131072)
	n, err := conn.Read(buf)
	if err != nil {
		fmt.Println("Receive Data Error: ", err)
		return
	}

	// parse response
	resp := EigenResponse{}
	err = json.Unmarshal(buf[0:n], &resp)
	if err != nil {
		fmt.Println("Parse Error: ", err)
		return
	}

	fmt.Printf("Response: %v", resp)
}
