package main

import (
	"bufio"
	"fmt"
	"iomgm/experiment/common"
	"os"
)

func testWrite() {
	element1 := common.ElementData{
		ID:         "test_id1",
		Type:       "test_type1",
		Name:       "test_node1",
		Properties: nil,
	}
	opr1 := common.OperationData{
		OprTime: "1",
		OprType: common.OprCreateEntity,
		UserID:  "user_id1",
		Element: element1,
	}

	element2 := common.ElementData{
		ID:         "test_id2",
		Type:       "test_type2",
		Properties: nil,
		Roles: []common.Role{
			common.Role{RoleName: "role1", ID: "test_id1"},
			common.Role{RoleName: "role1", ID: "test_id1"},
		},
	}
	opr2 := common.OperationData{
		OprTime: "2",
		OprType: common.OprCreateEntity,
		UserID:  "user_id1",
		Element: element2,
	}

	f, err := os.Create("./test.txt")
	if err != nil {
		panic(err)
	}
	defer f.Close()
	w := bufio.NewWriter(f)
	common.WriteOperationData([]*common.OperationData{&opr1, &opr2}, w)
}

func testRead() {
	f, err := os.Open("./test.txt")
	if err != nil {
		panic(err)
	}
	defer f.Close()
	r := bufio.NewReader(f)
	oprs := common.ReadOperationData(r)
	fmt.Printf("%v\n", *oprs[0])
}

func main() {
	testWrite()
	testRead()
}
