package common

import (
	"bufio"
	"encoding/json"
)

const (
	OprCreateEntity   = "createEntity"
	OprCreateRelation = "createRelation"
)

type Role struct {
	RoleName string `json:"role"`
	ID       string `json:"inst_id"`
}

// 表示一个entity或者relation，id/type/properties是通用字段，name是entity特有字段，roles是relation特有字段
type ElementData struct {
	ID         string            `json:"id"`
	Type       string            `json:"type_id"`
	Name       string            `json:"name,omitempty"`
	Properties map[string]string `json:"properties,omitempty"`
	Roles      []Role            `json:"roles,omitempty"`
}

type OperationData struct {
	OprTime string      `json:"time"`
	OprType string      `json:"operation"` // `createEntity` or `createRelation`
	UserID  string      `json:"user_id"`
	Element ElementData `json:"data"`
}

func ReadOperationData(reader *bufio.Reader) []*OperationData {
	oprs := make([]*OperationData, 0)
	for {
		opr, ok := ReadOneOperationData(reader)
		if !ok {
			break
		}
		oprs = append(oprs, opr)
	}
	return oprs
}

// @Description: 读入并返回一条operation data
// @Return $0: operation data
// @Return $1: 读入是否成功
func ReadOneOperationData(reader *bufio.Reader) (*OperationData, bool) {
	data, _, err := reader.ReadLine()
	if err != nil {
		// 读到文件末尾了
		return nil, false
	}

	var opr OperationData
	if err = json.Unmarshal(data, &opr); err != nil {
		panic("json unmarshal error")
	}

	return &opr, true
}

// @Description: 打印OperationData，格式上每一行是一个json，对应一个OperationData
func WriteOperationData(oprs []*OperationData, writer *bufio.Writer) {
	for _, opr := range oprs {
		data, _ := json.Marshal(opr)
		writer.Write(data)
		writer.WriteString("\n")
	}
	writer.Flush()
}

// @Description: 打印一条OperationData，占一行，格式为json
func WriteOneOperationData(opr *OperationData, writer *bufio.Writer) {
	data, _ := json.Marshal(opr)
	writer.Write(data)
	writer.WriteString("\n")
	writer.Flush()
}
