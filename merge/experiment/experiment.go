package main

import (
	"bufio"
	"fmt"
	globalcommon "iomgm/common"
	"iomgm/experiment/common"
	"iomgm/graph"
	"iomgm/mtree"
	"iomgm/mtree/merge"
	"math"
	"os"
	"path/filepath"
	"sort"
	"time"
	"unsafe"

	"github.com/go-ini/ini"
)

var (
	// 是否打印各个graph的信息
	printInfoFlag bool
	// 记录增量操作的指标（准确率、所用时间、错误融合节点的MR&MRR）
	exptRsPerOprFilepath   string
	exptRsPerOprFile       *os.File
	exptRsPerOprFileWriter *bufio.Writer
	// 增量操作结果的记录间隔
	exptRecordInterval int = 100
	onlyOprTimeFlag        = false
	// 记录最终结果（实验参数 & 最终实验指标）
	finalExptRsFilepath   string
	finalExptRsFile       *os.File
	finalExptRsFileWriter *bufio.Writer
	// 临时文件
	tmpFilepath   string
	tmpFile       *os.File
	tmpFileWriter *bufio.Writer
	tmpFlag       bool = false
	// 运行时间记录文件
	oprTimeFilePath   string
	oprTimeFile       *os.File
	oprTimeFileWriter *bufio.Writer
)

func init() {
	printInfoFlag = false
	exptRsPerOprFilepath = filepath.FromSlash("./experiment_result/experiment_result_per_operation.txt")
	finalExptRsFilepath = filepath.FromSlash("./experiment_result/experiment_info.txt")
	tmpFilepath = filepath.FromSlash("./experiment_result/tmp_file.txt")
	oprTimeFilePath = filepath.FromSlash("./experiment_result/operation_time.txt")
}

func StartExperiment(experimentConfigPath string) {
	// 初始化存储中间信息的文件夹
	_ = os.RemoveAll(filepath.FromSlash("./experiment_result"))
	_ = os.Mkdir(filepath.FromSlash("./experiment_result"), 0755)
	_ = os.Mkdir(filepath.FromSlash("./experiment_result/incremental"), 0755)

	// 打开记录实验结果的文件
	exptRsPerOprFile, _ = os.Create(exptRsPerOprFilepath)
	exptRsPerOprFileWriter = bufio.NewWriter(exptRsPerOprFile)
	defer exptRsPerOprFile.Close()
	finalExptRsFile, _ = os.Create(finalExptRsFilepath)
	finalExptRsFileWriter = bufio.NewWriter(finalExptRsFile)
	defer finalExptRsFile.Close()
	tmpFile, _ = os.Create(tmpFilepath)
	tmpFileWriter = bufio.NewWriter(tmpFile)
	defer tmpFile.Close()
	oprTimeFile, _ = os.Create(oprTimeFilePath)
	oprTimeFileWriter = bufio.NewWriter(oprTimeFile)
	defer oprTimeFile.Close()

	// 读取实验参数
	experimentConfigs := openConfigFile(experimentConfigPath)
	// 读取并构建一组初始图
	graphs, graphidMap, nodeidMaps, relationidMaps := readAndBuildInitGraphs(experimentConfigs)
	// readAndBuildInitGraphsDebug(graphs, graphidMap, nodeidMaps, relationidMaps)
	globalcommon.Log("finish building init graphs")
	// 构建merge tree
	mt := buildMergeTree(graphs, experimentConfigs)
	globalcommon.Log("finish building merge tree")
	// 读取并处理增量更新操作
	readAndProcessIncrementalOperation(mt, graphidMap, nodeidMaps, relationidMaps, graphs, experimentConfigs)
	globalcommon.Log("finish processing incremental operations")
}

// @Description: 读取参数
func openConfigFile(path string) *ini.File {
	configs, err := ini.Load(path)
	if err != nil {
		panic("experiment config read error")
	}

	return configs
}

// @Description: 读取初始图的数据并构建初始图
// @Return $0: 一组初始图
// @Return $1: 输入数据的graph的id（user id）到算法执行过程中实际的graph的id的映射表
// @Return $2: 输入数据的graphs的entity id到实际entity_node id的一组映射表
// @Return $3: 输入数据的graphs的relation id到实际relation_node id的一组映射表
func readAndBuildInitGraphs(experimentConfigs *ini.File) ([]*graph.Graph, *map[string]graph.GraphID, *map[string]*map[string]graph.NodeID, *map[string]*map[string]graph.NodeID) {
	// read data config
	dataConfigPath := experimentConfigs.Section("experiment_param").Key("ExperimentDataConfigPath").String()
	dataPath := experimentConfigs.Section("experiment_param").Key("ExperimentDataPath").String()
	dataConfigs, err := ini.Load(dataConfigPath)
	if err != nil {
		panic("data config read error")
	}
	graphNum, _ := dataConfigs.Section("varied_graph_generation_param").Key("VariedGraphNum").Int()
	fileNameOfOperationsForInitGraph := dataConfigs.Section("file_name_param").Key("FileNameOfOperationsForInitGraph").String()

	// build init graphs
	graphs := make([]*graph.Graph, graphNum)
	graphidMap := make(map[string]graph.GraphID)
	nodeidMaps := make(map[string]*map[string]graph.NodeID, graphNum)
	relationidMaps := make(map[string]*map[string]graph.NodeID, graphNum)
	for i := 0; i < graphNum; i++ {
		dataPath := fmt.Sprintf("%v/%v%v.json", dataPath, fileNameOfOperationsForInitGraph, i)
		userid, g, nodeidMap, relationidMap := readAndBuildOneInitGraph(dataPath)

		graphs[i] = g
		graphidMap[userid] = graphs[i].Gid
		nodeidMaps[userid] = nodeidMap
		relationidMaps[userid] = relationidMap
	}

	// 打印实验信息
	finalExptRsFileWriter.WriteString("init_graph_entity_num,")
	for i := 0; i < graphNum; i++ {
		finalExptRsFileWriter.WriteString(fmt.Sprintf("%v,", graphs[i].GetEntityNodeNum()))
	}
	finalExptRsFileWriter.WriteString("\n")
	finalExptRsFileWriter.WriteString("init_graph_relation_num,")
	for i := 0; i < graphNum; i++ {
		finalExptRsFileWriter.WriteString(fmt.Sprintf("%v,", graphs[i].GetRelationNodeNum()))
	}
	finalExptRsFileWriter.WriteString("\n")
	finalExptRsFileWriter.Flush()

	return graphs, &graphidMap, &nodeidMaps, &relationidMaps
}

func readAndBuildInitGraphsDebug(graphs []*graph.Graph, graphidMap *map[string]graph.GraphID, nodeidMaps *map[string]*map[string]graph.NodeID, relationidMaps *map[string]*map[string]graph.NodeID) {
	f, err := os.Create(filepath.FromSlash("./debug/read_and_build_init_graphs.txt"))
	if err != nil {
		panic(err)
	}
	defer f.Close()
	w := bufio.NewWriter(f)

	for i, g := range graphs {
		w.WriteString(fmt.Sprintf("graph%v: { node_num: %v, edge_num: %v }\n", i, len(g.Nids), len(g.Eids)))
		w.Flush()
	}

	w.WriteString(fmt.Sprintf("%v\n", graphidMap))
	w.Flush()

	for k, v := range *nodeidMaps {
		w.WriteString(fmt.Sprintf("{userid: %v}, {nodeidmap: %v}\n", k, v))
		w.Flush()
	}

	for k, v := range *relationidMaps {
		w.WriteString(fmt.Sprintf("{userid: %v}, {relationidMap: %v}\n", k, v))
		w.Flush()
	}

	for userid, mp := range *nodeidMaps {
		w.WriteString(fmt.Sprintf("graph%v nodes: [ ", userid))
		for k := range *mp {
			w.WriteString(fmt.Sprintf("%v ", k))
		}
		w.WriteString("]\n")
		w.Flush()
	}
}

// @Description: 读取并构建一个初始图
// @Return $0: 该初始图相应的user id，标识该graph属于哪一个user，也是graph的一个唯一标识
// @Return $1: 构建出来的初始图
func readAndBuildOneInitGraph(dataPath string) (string, *graph.Graph, *map[string]graph.NodeID, *map[string]graph.NodeID) {
	// open file
	f, err := os.Open(filepath.FromSlash(dataPath))
	if err != nil {
		panic("read and build one init graph error")
	}
	defer f.Close()
	reader := bufio.NewReader(f)

	// 初始化
	var userid string
	g := graph.NewEmptyGraph()
	nodeidMap := make(map[string]graph.NodeID)
	relationidMap := make(map[string]graph.NodeID)

	// 读取并处理每一个operation
	for {
		opr, ok := common.ReadOneOperationData(reader)
		if !ok { // 读到文件末尾
			break
		}
		userid = opr.UserID

		switch opr.OprType {
		case common.OprCreateEntity:
			// 新建Entity Node
			node := graph.NewNode(opr.Element.Name, opr.Element.Type, graph.EntityNode, nil)
			g.AddNode(node)
			nodeidMap[opr.Element.ID] = node.Nid
		case common.OprCreateRelation:
			// 新建Relation Node
			node := graph.NewNode(opr.Element.Name, opr.Element.Type, graph.RelationNode, nil)
			relationidMap[opr.Element.ID] = node.Nid
			// 新建两条Edge，都是由Relation Node指向Entity Node
			source := nodeidMap[opr.Element.Roles[0].ID]
			target := nodeidMap[opr.Element.Roles[1].ID]
			edge1 := graph.NewEdge(node.Nid, source, opr.Element.Roles[0].RoleName)
			edge2 := graph.NewEdge(node.Nid, target, opr.Element.Roles[1].RoleName)
			g.AddRelation(node, edge1, edge2)
		default:
			panic(fmt.Sprintf("unrecognized operation type: %v", opr.OprType))
		}
	}

	return userid, g, &nodeidMap, &relationidMap
}

func buildMergeTree(graphs []*graph.Graph, configs *ini.File) *mtree.MergeTree {
	mergeTreeInitMode := configs.Section("experiment_param").Key("MergeTreeInitMode").String()
	start := time.Now()
	mt, _ := mtree.NewMergeTree(graphs, mergeTreeInitMode)
	finalExptRsFileWriter.WriteString(fmt.Sprintf("build_time,%v,\n", time.Since(start).Milliseconds()))
	finalExptRsFileWriter.Flush()
	return mt
}

// @Description: 读取并处理增量更新操作
// @Param mt: merge tree
// @Param graphidMap: 输入数据的graph id到实际graph id的映射表
// @Param nodeidMaps: nodeidMaps[userid]表示userid对应的graph中的entity id到实际entity_node id的映射表
// @Param relationMaps: relationMaps[userid]表示userid对应的graph中的relation_id到实际relation_node id的`一组`映射表
// @Param graphs: 输入知识图谱的指针
// @Param experimentConfigs: experiment configs
func readAndProcessIncrementalOperation(mt *mtree.MergeTree, graphidMap *map[string]graph.GraphID, nodeidMaps *map[string]*map[string]graph.NodeID, relationidMaps *map[string]*map[string]graph.NodeID, graphs []*graph.Graph, experimentConfigs *ini.File) {
	// read data config
	dataConfigPath := experimentConfigs.Section("experiment_param").Key("ExperimentDataConfigPath").String()
	dataPath := experimentConfigs.Section("experiment_param").Key("ExperimentDataPath").String()
	dataConfigs, err := ini.Load(dataConfigPath)
	if err != nil {
		panic("data config read error")
	}
	fileNameOfFinalIncrementOperation := dataConfigs.Section("file_name_param").Key("FileNameOfFinalIncrementOperation").String()

	// open file
	dataPath = fmt.Sprintf("%v/%v.json", dataPath, fileNameOfFinalIncrementOperation)
	f, err := os.Open(filepath.FromSlash(dataPath))
	if err != nil {
		panic("read and build one init graph error")
	}
	defer f.Close()
	reader := bufio.NewReader(f)

	// 转换create entity的格式
	convertFormatOfCreateEntity := func(opr *common.OperationData) *globalcommon.AddEntityArgs {
		return &globalcommon.AddEntityArgs{
			Gid:   (*graphidMap)[opr.UserID],
			Attrs: opr.Element.Properties,
			Name:  opr.Element.Name,
			Type:  opr.Element.Type,
		}
	}

	// 转换create relation的格式
	convertFormatOfCreateRelation := func(opr *common.OperationData) *globalcommon.AddRelationArgs {
		args := &globalcommon.AddRelationArgs{
			Gid:   (*graphidMap)[opr.UserID],
			Nids:  make([]graph.NodeID, len(opr.Element.Roles)),
			Roles: make([]string, len(opr.Element.Roles)),
			Attrs: opr.Element.Properties,
			Name:  opr.Element.Name,
			Type:  opr.Element.Type,
		}
		for i, role := range opr.Element.Roles {
			// 从input data的id转换到相应的node的id
			args.Nids[i] = (*(*nodeidMaps)[opr.UserID])[role.ID]
			args.Roles[i] = role.RoleName
		}
		return args
	}

	// 向 实验结果文件 打印各列数据的标题
	exptRsPerOprFileWriter.WriteString(
		"round,operation_time,accuracy,average_precision,average_recall,mean_rank,mean_reciprocal_rank,\n")

	// 计数器 & 计时器
	var round int
	var oprTime time.Duration
	var timeCount time.Duration = 0
	var timeCountPerHundred time.Duration = 0
	var totalTimeCount time.Duration = 0
	var createEntityOprCnt int = 0
	var createRelationOprCnt int = 0
	var createEntityTimeCount time.Duration = 0
	var createRelationTimeCount time.Duration = 0

	// 统计增量操作前的数据
	calculateExpermentIndexes(mt, graphidMap, nodeidMaps, relationidMaps, 0, 0)

	// 一条条地读入输入数据并进行处理
	for round = 1; ; round++ {
		opr, ok := common.ReadOneOperationData(reader)
		if !ok { // 读到文件末尾
			break
		}

		globalcommon.Log(fmt.Sprintf("start prcocess opreration: %v", opr))

		start := time.Now()
		switch opr.OprType {
		case common.OprCreateEntity:
			// 添加实体
			args := convertFormatOfCreateEntity(opr)
			newEntityNodeID := mt.AddEntity(args)
			// 更新nodeidMaps
			(*(*nodeidMaps)[opr.UserID])[opr.Element.ID] = newEntityNodeID
			// 计数
			createEntityOprCnt++
		case common.OprCreateRelation:
			// 添加关系
			args := convertFormatOfCreateRelation(opr)
			newRelationNodeID := mt.AddRelation(args)
			// 更新relationidMaps
			(*(*relationidMaps)[opr.UserID])[opr.Element.ID] = newRelationNodeID
			// 计数
			createRelationOprCnt++
		default:
			panic(fmt.Sprintf("unrecognized operation type: %v", opr.OprType))
		}

		// 单次运行时间
		t := time.Since(start)
		// 累加
		timeCount += t
		timeCountPerHundred += t
		totalTimeCount += t
		switch opr.OprType {
		case common.OprCreateEntity:
			createEntityTimeCount += t
		case common.OprCreateRelation:
			createRelationTimeCount += t
		}

		// 打印操作时间（每100条统计一次，单位为ms）
		if round%100 == 0 {
			// oprTimeFileWriter.WriteString(
			// 	fmt.Sprintf("%v,%v,\n", round, timeCountPerHundred.Milliseconds()))
			oprTimeFileWriter.WriteString(
				fmt.Sprintf("%v,%v,\n", round, totalTimeCount.Milliseconds()))
			oprTimeFileWriter.Flush()
			// 打印运行时间到控制台
			fmt.Printf("Round %v - %v, time: %v\n", round-99, round, timeCountPerHundred)
			timeCountPerHundred = 0
		}

		// 每exptRecordInterva次操作计算一次实验指标
		if !onlyOprTimeFlag && round%exptRecordInterval == 0 {
			// 上exptRecordInterval条数据的运行时间
			oprTime = timeCount
			timeCount = 0
			// 统计并打印实验指标
			calculateExpermentIndexes(mt, graphidMap, nodeidMaps, relationidMaps, round, oprTime)
		}

		// printMergedResultWithFilePath(mt, fmt.Sprintf("./experiment_result/incremental/merge_result%v.txt", i))
	}

	// 打印剩余round的实验指标
	// tmpFlag = true
	// oprTime = timeCount
	// calculateExpermentIndexes(mt, graphidMap, nodeidMaps, relationidMaps, round, oprTime)

	// 打印实验信息
	finalExptRsFileWriter.WriteString(
		fmt.Sprintf("record_interval,%v,\n", exptRecordInterval))
	finalExptRsFileWriter.WriteString(
		fmt.Sprintf("input_graph_number,%v,\n", len(graphs)))
	finalExptRsFileWriter.WriteString(
		fmt.Sprintf("total_operation_number,%v,\n", round))
	finalExptRsFileWriter.WriteString(
		fmt.Sprintf("total_entity_creation_number,%v,\n", createEntityOprCnt))
	finalExptRsFileWriter.WriteString(
		fmt.Sprintf("total_relation_creation_number,%v,\n", createRelationOprCnt))
	finalExptRsFileWriter.WriteString(
		fmt.Sprintf("total_operation_time,%v,\n", totalTimeCount.Milliseconds()))
	finalExptRsFileWriter.WriteString(
		fmt.Sprintf("total_entity_creation_time,%v,\n", createEntityTimeCount.Milliseconds()))
	finalExptRsFileWriter.WriteString(
		fmt.Sprintf("total_relation_creation_time,%v,\n", createRelationTimeCount.Milliseconds()))
	finalExptRsFileWriter.WriteString(
		fmt.Sprintf("result_entity_num,%v,\n", mt.Root.MergedGraph.GetEntityNodeNum()))
	finalExptRsFileWriter.WriteString(
		fmt.Sprintf("result_relation_num,%v,\n", mt.Root.MergedGraph.GetRelationNodeNum()))
	finalExptRsFileWriter.Flush()
	// 打印每个graph最终大小
	finalExptRsFileWriter.WriteString("result_graph_entity_num,")
	for i := range graphs {
		finalExptRsFileWriter.WriteString(fmt.Sprintf("%v,", graphs[i].GetEntityNodeNum()))
	}
	finalExptRsFileWriter.WriteString("\n")
	finalExptRsFileWriter.WriteString("result_graph_relation_num,")
	for i := range graphs {
		finalExptRsFileWriter.WriteString(fmt.Sprintf("%v,", graphs[i].GetRelationNodeNum()))
	}
	finalExptRsFileWriter.WriteString("\n")
	finalExptRsFileWriter.Flush()

	// print info
	if printInfoFlag {
		printMergeTreeInfo(mt)
		printMergedResult(mt)
		printAllNodeOfAllGraphInMergeTree(mt)
		printRelationInMergeGraph(mt)
	}

	globalcommon.Log("finish process operations")
}

func printMergedResultWithFilePath(mt *mtree.MergeTree, path string) {
	f, err := os.Create(filepath.FromSlash(path))
	if err != nil {
		panic(err)
	}
	defer f.Close()
	w := bufio.NewWriter(f)

	mergedGraph := mt.Root.MergedGraph
	graphsMap := mt.GidToGraphMap
	var printFunc func(g *graph.Graph, nid graph.NodeID, arr *[]string)
	printFunc = func(g *graph.Graph, nid graph.NodeID, arr *[]string) {
		if len(*g.NodeToGraphMap[nid]) == 0 {
			*arr = append(*arr, g.Nodes[nid].Name)
			return
		}

		for gid2, nid2 := range *g.NodeToGraphMap[nid] {
			printFunc(graphsMap[gid2], nid2, arr)
		}
	}

	result := make([][]string, 0)
	for nid, node := range mergedGraph.Nodes {
		if node.Label == graph.EntityNode {
			arr := make([]string, 0)
			printFunc(mergedGraph, nid, &arr)
			result = append(result, arr)
		}
	}

	sort.Slice(result, func(i, j int) bool {
		return result[i][0] < result[j][0]
	})
	for i := range result {
		for j := range result[i] {
			w.WriteString(fmt.Sprintf("%v ", result[i][j]))
		}
		w.WriteString("\n")
	}
	w.Flush()
}

func printMergeTreeInfo(mt *mtree.MergeTree) {
	f, err := os.Create(filepath.FromSlash("./experiment_result/merge_tree_info.txt"))
	if err != nil {
		panic(err)
	}
	defer f.Close()
	w := bufio.NewWriter(f)

	var printFunc func(mt *mtree.MergeTreeNode)
	printFunc = func(mtn *mtree.MergeTreeNode) {
		w.WriteString(fmt.Sprintf("%v graph_num:%v child_num:%v graphs:%v childs:%v\n", unsafe.Pointer(mtn), len(mtn.Graphs), len(mtn.Childs), mtn.Graphs, mtn.Childs))
		if mtn.Childs != nil {
			for _, child := range mtn.Childs {
				printFunc(child)
			}
		}
	}
	printFunc(mt.Root)
	w.Flush()
}

func printMergedResult(mt *mtree.MergeTree) {
	f, err := os.Create(filepath.FromSlash("./experiment_result/merged_result.txt"))
	if err != nil {
		panic(err)
	}
	defer f.Close()
	w := bufio.NewWriter(f)

	mergedGraph := mt.Root.MergedGraph
	graphsMap := mt.GidToGraphMap
	var printFunc func(g *graph.Graph, nid graph.NodeID, arr *[]string)
	printFunc = func(g *graph.Graph, nid graph.NodeID, arr *[]string) {
		if len(*g.NodeToGraphMap[nid]) == 0 {
			*arr = append(*arr, g.Nodes[nid].Name)
			return
		}

		for gid2, nid2 := range *g.NodeToGraphMap[nid] {
			printFunc(graphsMap[gid2], nid2, arr)
		}
	}

	result := make([][]string, 0)
	for nid, node := range mergedGraph.Nodes {
		if node.Label == graph.EntityNode {
			arr := make([]string, 0)
			printFunc(mergedGraph, nid, &arr)
			result = append(result, arr)
		}
	}

	sort.Slice(result, func(i, j int) bool {
		return result[i][0] < result[j][0]
	})
	for i := range result {
		for j := range result[i] {
			w.WriteString(fmt.Sprintf("%v ", result[i][j]))
		}
		w.WriteString("\n")
	}
	w.Flush()
}

func printAllNodeOfAllGraphInMergeTree(mt *mtree.MergeTree) {
	printGraphFunc := func(g *graph.Graph, depth int) {
		fileName := fmt.Sprintf("./experiment_result/graph_depth%v_%v.txt", depth, unsafe.Pointer(g))
		fileName = filepath.FromSlash(fileName)
		f, err := os.Create(fileName)
		if err != nil {
			panic(err)
		}
		defer f.Close()
		w := bufio.NewWriter(f)

		entityNodeNum := 0
		for _, node := range g.Nodes {
			if node.Label == graph.EntityNode {
				entityNodeNum++
			}
		}

		w.WriteString(fmt.Sprintf("entity_node_num:%v relation_node_num:%v\n", entityNodeNum, len(g.Nodes)-entityNodeNum))
		arr := make([]string, 0, entityNodeNum)
		for _, node := range g.Nodes {
			if node.Label == graph.EntityNode {
				arr = append(arr, node.Name)
			}
		}
		sort.Slice(arr, func(i, j int) bool {
			return arr[i] < arr[j]
		})
		for i := range arr {
			w.WriteString(fmt.Sprintf("%v\n", arr[i]))
		}
		w.Flush()
	}

	var printFunc func(mtn *mtree.MergeTreeNode, depth int)
	printFunc = func(mtn *mtree.MergeTreeNode, depth int) {
		printGraphFunc(mtn.MergedGraph, depth)
		if mtn.Childs != nil {
			for _, child := range mtn.Childs {
				printFunc(child, depth+1)
			}
		} else {
			for i := range mtn.Graphs {
				printGraphFunc(mtn.Graphs[i], depth+1)
			}
		}
	}
	printFunc(mt.Root, 0)
}

func printRelationInMergeGraph(mt *mtree.MergeTree) {
	f, err := os.Create(filepath.FromSlash("./experiment_result/relation_in_merge_graph.txt"))
	if err != nil {
		panic(err)
	}
	defer f.Close()
	w := bufio.NewWriter(f)

	arr := make([][]string, 0)
	g := mt.Root.MergedGraph
	for nid, node := range g.Nodes {
		if node.Label == graph.RelationNode {
			r := make([]string, 2)
			for eid := range *g.OutEdge[nid] {
				edge := g.Edges[eid]
				if edge.Name == "s" || edge.Name == "t" {
					if edge.Name == "s" {
						r[0] = g.Nodes[edge.Target()].Name
					} else {
						r[1] = g.Nodes[edge.Target()].Name
					}
				}
			}
			arr = append(arr, r)
		}
	}

	sort.Slice(arr, func(i, j int) bool {
		if arr[i][0] != arr[j][0] {
			return arr[i][0] < arr[j][0]
		} else {
			return arr[i][1] < arr[j][1]
		}
	})
	for i := range arr {
		w.WriteString(fmt.Sprintf("%v %v\n", arr[i][0], arr[i][1]))
	}
	w.Flush()
}

// @Description: 统计当前时间点的实验指标（统计速度较慢）
// @Param mt: merge tree
// @Param graphidMap: 输入数据的graph id到实际graph id的映射表
// @Param nodeidMaps: nodeidMaps[userid]表示userid对应的graph中的entity id到实际entity_node id的映射表
// @Param relationMaps: relationMaps[userid]表示userid对应的graph中的relation_id到实际relation_node id的`一组`映射表
func calculateExpermentIndexes(mt *mtree.MergeTree, graphidMap *map[string]graph.GraphID, nodeidMaps *map[string]*map[string]graph.NodeID, relationidMaps *map[string]*map[string]graph.NodeID, round int, oprTime time.Duration) {
	// TODO 可以优化一下
	// 统计所有出现的node id & relation id，记录都来自哪些user
	nids := make(map[string][]string)
	rids := make(map[string][]string)
	for uid, mp := range *nodeidMaps {
		for nid := range *mp {
			nids[nid] = append(nids[nid], uid)
		}
	}
	for uid, mp := range *relationidMaps {
		for rid := range *mp {
			rids[rid] = append(rids[rid], uid)
		}
	}

	// 最终融合知识图谱中的entity数目 & relation数目
	entityNum, relationNum := 0, 0
	// 记录融合节点来自哪些节点，[2]string{userid/graphid, nodeid}
	entityNodeList := make(map[graph.NodeID][][2]string)
	relationNodeList := make(map[graph.NodeID][][2]string)
	// 最终融合知识图谱的融合节点的熵值
	nodeEntropy, _ := mt.GetAllNodeEntropy(mt.Root.MergedGraph)
	// 错误融合的数目
	wrongEntityCount, wrongRelationCount := 0, 0
	// MR & MRR
	mrSumEntity, mrSumRelation, mrrSumEntity, mrrSumRelation := 0.0, 0.0, 0.0, 0.0

	// 记录熵值排名
	entityNodeEntropy := make([]merge.NodeEntropy, 0)
	relationNodeEntropy := make([]merge.NodeEntropy, 0)
	entityNodeEntropyRank := make(map[graph.NodeID]int)
	relationNodeEntropyRank := make(map[graph.NodeID]int)
	for _, e := range nodeEntropy {
		node := mt.Root.MergedGraph.Nodes[e.Nid]
		if node.Label == graph.EntityNode {
			entityNodeEntropy = append(entityNodeEntropy, e)
		} else if node.Label == graph.RelationNode {
			relationNodeEntropy = append(relationNodeEntropy, e)
		}
	}
	tmpCnt := 1
	for i, e := range entityNodeEntropy {
		if i > 0 && math.Abs(entityNodeEntropy[i].E-entityNodeEntropy[i-1].E) > 1e-5 {
			tmpCnt++
		}
		entityNodeEntropyRank[e.Nid] = tmpCnt
	}
	tmpCnt = 1
	for i, e := range relationNodeEntropy {
		if i > 0 && math.Abs(relationNodeEntropy[i].E-relationNodeEntropy[i-1].E) > 1e-5 {
			tmpCnt++
		}
		relationNodeEntropyRank[e.Nid] = tmpCnt
	}
	// 融合节点的熵值排名（高的在前）
	// entropyRank := make(map[graph.NodeID]int)
	// rkCnt := 1
	// for i, e := range nodeEntropy {
	// 	if i > 0 && math.Abs(nodeEntropy[i].E-nodeEntropy[i-1].E) > 1e-5 {
	// 		rkCnt++
	// 		// rkCnt = i + 1
	// 	}
	// 	entropyRank[e.Nid] = rkCnt
	// }

	// 统计最终融合图谱中的entity & relation数目
	finalMergedGraph := mt.Root.MergedGraph
	for _, node := range finalMergedGraph.Nodes {
		if node.Label == graph.EntityNode {
			entityNum++
		} else if node.Label == graph.RelationNode {
			relationNum++
		}
	}

	// 统计两两graph之间的正确配对数
	entityCqtMatchNumGT := make(map[[2]string]int)
	relationCqtMatchNumGT := make(map[[2]string]int)
	for _, l := range nids {
		for _, uid1 := range l {
			for _, uid2 := range l {
				entityCqtMatchNumGT[[2]string{uid1, uid2}]++
			}
		}
	}
	for _, l := range rids {
		for _, uid1 := range l {
			for _, uid2 := range l {
				relationCqtMatchNumGT[[2]string{uid1, uid2}]++
			}
		}
	}

	// 统计融合节点来自哪些节点（entity）
	for nidInString, uids := range nids {
		for _, uid := range uids {
			// 节点相应的GraphNodeID
			nid := (*(*nodeidMaps)[uid])[nidInString]
			// 向上遍历，查找最终融合图谱中该节点对应的GraphNodeID
			tnode := mt.GidToMergeTreeNodeMap[(*graphidMap)[uid]]
			nid = (*tnode.MergedGraph.GraphToNodeMap[(*graphidMap)[uid]])[nid]
			for tnode.Father != mt.Root {
				nid = (*tnode.Father.MergedGraph.GraphToNodeMap[tnode.MergedGraph.Gid])[nid]
				tnode = tnode.Father
			}
			// append
			entityNodeList[nid] = append(entityNodeList[nid], [2]string{uid, nidInString})
		}
	}
	// 统计融合节点来自哪些节点（relation）
	for nidInString, uids := range rids {
		for _, uid := range uids {
			// 节点相应的GraphNodeID
			nid := (*(*relationidMaps)[uid])[nidInString]
			// 向上遍历，查找最终融合图谱中该节点对应的GraphNodeID
			tnode := mt.GidToMergeTreeNodeMap[(*graphidMap)[uid]]
			nid = (*tnode.MergedGraph.GraphToNodeMap[(*graphidMap)[uid]])[nid]
			for tnode.Father != mt.Root {
				nid = (*tnode.Father.MergedGraph.GraphToNodeMap[tnode.MergedGraph.Gid])[nid]
				tnode = tnode.Father
			}
			// append
			relationNodeList[nid] = append(relationNodeList[nid], [2]string{uid, nidInString})
		}
	}

	// TODO 这个统计指标感觉得改一下，按照现在这种计算方式，全部不融合正确率是100%
	// 统计融合节点的正确率（只有将不该在一起节点的融合到一起才算错，“少了”不算错）
	// entity
	for nid, l := range entityNodeList {
		flag := true
		for i := 1; i < len(l); i++ {
			if l[i][1] != l[i-1][1] {
				flag = false
				break
			}
		}

		if flag {
			// 正确，所有节点都该融合在一起
		} else {
			// 错误
			wrongEntityCount++
			i := entityNodeEntropyRank[nid]
			// i := entropyRank[nid]
			mrSumEntity += float64(i)
			mrrSumEntity += 1.0 / float64(i)
		}
	}
	// relation
	for nid, l := range relationNodeList {
		flag := true
		for i := 1; i < len(l); i++ {
			if l[i][1] != l[i-1][1] {
				flag = false
				break
			}
		}

		if flag {
			// 正确，所有节点都该融合在一起
		} else {
			// 错误
			wrongRelationCount++
			i := relationNodeEntropyRank[nid]
			// i := entropyRank[nid]
			mrSumRelation += float64(i)
			mrrSumRelation += 1.0 / float64(i)
		}
	}

	if tmpFlag {
		// 打印信息到临时文件
		tmpFileWriter.WriteString(fmt.Sprintf("Entity Node Entropy List:\n"))
		for _, e := range entityNodeEntropy {
			tmpFileWriter.WriteString(
				fmt.Sprintf("%v %v %v\n", entityNodeEntropyRank[e.Nid], e.Nid, e.E))
		}
		tmpFileWriter.WriteString("\n")

		tmpFileWriter.WriteString(fmt.Sprintf("Relation Node Entropy List:\n"))
		for _, e := range relationNodeEntropy {
			tmpFileWriter.WriteString(
				fmt.Sprintf("%v %v %v\n", relationNodeEntropyRank[e.Nid], e.Nid, e.E))
		}
		tmpFileWriter.WriteString("\n")

		tmpFileWriter.WriteString(fmt.Sprintf("Entity IDs in Every Wrong Merged Entity Node\n"))
		for nid, l := range entityNodeList {
			flag := true
			for i := 1; i < len(l); i++ {
				if l[i][1] != l[i-1][1] {
					flag = false
					break
				}
			}
			if !flag {
				continue
			}

			str := ""
			for _, id := range l {
				str += fmt.Sprintf("%v ", id)
			}
			str += fmt.Sprintf("%v", nid)
			str += "\n"
			tmpFileWriter.WriteString(str)
		}
		tmpFileWriter.WriteString("\n")

		tmpFileWriter.WriteString(fmt.Sprintf("Relation IDs in Every Wrong Merged Relation Node:\n"))
		for nid, l := range relationNodeList {
			flag := true
			for i := 1; i < len(l); i++ {
				if l[i][1] != l[i-1][1] {
					flag = false
					break
				}
			}
			if !flag {
				continue
			}

			str := ""
			for _, id := range l {
				str += fmt.Sprintf("%v ", id)
			}
			str += fmt.Sprintf("%v", nid)
			str += "\n"
			tmpFileWriter.WriteString(str)
		}
		tmpFileWriter.WriteString("\n")
	}

	// record
	acc_e := 1.0 - float64(wrongEntityCount)/float64(entityNum)
	acc_r := 1.0 - float64(wrongRelationCount)/float64(relationNum)
	mr_e, mr_r, mrr_e, mrr_r := 0.0, 0.0, 0.0, 0.0
	if wrongEntityCount != 0 {
		mr_e = mrSumEntity / float64(wrongEntityCount)
		mrr_e = mrrSumEntity / float64(wrongEntityCount)
	}
	if wrongRelationCount != 0 {
		mr_r = mrSumRelation / float64(wrongRelationCount)
		mrr_r = mrrSumRelation / float64(wrongRelationCount)
	}

	// 统计两两graph之间的配对总数
	entityMatchNum := make(map[[2]string]int)
	relationMatchNum := make(map[[2]string]int)
	for _, l := range entityNodeList {
		for i := range l {
			for j := range l {
				entityMatchNum[[2]string{l[i][0], l[j][0]}]++
			}
		}
	}
	for _, l := range relationNodeList {
		for i := range l {
			for j := range l {
				relationMatchNum[[2]string{l[i][0], l[j][0]}]++
			}
		}
	}
	// 统计平均precision & recall
	entityCqtMatchNumRes := make(map[[2]string]int)
	relationCqtMatchNumRes := make(map[[2]string]int)
	avgPrecision, avgRecall := 0.0, 0.0
	avgPrecisionEntity, avgRecallEntity := 0.0, 0.0
	avgPrecisionRelation, avgRecallRelation := 0.0, 0.0
	for _, nids := range entityNodeList {
		for i := range nids {
			for j := range nids {
				if nids[i][1] == nids[j][1] {
					entityCqtMatchNumRes[[2]string{nids[i][0], nids[j][0]}]++
				}
			}
		}
	}
	for _, nids := range relationNodeList {
		for i := range nids {
			for j := range nids {
				if nids[i][1] == nids[j][1] {
					relationCqtMatchNumRes[[2]string{nids[i][0], nids[j][0]}]++
				}
			}
		}
	}
	for uid1 := range *graphidMap {
		for uid2 := range *graphidMap {
			if uid1 != uid2 {
				precision := getRatio(
					entityCqtMatchNumRes[[2]string{uid1, uid2}]+
						relationCqtMatchNumRes[[2]string{uid1, uid2}],
					entityMatchNum[[2]string{uid1, uid2}]+
						relationMatchNum[[2]string{uid1, uid2}])
				recall := getRatio(
					entityCqtMatchNumRes[[2]string{uid1, uid2}]+
						relationCqtMatchNumRes[[2]string{uid1, uid2}],
					entityCqtMatchNumGT[[2]string{uid1, uid2}]+
						relationCqtMatchNumGT[[2]string{uid1, uid2}])
				precisionEntity := getRatio(
					entityCqtMatchNumRes[[2]string{uid1, uid2}],
					entityMatchNum[[2]string{uid1, uid2}])
				recallEntity := getRatio(
					entityCqtMatchNumRes[[2]string{uid1, uid2}],
					entityCqtMatchNumGT[[2]string{uid1, uid2}])
				precisionRelation := getRatio(
					relationCqtMatchNumRes[[2]string{uid1, uid2}],
					relationMatchNum[[2]string{uid1, uid2}])
				recallRelation := getRatio(
					relationCqtMatchNumRes[[2]string{uid1, uid2}],
					relationCqtMatchNumGT[[2]string{uid1, uid2}])

				avgPrecision += precision
				avgRecall += recall
				avgPrecisionEntity += precisionEntity
				avgRecallEntity += recallEntity
				avgPrecisionRelation += precisionRelation
				avgRecallRelation += recallRelation

				if tmpFlag {
					tmpFileWriter.WriteString(
						fmt.Sprintf("%v %v %v %v %v %v\n",
							entityMatchNum[[2]string{uid1, uid2}],
							entityCqtMatchNumRes[[2]string{uid1, uid2}],
							entityCqtMatchNumGT[[2]string{uid1, uid2}],
							relationMatchNum[[2]string{uid1, uid2}],
							relationCqtMatchNumRes[[2]string{uid1, uid2}],
							relationCqtMatchNumGT[[2]string{uid1, uid2}]))
					tmpFileWriter.WriteString(
						fmt.Sprintf("%.3f %.3f %.3f %.3f %.3f %.3f\n",
							precision, precisionEntity, precisionRelation,
							recall, recallEntity, recallRelation))
					tmpFileWriter.WriteString("\n")
				}
			}
		}
	}
	graphN := len(*graphidMap)
	avgPrecision = avgPrecision / float64(graphN*(graphN-1))
	avgRecall = avgRecall / float64(graphN*(graphN-1))
	avgPrecisionEntity = avgPrecisionEntity / float64(graphN*(graphN-1))
	avgRecallEntity = avgRecallEntity / float64(graphN*(graphN-1))
	avgPrecisionRelation = avgPrecisionRelation / float64(graphN*(graphN-1))
	avgRecallRelation = avgRecallRelation / float64(graphN*(graphN-1))

	// 统计融合节点的“正确率”，即“错误融合节点”的占比，“少了”不算错
	acc := 1.0 - float64(wrongEntityCount+wrongRelationCount)/float64(entityNum+relationNum)
	mr, mrr := 0.0, 0.0
	if wrongEntityCount != 0 || wrongRelationCount != 0 {
		mr = (mrSumEntity + mrSumRelation) / float64(wrongEntityCount+wrongRelationCount)
		mrr = (mrrSumEntity + mrrSumRelation) / float64(wrongEntityCount+wrongRelationCount)
	}

	// 向 tmpFile 打印信息
	tmpFileWriter.WriteString(
		fmt.Sprintf("avg_prec: %.3f, avg_prec_e: %.3f, avg_prec_r: %.3f, avg_rec: %.3f, avg_rec_e: %.3f, avg_rec_r: %.3f\n",
			avgPrecision, avgPrecisionEntity, avgPrecisionRelation, avgRecall, avgRecallEntity, avgRecallRelation))
	tmpFileWriter.WriteString(
		fmt.Sprintf("acc: %.3f, acc_e: %.3f, acc_r %.3f, mr: %.3f, mr_e: %.3f, mr_r: %.3f, mrr: %3.f, mrr_e: %.3f, mrr_r: %.3f, wrong_entity_cnt: %v, entity_num: %v, wrong_relation_cnt: %v, relation_num: %v\n",
			acc, acc_e, acc_r, mr, mr_e, mr_r, mrr, mrr_e, mrr_r, wrongEntityCount, entityNum, wrongRelationCount, relationNum))
	tmpFileWriter.WriteString("\n")
	tmpFileWriter.Flush()

	// 向 实验结果文件 打印信息
	exptRsPerOprFileWriter.WriteString(
		fmt.Sprintf("%v,%v,%.3f,%.3f,%.3f,%.3f,%.3f,\n",
			round, oprTime.Milliseconds(), acc_e, avgPrecisionEntity, avgRecallEntity, mr_e, mrr_e))
	exptRsPerOprFileWriter.Flush()
}

func getRatio(x, y int) float64 {
	if y == 0 {
		return 1.0
	} else {
		return float64(x) / float64(y)
	}
}
