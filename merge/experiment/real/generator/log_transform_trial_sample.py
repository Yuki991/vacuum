#!/usr/bin/env python3
# -*- coding: utf-8 -*- 
import os
import re
import json

admin_operation = ["createProject","createEntityType","createRelationType","createSymbolType","getTypeIntersection","getTypeUnion","addTypeProperty","deleteTypeProperty"]
read_operation = ["getProjects","getGraph","getGlobalRecommend","getGlobalEntityRecommend","getRecommend","exportModel"]
# 所有涉及实例层修改的operation
retain_operation = ["createEntity", "createRelation", "setInstanceProperty", "deleteEntity", "deleteRelation", "citeRecommendInst", "citeEntity", "rejectInst", "renameInst"]
del_operation = ["deleteEntity", "deleteRelation", "rejectInst", "renameInst"]
del_operation_counter = [0, 0, 0, 0]

src_path = "./log/operation.-2021-10-06.log"
project_name = "多学科医学知识图谱"
output_path ="./output.log"
src_folder = "./log/"
output_folder = "./log_filtered/"

def parse_log(log : str):
    # print(log)
    log_pattern = re.compile(r'\[([0-9A-Za-z\-:.]*)\] \[([0-9A-Za-z]*)\] ([0-9A-Za-z]*) - (.*)')
    # log_pattern = re.compile(r'\[([0-9A-Za-z:.\-]*)\]')
    result = re.match(log_pattern, log).groups()
    return result


if __name__ == '__main__':
    l = '[2021-10-26T13:38:49.291] [INFO] default - {"request":{"operation":"createEntity","user_id":151797,"project":"多学科医学知识图谱","entity_type_id":"153398","entity_name":"心律失常","user_name":"0006178148"},"response":{"operation":"createEntity","user_id":151797,"project":"多学科医学知识图谱","error":false,"id":150864,"properties":{},"name":"心律失常"}}'
    pl = json.loads(parse_log(l)[3])
    req = pl["request"]
    resp = pl["response"]
    tar = {
        "user_id": str(req["user_id"]),
        "operation": req["operation"],
        "type_id": str(req["entity_type_id"]),
        "name": req["entity_name"],
        "inst_id": str(resp["id"]),
    }
    print(json.dumps(tar, ensure_ascii=False))

    # l = '[2021-11-05T11:48:21.308] [INFO] default - {"request":{"operation":"createRelation","user_id":"197392","project":"多学科医学知识图谱","relation_type_id":"152540","roles":[{"role":"疾病","inst_id":"152580"},{"role":"症状表现","inst_id":"151035"}],"user_name":""},"response":{"operation":"createRelation","user_id":"197392","project":"多学科医学知识图谱","error":false,"id":151037,"properties":{}}}'
    # pl = json.loads(parse_log(l)[3])
    # req = pl["request"]
    # resp = pl["response"]
    # tar = {
    #     "user_id": str(req["user_id"]),
    #     "operation": req["operation"],
    #     "type_id": str(req["relation_type_id"]),
    #     "roles": [],
    #     "inst_id": str(resp["id"]),
    # }
    # for v in req["roles"]:
    #     tar["roles"].append({"role": v["role"], "inst_id": str(v["inst_id"])})
    # print(json.dumps(tar, ensure_ascii=False))

