#!/usr/bin/env python3
# -*- coding: utf-8 -*- 
import os
import re
import json

admin_operation = ["createProject","createEntityType","createRelationType","createSymbolType","getTypeIntersection","getTypeUnion","addTypeProperty","deleteTypeProperty"]
read_operation = ["getProjects","getGraph","getGlobalRecommend","getGlobalEntityRecommend","getRecommend","exportModel"]
# 所有涉及实例层修改的operation
retain_operation = ["createEntity", "createRelation", "setInstanceProperty", "deleteEntity", "deleteRelation", "citeRecommendInst", "citeEntity", "rejectInst", "renameInst"]
del_operation = ["deleteEntity", "deleteRelation", "rejectInst", "renameInst"]
del_operation_counter = [0, 0, 0, 0]

src_path = "./log/operation.-2021-10-06.log"
project_name = "多学科医学知识图谱"
output_path ="./output.log"
src_folder = "./log/"
output_folder = "./log_filtered/"

def parse_log(log : str):
    # print(log)
    log_pattern = re.compile(r'\[([0-9A-Za-z\-:.]*)\] \[([0-9A-Za-z]*)\] ([0-9A-Za-z]*) - (.*)')
    # log_pattern = re.compile(r'\[([0-9A-Za-z:.\-]*)\]')
    result = re.match(log_pattern, log).groups()
    return result


def parse_logs_from_file(src_path : str, output_path : str):
    file = open(src_path, "r")
    output_file = open(output_path, "w")
    logs = file.readlines()
    for log in logs:
        parse_result = parse_log(log)
        content = json.loads(parse_result[3])
        if content["request"].__contains__("project") and content["request"]["project"] == project_name \
            and content["request"]["operation"] in retain_operation:
            # for i in range(len(del_operation)):
            #     if content["request"]["operation"] == del_operation[i]:
            #         del_operation_counter[i] += 1
            #         print(content["request"])

            # 还要排除出现error的
            print("[{}] [{}] {} - {}".format(parse_result[0], parse_result[1], parse_result[2], parse_result[3]), file = output_file)
    file.close()
    output_file.close()


if __name__ == '__main__':
    for root, dirs, files in os.walk("./log"):
        # print("{}, {}, {}".format(root, dirs, files))
        for file in files:
            src_path = src_folder + file
            output_path = output_folder + file
            # print(src_path)
            parse_logs_from_file(src_path, output_path)
    # print(del_operation_counter)