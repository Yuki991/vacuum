#   该文件从getGraph/createEntity/createRelation/getRecommend/getGlobalEntityRecommend五种操作中获得entity/relation字典

#!/usr/bin/env python3
# -*- coding: utf-8 -*- 
import os
import re
import json

retain_opr_list = ["getGraph", "createEntity", "createRelation", "getRecommend", "getGlobalEntityRecommend"]

# src_path = "./log/operation.-2021-10-06.log"
project_name = "多学科医学知识图谱"
output_path ="./output.log"
src_folder = "./full_log/"
output_folder = "./log_filtered/"

entity_dict = {}
entity_type_dict = {}
relation_dict = {}
relation_type_dict = {}

def parse_log(log : str):
    # print(log)
    log_pattern = re.compile(r'\[([0-9A-Za-z\-:.]*)\] \[([0-9A-Za-z]*)\] ([0-9A-Za-z]*) - (.*)')
    # log_pattern = re.compile(r'\[([0-9A-Za-z:.\-]*)\]')
    result = re.match(log_pattern, log).groups()
    return result


# 遍历所有log中的getGraph/createEntity/createRelation操作，记录所有出现的entity/relation的具体内容
# TODO 处理冲突，现在方法是直接用新的覆盖旧的
def parse_logs_from_file(src_path : str):
    file = open(src_path, "r")
    try:
        logs = file.readlines()
    except Exception as e:
        print("error: {}".format(e))
        return
    for log in logs:
        parse_result = parse_log(log)
        try:
            content = json.loads(parse_result[3])
        except Exception:
            continue
        if content["request"].__contains__("project") and content["request"]["project"] == project_name \
            and content["response"]["error"] == False \
            and content["request"]["operation"] in retain_opr_list:
            # 补全entity_dict和relation_dict
            opr = content["request"]["operation"]
            if opr == "getGraph":
                # entity
                entities = content["response"]["entities"]
                if isinstance(entities, list):
                    for e in entities:
                        id = str(e["id"])
                        entity_dict[id] = e
                elif isinstance(entities, dict):
                    for id, e in entities.items():
                        id = str(id)
                        e["id"] = id
                        entity_dict[id] = e
                else:
                    print("error")
                
                # relation
                relations = content["response"]["relations"]
                if isinstance(relations, list):
                    for r in relations:
                        id = str(r["id"])
                        relation_dict[id] = r
                elif isinstance(relations, dict):
                    for id, r in relations.items():
                        id = str(id)
                        r["id"] = id
                        relation_dict[id] = r
                else:
                    print("error")
                
                # entity_type
                entity_types = content["response"]["entity_types"]
                if isinstance(entity_types, list):
                    for t in entity_types:
                        id = str(t["id"])
                        entity_type_dict[id] = t
                elif isinstance(entity_types, dict):
                    for id, t in entity_types.items():
                        id = str(id)
                        t["id"] = id
                        entity_type_dict[id] = t
                else:
                    print("error")

                # relation_type
                relation_types = content["response"]["relation_types"]
                if isinstance(relation_types, list):
                    for t in relation_types:
                        id = str(t["id"])
                        relation_type_dict[id] = t
                elif isinstance(relation_types, dict):
                    for id, t in relation_types.items():
                        id = str(id)
                        t["id"] = id
                        relation_type_dict[id] = t
                else:
                    print("error")
            elif opr == "getRecommend":
                # entity
                entities = content["response"]["rec_entities"]
                if isinstance(entities, list):
                    for e in entities:
                        t = {
                            "id": str(e["id"]),
                            "type_id": str(e["type_id"]),
                            "name": e["name"],
                            "properties": e["properties"],
                        }
                        entity_dict[t["id"]] = t
                elif isinstance(entities, dict):
                    for id, e in entities.items():
                        t = {
                            "id": str(id),
                            "type_id": str(e["type_id"]),
                            "name": e["name"],
                            "properties": e["properties"],
                        }
                        entity_dict[t["id"]] = t

                # relation
                relations = content["response"]["rec_relations"]
                if isinstance(relations, list):
                    for r in relations:
                        t = {
                            "id": str(r["id"]),
                            "type_id": str(r["type_id"]),
                            "roles": r["roles"],
                            "properties": r["properties"],
                        }
                        relation_dict[t["id"]] = t
                elif isinstance(relations, dict):
                    for id, r in relations.items():
                        t = {
                            "id": str(id),
                            "type_id": str(r["type_id"]),
                            "roles": r["roles"],
                            "properties": r["properties"],
                        }
                        relation_dict[t["id"]] = t
                
                relations = content["response"]["relations"]
                if isinstance(relations, list):
                    for r in relations:
                        t = {
                            "id": str(r["id"]),
                            "type_id": str(r["type_id"]),
                            "roles": r["roles"],
                            "properties": r["properties"],
                        }
                        relation_dict[t["id"]] = t
                elif isinstance(relations, dict):
                    for id, r in relations.items():
                        t = {
                            "id": str(id),
                            "type_id": str(r["type_id"]),
                            "roles": r["roles"],
                            "properties": r["properties"],
                        }
                        relation_dict[t["id"]] = t
            elif opr == "getGlobalEntityRecommend":
                for tid, entities in content["response"]["entities"].items():
                    for e in entities:
                        t = {
                            "id": str(e["id"]),
                            "type_id": str(tid),
                            "name": e["name"],
                            "properties": e["properties"],
                        }
                        entity_dict[t["id"]] = t
            elif opr == "createEntity":
                e = {
                    "id": str(content["response"]["id"]),
                    "type_id": str(content["request"]["entity_type_id"]),
                    "name": content["response"]["name"],
                    "properties": {},
                }
                entity_dict[e["id"]] = e
            elif opr == "createRelation":
                r = {
                    "id": str(content["response"]["id"]),
                    "type_id": str(content["request"]["relation_type_id"]),
                    "roles": content["request"]["roles"],
                    "properties": {},
                }
                relation_dict[r["id"]] = r
    file.close()


if __name__ == '__main__':
    for root, dirs, files in os.walk(src_folder):
        # print("{}, {}, {}".format(root, dirs, files))
        for file in files:
            src_path = src_folder + file
            print(src_path)
            parse_logs_from_file(src_path)
    
    # output entity_dict
    output_file = open(output_folder + "entity_dict.txt", "w")
    for _, e in entity_dict.items():
        print(e, file=output_file)
    output_file.close()

    # output relation dict
    output_file = open(output_folder + "relation_dict.txt", "w")
    for _, r in relation_dict.items():
        print(r, file=output_file)
    output_file.close()

    # output entity type dict
    output_file = open(output_folder + "entity_type_dict.txt", "w")
    for _, e in entity_type_dict.items():
        print(e, file=output_file)
    output_file.close()

    # output relation type dict
    output_file = open(output_folder + "relation_type_dict.txt", "w")
    for _, e in relation_type_dict.items():
        print(e, file=output_file)
    output_file.close()