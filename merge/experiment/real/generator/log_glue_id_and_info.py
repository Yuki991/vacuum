# 该文件将id与具体信息绑定
# 输出格式:
# {
#   "time": xxx,
#   "operation": xxx,
#   "userid": xxx,
#   "data": xxx,
# }

#!/usr/bin/env python3
# -*- coding: utf-8 -*- 
import os
import re
import json

# 是否过滤掉entity数量小于threshold的user
ent_filter_flag = True
ent_filter_threshold = 5

folder = "./log_filtered/"
output_data_folder = "./data/"
entity_dict_path = folder + "entity_dict.txt"
relation_dict_path = folder + "relation_dict.txt"
entity_type_dict_path = folder + "entity_type_dict.txt"
relation_type_dict_path = folder + "relation_type_dict.txt"
original_log_path = folder + "only_create_delete.log"
output_log_path = output_data_folder + "final_create_delete_logs.json"

entity_dict = {}
relation_dict = {}
entity_type_dict = {}
relation_type_dict = {}

output_datas = []

def read_dict(entity_dict_path : str, relation_dict_path : str, entity_type_dict_path : str, relation_type_dict_path : str):
    file = open(entity_dict_path, "r")
    contents = file.readlines()
    for c in contents:
        c = eval(c)
        entity_dict[c["id"]] = c
    file.close()

    file = open(relation_dict_path, "r")
    contents = file.readlines()
    for c in contents:
        c = eval(c)
        relation_dict[c["id"]] = c
    file.close()

    file = open(entity_type_dict_path, "r")
    contents = file.readlines()
    for c in contents:
        c = eval(c)
        entity_type_dict[c["id"]] = c
    file.close()

    file = open(relation_type_dict_path, "r")
    contents = file.readlines()
    for c in contents:
        c = eval(c)
        relation_type_dict[c["id"]] = c
    file.close()


def glue_logs(original_log_path : str, output_log_path : str):
    input_file = open(original_log_path, "r")
    output_file = open(output_log_path, "w")
    user_ids = {}
    ent_opr_cnt = 0
    rel_opr_cnt = 0

    logs = input_file.readlines()
    for log in logs:
        log = log[1:-2].split(",")
        log = [info.strip() for info in log]
        t = log[0]
        opr = log[1]
        user_id = (log[2])
        id = str(log[3])
        c = {}

        # 只保留部分id的数据
        # if user_id not in ["111620", "111669"]:
        #     continue

        if opr in ["createEntity", "citeEntity"]:
            # print("{} {}".format(id, entity_dict[id]))
            try:
                c = entity_dict[id]
                c["type_id"] = str(c["type_id"])
            except Exception as e:
                print("entity_id: {}".format(id))
        elif opr == "createRelation":
            # print("{} {}".format(id, relation_dict[id]))
            try:
                c = relation_dict[id]
                c["type_id"] = str(c["type_id"])
                c["name"] = relation_type_dict[c["type_id"]]["name"]
                for role in c["roles"]:
                    role["inst_id"] = str(role["inst_id"])
            except Exception as e:
                print("relation_id:{}".format(id))
        # print("{", file=output_file, end="")
        # print("\'time\': {}, \'operation\': {}, \'userid\': {}, \'data\': {}".format(t, opr, user_id, c), file=output_file, end="")
        # print("}", file=output_file)
        output_data = {
            "time": t,
            "operation": opr,
            "user_id": user_id,
            "data": c,
        }
        # json_output = json.dumps(output_data, ensure_ascii=False)
        # print(json_output, file=output_file)
        output_datas.append(output_data)

        # 统计数据
        if opr in ["createEntity", "citeEntity"]:
            ent_opr_cnt += 1
        elif opr == "createRelation":
            rel_opr_cnt += 1
        else:
            pass
        if user_id not in user_ids.keys():
            user_ids[user_id] = {"createEntity": 0,"createRelation": 0}
        user_ids[user_id][opr] += 1
    
    # 统计应当出现在最终log中的user_id，以及相互之间的map，从0开始计数
    userid_cnt = 0
    userid_to_seq = {}
    seq_to_userid = {}
    for output_data in output_datas:
        user_id = output_data["user_id"]
        if not ent_filter_flag or user_ids[user_id]["createEntity"] > ent_filter_threshold:
            # 每个userid取第一条操作作为init operation，其余作为incremental operation
            if user_id not in userid_to_seq:
                userid_to_seq[user_id] = str(userid_cnt)
                seq_to_userid[str(userid_cnt)] =  user_id
                tmp_file = open(output_data_folder + f'init_graph{userid_cnt}.json', "w")
                json_output = json.dumps(output_data, ensure_ascii=False)
                print(json_output, file=tmp_file)
                tmp_file.close()
                userid_cnt += 1
            else:
                json_output = json.dumps(output_data, ensure_ascii=False)
                print(json_output, file=output_file)


    input_file.close()
    output_file.close()

    info_output_file = open(folder + "final_logs_info.txt", "w")
    print(f'user_num: {len(user_ids)}')
    print(f'ent_opr_cnt: {ent_opr_cnt}')
    print(f'rel_opr_cnt: {rel_opr_cnt}')
    # print(user_ids)
    print(f'user_num: {len(user_ids)}', file=info_output_file)
    print(f'ent_opr_cnt: {ent_opr_cnt}', file=info_output_file)
    print(f'rel_opr_cnt: {rel_opr_cnt}', file=info_output_file)
    # print(user_ids, file=info_output_file)
    for key in user_ids:
        print(f'{key} {user_ids[key]["createEntity"]} {user_ids[key]["createRelation"]}')
        print(f'{key}, {user_ids[key]["createEntity"]}, {user_ids[key]["createRelation"]}', file=info_output_file)
    info_output_file.close()


if __name__ == '__main__':
    read_dict(entity_dict_path, relation_dict_path, entity_type_dict_path, relation_type_dict_path)
    glue_logs(original_log_path, output_log_path)