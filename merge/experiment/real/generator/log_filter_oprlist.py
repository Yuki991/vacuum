# 处理目标：
#   将所有操作转换为createEntity 和 createRelation两种操作（忽略property修改，忽略rename）
#   假定不同用户生成的同名实体关系都具有相同的properties
# 处理方式：
#   分成两步处理
#   Step1.  先滤出所有createEntity createRelation deleteEntity deleteRelation citeEntity citeRecommendInst操作
#           citeEntity操作转换为createEntity操作
#           TODO citeRecommendInst转换为若干个createEntity和一个createRelation，因为没有高阶关系所以是一个createRelation
#           然后遍历所有的createEntity操作，如果create的entity最终没有被delete，则打印
#           打印格式：{操作时间 操作用户 操作类型 entity_id/relation_id}
#   Step2.  补全Step1中打印的操作序列的内容(上面仅记录了操作对应的entity/relation的id，没有记录id的name type properties等信息)
#           从过滤前的log中的getGraph/createEntity/createRelation三种操作中获得具体信息
#
#   该文件处理step1中打印所有createEntity/createRelation操作
# 
# 问题1.不知道为什么会有实体和关系具有相同的id，可能导致某条关系相关的实体找不到id
# 出现这种问题的关系数目不多，直接忽略这些问题数据
# 

#!/usr/bin/env python3
# -*- coding: utf-8 -*- 
import os
import re
import json

# 所有涉及实例层修改的operation
retain_operation = ["createEntity", "createRelation", "deleteEntity", "deleteRelation", "citeEntity", "citeRecommendInst"]
del_operation = ["deleteEntity", "deleteRelation"]

src_path = "./log/operation.-2021-10-06.log"
project_name = "多学科医学知识图谱"
output_path ="./output.log"
# src_folder = "./log/"
src_folder = "./full_log/"
output_folder = "./log_filtered/"
output_file = open(output_folder + "only_create_delete.log", "w")

opr_list = []

entity_dict_path = "./log_filtered/entity_dict.txt"
relation_dict_path = "./log_filtered/relation_dict.txt"
entity_dict = {}
relation_dict = {}

def read_dict(entity_dict_path : str, relation_dict_path):
    file = open(entity_dict_path, "r")
    contents = file.readlines()
    for c in contents:
        c = eval(c)
        entity_dict[c["id"]] = c
    file.close()

    file = open(relation_dict_path, "r")
    contents = file.readlines()
    for c in contents:
        c = eval(c)
        relation_dict[c["id"]] = c
    file.close()


def parse_log(log : str):
    # print(log)
    log_pattern = re.compile(r'\[([0-9A-Za-z\-:.]*)\] \[([0-9A-Za-z]*)\] ([0-9A-Za-z]*) - (.*)')
    # log_pattern = re.compile(r'\[([0-9A-Za-z:.\-]*)\]')
    result = re.match(log_pattern, log).groups()
    return result


def parse_logs_from_file(src_path : str):
    file = open(src_path, "r")
    logs = file.readlines()
    for log in logs:
        parse_result = parse_log(log)
        content = json.loads(parse_result[3])
        if content["request"].__contains__("project") and content["request"]["project"] == project_name \
            and content["request"]["operation"] in retain_operation:
            # 还要排除出现error的
            # print("[{}] [{}] {} - {}".format(parse_result[0], parse_result[1], parse_result[2], parse_result[3]), file = output_file)
            if content["response"]["error"] == False:
                t = parse_result[0]
                opr = content["request"]["operation"]
                user_id = str(content["request"]["user_id"])
                inst_id = ""
                if opr in ["createEntity", "createRelation"]:
                    inst_id = str(content["response"]["id"])
                elif opr in ["deleteEntity"]:
                    inst_id = str(content["request"]["entity_id"])
                elif opr in ["deleteRelation"]:
                    inst_id = str(content["request"]["relation_id"])
                elif opr in ["citeEntity"]:
                    inst_id = str(content["request"]["entity_id"])
                
                if opr == "citeRecommendInst":
                    inst_id = str(content["request"]["inst_id"])
                    # 若干个createEntity
                    for id in content["response"]["inst_ids"]:
                        id = str(id)
                        if inst_id != id:
                            opr_list.append([t, "createEntity", user_id, id])
                    # 一个createRelation
                    opr_list.append([t, "createRelation", user_id, inst_id])
                else:
                    opr_list.append([t, opr, user_id, inst_id])
    file.close()


if __name__ == '__main__':
    # 读入entity dict和relation dict
    read_dict(entity_dict_path, relation_dict_path)

    for root, dirs, files in os.walk(src_folder):
        # print("{}, {}, {}".format(root, dirs, files))
        for file in files:
            if file == '.DS_Store':
                continue
            src_path = src_folder + file
            print(src_path)
            parse_logs_from_file(src_path)
    
    # 所有操作按时间排序
    opr_list.sort(key=lambda x : x[0])

    # 打印调试
    tmp_output_file = open(output_folder + "tmp_filtered_oprlist.log", "w")
    for i in range(len(opr_list)):
        t = opr_list[i][0]
        opr = opr_list[i][1]
        user_id = opr_list[i][2]
        inst_id = opr_list[i][3]
        # [操作时间 操作类型 操作用户 目标id]
        print("[{}, {}, {}, {}]".format(t, opr, user_id, inst_id), file=tmp_output_file)
    tmp_output_file.close()

    # 过滤被create之后delete的操作
    mp = {}
    for i in range(len(opr_list)):
        t = opr_list[i][0]
        opr = opr_list[i][1]
        user_id = str(opr_list[i][2])
        inst_id = str(opr_list[i][3])

        if user_id not in mp.keys():
            mp[user_id] = {}
        if opr in del_operation:
            mp[user_id][inst_id] = -1
        else:
            mp[user_id][inst_id] = i
    
    # 打印
    for i in range(len(opr_list)):
        t = opr_list[i][0]
        opr = opr_list[i][1]
        user_id = str(opr_list[i][2])
        inst_id = str(opr_list[i][3])

        if opr not in del_operation:
            # 1. 检查createEntity操作的新建实体是否之后被删除，如果是则忽略该操作
            if mp[user_id][inst_id] != i:
                continue
            # 2. 检查createRelation操作的新建关系是否之后被删除，以及该关系相关的实体是否已经被创建，如果被删除/实体未创建则忽略该操作
            if opr == "createRelation":
                flag = False
                content = relation_dict[inst_id]
                for role in content["roles"]:
                    try:
                        tid = mp[user_id][str(role["inst_id"])]
                    except Exception as e:
                        flag = True
                        break
                    if tid == -1 or tid > i:
                        flag = True
                        break
                if flag:
                    continue

            if opr == "citeEntity":
                opr = "createEntity"
            
            # [操作时间 操作类型 操作用户 目标id]
            print("[{}, {}, {}, {}]".format(t, opr, user_id, inst_id), file=output_file)
    
    output_file.close()