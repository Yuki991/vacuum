package main

import (
	"flag"
	"iomgm/experiment/synthetic"
	"path/filepath"

	"github.com/go-ini/ini"
)

// `er` or `ba`
var mode string

func init() {
	flag.StringVar(&mode, "mode", "er", "synthetic graph generating mode, please use `er` or `ba`")
	flag.Parse()
}

func openConfigFile(path string) *ini.File {
	configs, err := ini.Load(path)
	if err != nil {
		panic("synthetic_data_generation_config read error")
	}
	return configs
}

func main() {
	printPath := filepath.FromSlash("../data/synthetic")
	commonConfigs := openConfigFile(filepath.FromSlash("../config/synthetic_data_generation_common_config.ini"))
	var graphGenerateConfigs *ini.File
	switch mode {
	case "er":
		graphGenerateConfigs = openConfigFile(filepath.FromSlash("../config/synthetic_data_generation_config_er.ini"))
	case "ba":
		graphGenerateConfigs = openConfigFile(filepath.FromSlash("../config/synthetic_data_generation_config_ba.ini"))
	}
	synthetic.GenerateSyntheticData(commonConfigs, graphGenerateConfigs, printPath)
}
