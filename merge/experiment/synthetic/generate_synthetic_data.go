package synthetic

import (
	"bufio"
	"fmt"
	"iomgm/experiment/common"
	"iomgm/experiment/synthetic/generator"
	"os"
	"path/filepath"

	"github.com/go-ini/ini"
)

func GenerateSyntheticData(commonConfigs *ini.File, graphGenerataConfigs *ini.File, printPath string) {
	// 生成一个母图
	var generatingGraph *generator.VanillaGraph
	{
		param := graphGenerataConfigs.Section("graph_generation_param")
		graphGenerationMode := param.Key("GraphGenerationFunc").Value()
		switch graphGenerationMode {
		case "ER": // 以ER法生成母图
			// read parameters
			nodeNum, _ := param.Key("NodeNum").Int()
			edgeProbability, _ := param.Key("EdgeProbability").Float64()
			nodeTypeNum, _ := param.Key("NodeTypeNum").Int()
			edgeTypeNum, _ := param.Key("EdgeTypeNum").Int()
			// generate graph
			generatingGraph = generator.GenerateGraphER(nodeNum, edgeProbability, nodeTypeNum, edgeTypeNum)
		case "BA": // 以BA法生成母图
			// read parameters
			initNodeNum, _ := param.Key("InitNodeNum").Int()
			initEdgeNum, _ := param.Key("InitEdgeNum").Int()
			finalNodeNum, _ := param.Key("FinalNodeNum").Int()
			newEdgeNumPerNode, _ := param.Key("NewEdgeNumPerNode").Int()
			nodeTypeNum, _ := param.Key("NodeTypeNum").Int()
			edgeTypeNum, _ := param.Key("EdgeTypeNum").Int()
			// generate graph
			generatingGraph = generator.GenerateGraphBA(initNodeNum, initEdgeNum, finalNodeNum, newEdgeNumPerNode, nodeTypeNum, edgeTypeNum)
		default:
			panic("unrecognized graph generation mode, please use `ER` or `BA`")
		}
	}

	// 生成一系列变异图
	var variedGraphs []*generator.VanillaGraph
	{
		// read parameters
		param := commonConfigs.Section("varied_graph_generation_param")
		variedGraphNum, _ := param.Key("VariedGraphNum").Int()
		hopNum, _ := param.Key("HopNum").Int()
		edgeDropoutFlag, _ := param.Key("EdgeDropoutFlag").Bool()
		edgeDropout, _ := param.Key("EdgeDropout").Float64()
		// generate varied graphs
		variedGraphGenerator := generator.NewVariedGraphGenerator(hopNum, edgeDropout, edgeDropoutFlag)
		variedGraphs = variedGraphGenerator.CreateVariedGraphs(generatingGraph, variedGraphNum)
	}

	// 打印仿真数据（母图 & 一系列变异图）
	printSyntheticGraphs(generatingGraph, variedGraphs, printPath)

	// 生成操作序列
	createFinalSyntheticData(generatingGraph, variedGraphs, printPath, commonConfigs)
}

// @Description: 根据输入的graphs，生成一系列初始图以及一个操作序列
func createFinalSyntheticData(generatingGraph *generator.VanillaGraph, variedGraphs []*generator.VanillaGraph, filePath string, cfgs *ini.File) {
	// TODO 目前没有将generatingGraph也作为输入数据
	// 1. 输入的graph的50%节点相应的诱导子图作为初始图，剩余节点和边用于生成操作序列
	// 2. 每个graph相应地有一个操作序列，需要将这些操作序列合并为一个操作序列，做法是”轮询“即从每个操作序列中轮流取一个操作

	filePath = fmt.Sprintf("%v/processed_data", filePath)
	fileNameOfOperationsForInitGraph := cfgs.Section("file_name_param").Key("FileNameOfOperationsForInitGraph").String()
	fileNameOfIncrementalOperationsOfGraph := cfgs.Section("file_name_param").Key("FileNameOfIncrementalOperationsOfGraph").String()
	fileNameOfFinalIncrementOperation := cfgs.Section("file_name_param").Key("FileNameOfFinalIncrementOperation").String()

	// 清除filePath下所有文件
	filePath = filepath.FromSlash(filePath)
	_ = os.RemoveAll(filePath)
	_ = os.Mkdir(filePath, 0755)

	// TODO ratio改为由参数控制
	// 选取一定比例的节点，其诱导子图作为初始图，剩余节点和边以增量操作的方式给出，生成方法基于BFS
	ratio := 0.5
	// 一系列初始图
	variedInitGraphs := make([]*generator.VanillaGraph, len(variedGraphs))
	// 每个初始图相应的生成操作序列
	oprsOfInitGraphs := make([][]*common.OperationData, len(variedGraphs))
	// 每个变异图的后续增量操作序列
	oprs := make([][]*common.OperationData, len(variedGraphs))
	// 为每个变异图生成初始图和操作序列
	for i, g := range variedGraphs {
		variedInitGraphs[i], oprsOfInitGraphs[i], oprs[i] = createInitGraphAndOperations(g, ratio, fmt.Sprintf("%v", i))
	}

	// 打印生成的初始图、初始图的生成操作序列以及后续的增量操作序列
	for i := 0; i < len(variedGraphs); i++ {
		printSingleSyntheticGraph(variedInitGraphs[i], fmt.Sprintf("%v/varied_init_graph%v.txt", filePath, i))
		printSyntheticOperationData(oprsOfInitGraphs[i], fmt.Sprintf("%v/%v%v.json", filePath, fileNameOfOperationsForInitGraph, i))
		printSyntheticOperationData(oprs[i], fmt.Sprintf("%v/%v%v.json", filePath, fileNameOfIncrementalOperationsOfGraph, i))
	}

	// 将各个操作序列合并为一个操作序列
	finalOprs := mergeListOfOperationData(oprs)
	// 打印最终的操作序列
	printSyntheticOperationData(finalOprs, fmt.Sprintf("%v/%v.json", filePath, fileNameOfFinalIncrementOperation))
}

// @Description: 根据输入的graph生成一个初始图以及一个增量操作序列，初始图是输入图保留一定比例的节点后的诱导子图，增量操作中的UserID由参数指定，操作序列保证添加边时相关节点已经在图中
// @Param ratio: 保留节点的比例
// @Param userid: 指定增量操作的userid
// @Return $0: 生成的初始图
// @Return $1: 生成初始图的操作序列
// @Return $2: 后续增量操作序列
func createInitGraphAndOperations(g *generator.VanillaGraph, ratio float64, userid string) (*generator.VanillaGraph, []*common.OperationData, []*common.OperationData) {
	// 生成init graph
	reserveNum := int(ratio * float64(g.NodeNum))
	initGraph := generator.NewVanillaGraph(reserveNum)
	// 生成init graph的操作序列
	oprsOfInitGraph := make([]*common.OperationData, 0)
	// 增量操作序列
	oprs := make([]*common.OperationData, 0)

	// 向操作序列加入一个添加节点的增量操作
	addCreateEntityOperation := func(node *generator.Node, oprs *[]*common.OperationData) {
		*oprs = append(*oprs, &common.OperationData{
			OprTime: fmt.Sprintf("%v", len(*oprs)),
			OprType: common.OprCreateEntity,
			UserID:  userid,
			Element: common.ElementData{
				ID:   fmt.Sprintf("%v", node.ID),
				Type: node.Type,
				Name: node.Name,
			},
		})
	}
	// 向操作序列中加入一个添加边的增量操作
	addCreateRelationOperation := func(edge *generator.Edge, oprs *[]*common.OperationData) {
		*oprs = append(*oprs, &common.OperationData{
			OprTime: fmt.Sprintf("%v", len(*oprs)),
			OprType: common.OprCreateRelation,
			UserID:  userid,
			Element: common.ElementData{
				ID:   fmt.Sprintf("%v", edge.ID),
				Type: edge.Type,
				Roles: []common.Role{
					{RoleName: "s", ID: fmt.Sprintf("%v", edge.Source)},
					{RoleName: "t", ID: fmt.Sprintf("%v", edge.Target)},
				},
			},
		})
	}

	{ // 生成诱导子图
		// add node
		cnt := reserveNum
		for _, node := range g.Nodes {
			initGraph.AddNode(node.ID, node.Name, node.Type)
			addCreateEntityOperation(node, &oprsOfInitGraph)
			if cnt--; cnt == 0 {
				break
			}
		}
		// add edge
		for _, edge := range g.Edges {
			if initGraph.HasNode(edge.Source) && initGraph.HasNode(edge.Target) {
				initGraph.AddEdge(edge.ID, edge.Type, edge.Source, edge.Target)
				addCreateRelationOperation(edge, &oprsOfInitGraph)
			}
		}
	}

	{ // 通过BFS生成操作序列
		// bfs队列
		queue := make([]int, 0, initGraph.NodeNum)
		// 记录哪些节点在队列中
		mp := make(map[int]bool)
		// 将init graph所有节点放入队列
		for nid := range initGraph.Nodes {
			queue = append(queue, nid)
			mp[nid] = true
		}

		// BFS
		for i := 0; i < len(queue); i++ {
			nid := queue[i]
			// 遍历inedge
			for _, eid := range g.InEdges[nid] {
				edge := g.Edges[eid]
				if _, ok := mp[edge.Source]; !ok {
					// source节点未遍历，添加到bfs队列以及增量操作序列中
					queue = append(queue, edge.Source)
					mp[edge.Source] = true
					addCreateEntityOperation(g.Nodes[edge.Source], &oprs)
				}
				// 判断是否该边是否需要添加增量操作序列中
				if !initGraph.HasEdge(eid) { // 若该边不在init graph中，则需要加入到增量操作序列中
					if edge.Source < nid { // 防止边被添加两次
						addCreateRelationOperation(edge, &oprs)
					}
				}
			}
			// 遍历outedge
			for _, eid := range g.OutEdges[nid] {
				edge := g.Edges[eid]
				if _, ok := mp[edge.Target]; !ok {
					// target节点未遍历，添加到bfs队列以及增量操作序列中
					queue = append(queue, edge.Target)
					mp[edge.Target] = true
					addCreateEntityOperation(g.Nodes[edge.Target], &oprs)
				}
				// 判断是否该边是否需要添加增量操作序列中
				if !initGraph.HasEdge(eid) { // 若该边不在init graph中，则需要加入到增量操作序列中
					if edge.Target < nid { // 防止边被添加两次
						addCreateRelationOperation(edge, &oprs)
					}
				}
			}
		}
	}

	return initGraph, oprsOfInitGraph, oprs
}

// @Description: 将多个增量操作序列合并为一个增量操作序列，OprTime会被更改
func mergeListOfOperationData(oprs [][]*common.OperationData) []*common.OperationData {
	idx := make([]int, len(oprs))
	result := make([]*common.OperationData, 0)
	// 通过Round Robin合并序列
	for {
		flag := false
		for i := 0; i < len(oprs); i++ {
			if idx[i] < len(oprs[i]) {
				// 修改OprTime
				opr := oprs[i][idx[i]]
				opr.OprTime = fmt.Sprintf("%v", len(result))
				// append
				result = append(result, opr)
				idx[i]++
				flag = true
			}
		}
		if !flag {
			break
		}
	}
	return result
}

// @Description: 打印所有生成的仿真图
func printSyntheticGraphs(generatingGraph *generator.VanillaGraph, variedGraphs []*generator.VanillaGraph, filePath string) {
	filePath = fmt.Sprintf("%v/raw_data", filePath)
	filePath = filepath.FromSlash(filePath)
	// 清除filePath下所有文件
	_ = os.RemoveAll(filePath)
	_ = os.Mkdir(filePath, 0755)
	// 打印generatingGraph
	printSingleSyntheticGraph(generatingGraph, fmt.Sprintf("%v/generating_graph.txt", filePath))
	// 打印variedGraphs中的每一个graph
	for i := range variedGraphs {
		printSingleSyntheticGraph(variedGraphs[i], fmt.Sprintf("%v/varied_graph%v.txt", filePath, i))
	}
	// 统计并打印信息
	printSyntheticGraphsInfo(generatingGraph, variedGraphs, fmt.Sprintf("%v/matching_info.txt", filePath))
}

// @Description: 打印一个仿真图
func printSingleSyntheticGraph(g *generator.VanillaGraph, path string) {
	// open file
	path = filepath.FromSlash(path)
	f, err := os.Create(path)
	if err != nil {
		panic(err)
	}
	defer f.Close()

	w := bufio.NewWriter(f)

	w.WriteString(fmt.Sprintf("node_num: %v\n\n", g.NodeNum))
	w.WriteString(fmt.Sprintf("edge_num: %v\n\n", g.EdgeNum))

	w.WriteString("nodes_id:\n")
	for _, node := range g.Nodes {
		w.WriteString(fmt.Sprintf("%v ", node.ID))
	}
	w.WriteString("\n")

	w.WriteString("edges:\n")
	for _, edge := range g.Edges {
		w.WriteString(fmt.Sprintf("%v %v\n", edge.Source, edge.Target))
	}
	w.WriteString("\n")

	w.Flush()
}

// @Description: 统计并打印generatingGraph中的每个节点属于variedGraphs中的哪些graphs
func printSyntheticGraphsInfo(generatingGraph *generator.VanillaGraph, variedGraphs []*generator.VanillaGraph, path string) {
	// open file
	path = filepath.FromSlash(path)
	f, err := os.Create(path)
	if err != nil {
		panic(err)
	}
	defer f.Close()

	w := bufio.NewWriter(f)

	w.WriteString(fmt.Sprintf("node_num: %v\n\n", generatingGraph.NodeNum))
	w.WriteString(fmt.Sprintf("graphs_num: %v\n\n", len(variedGraphs)))

	w.WriteString("matching:\n")
	for _, node := range generatingGraph.Nodes {
		w.WriteString(fmt.Sprintf("%v: ", node.ID))
		for i, g := range variedGraphs {
			if _, ok := g.Nodes[node.ID]; ok {
				w.WriteString(fmt.Sprintf("%v ", i))
			}
		}
		w.WriteString("\n")
	}
	w.WriteString("\n")

	w.Flush()
}

// @Description: 打印一个操作序列
func printSyntheticOperationData(oprs []*common.OperationData, path string) {
	// open file
	path = filepath.FromSlash(path)
	f, err := os.Create(path)
	if err != nil {
		panic(err)
	}
	defer f.Close()

	w := bufio.NewWriter(f)

	common.WriteOperationData(oprs, w)
}
