package generator

type Node struct {
	ID   int
	Name string
	Type string
}

type Edge struct {
	ID     int
	Type   string
	Source int
	Target int
}

type VanillaGraph struct {
	NodeNum int
	EdgeNum int
	Nodes   map[int]*Node
	Edges   map[int]*Edge
	// OutEdges[i]存储所有从id为i的节点出发的边的ID
	OutEdges map[int][]int
	// InEdges[i]存储所有目标是id为i的节点的边的ID
	InEdges map[int][]int
}

func NewVanillaGraph(nodeNum int) *VanillaGraph {
	return &VanillaGraph{
		NodeNum:  nodeNum,
		EdgeNum:  0,
		Nodes:    make(map[int]*Node),
		Edges:    make(map[int]*Edge),
		OutEdges: make(map[int][]int),
		InEdges:  make(map[int][]int),
	}
}

func (g *VanillaGraph) HasNode(id int) bool {
	_, ok := g.Nodes[id]
	return ok
}

func (g *VanillaGraph) HasEdge(id int) bool {
	_, ok := g.Edges[id]
	return ok
}

func (g *VanillaGraph) AddNode(id int, name string, tp string) {
	g.Nodes[id] = &Node{
		ID:   id,
		Name: name,
		Type: tp,
	}
}

func (g *VanillaGraph) AddEdge(id int, tp string, source int, target int) {
	g.Edges[id] = &Edge{
		ID:     id,
		Type:   tp,
		Source: source,
		Target: target,
	}
	g.OutEdges[source] = append(g.OutEdges[source], id)
	g.InEdges[target] = append(g.InEdges[target], id)
	g.EdgeNum++
}
