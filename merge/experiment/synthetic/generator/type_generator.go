/* type generator，用于生成node type和edge type
 */

package generator

import "math/rand"

type TypeGenerator struct {
	// type的数目
	TypeNum int
	// 生成的Type
	Types []string
}

func NewTypeGenerator(typeNum int) *TypeGenerator {
	g := &TypeGenerator{
		TypeNum: typeNum,
		Types:   make([]string, typeNum),
	}

	for i := 0; i < typeNum; i++ {
		g.Types[i] = num2String(i)
	}

	return g
}

func (g *TypeGenerator) CreateType() string {
	r := rand.Int()
	return g.Types[r%g.TypeNum]
}

func num2String(i int) string {
	str := ""

	if i == 0 {
		str = "0"
	} else {
		for i > 0 {
			str += string(byte(i%10 + 48))
			i /= 10
		}
	}

	return str
}
