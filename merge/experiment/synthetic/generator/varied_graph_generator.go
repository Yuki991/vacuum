package generator

import (
	"math/rand"
)

type VariedGraphGenerator struct {
	// 用于控制几跳邻居作为新的graph
	NeighborHopNum int
	// 随机删除某条边的概率
	EdgeDropout float64
	// 是否启用edge dropout
	EdgeDropoutFlag bool
}

func NewVariedGraphGenerator(hopNum int, edgeDropout float64, edgeDropoutFlag bool) *VariedGraphGenerator {
	return &VariedGraphGenerator{
		NeighborHopNum:  hopNum,
		EdgeDropout:     edgeDropout,
		EdgeDropoutFlag: edgeDropoutFlag,
	}
}

// @Description: 根据输入的graph，以删边/删点的方式生成一系列“变异图“，node type和edge type不变，node name会以一定概率改变
// @Param g: 输入的graph
// @Param variedGraphNum: 拟生成的graphs的数目
func (vgg *VariedGraphGenerator) CreateVariedGraphs(g *VanillaGraph, variedGraphNum int) []*VanillaGraph {
	variedGraphs := make([]*VanillaGraph, 0, variedGraphNum)
	for i := 0; i < variedGraphNum; i++ {
		// 生成varied graph
		variedGraph := createVariedGraphViaNeighbor(g, vgg.NeighborHopNum, vgg.EdgeDropoutFlag, vgg.EdgeDropout)
		variedGraphs = append(variedGraphs, variedGraph)
	}
	return variedGraphs
}

// @Description: 生成一个“变异图”，随机选择g中的一个节点，以其hopNum跳邻居作为生成的varied graph，node name会随机“变异”
// @Param hopNum: 邻居跳数
func createVariedGraphViaNeighbor(g *VanillaGraph, hopNum int, dropoutFlag bool, dropout float64) *VanillaGraph {
	// 随机选择g中的一个node
	central := rand.Intn(len(g.Nodes))

	// 通过宽度优先搜索（BFS）找hopNum跳邻居
	// 用于bfs的队列
	queue := make([]int, 0)
	// 用于记录哪些节点进行被加入到队列当中
	mp := make(map[int]bool)
	// 用于记录节点与中心节点的距离
	dis := make(map[int]int)
	// 初始化
	queue = append(queue, central)
	mp[central] = true
	dis[central] = 0
	// BFS
	for i := 0; i < len(queue); i++ {
		nid := queue[i]
		if dis[nid] == hopNum {
			continue
		}

		// 遍历inedge
		for _, eid := range g.InEdges[nid] {
			nid2 := g.Edges[eid].Source
			if _, ok := mp[nid2]; !ok {
				mp[nid2] = true
				dis[nid2] = dis[nid] + 1
				queue = append(queue, nid2)
			}
		}

		// 遍历outedge
		for _, eid := range g.OutEdges[nid] {
			nid2 := g.Edges[eid].Target
			if _, ok := mp[nid2]; !ok {
				mp[nid2] = true
				dis[nid2] = dis[nid] + 1
				queue = append(queue, nid2)
			}
		}
	}

	// 根据宽度优先搜索的结果生成诱导子图作为最终结果
	variedGraph := NewVanillaGraph(len(queue))
	// 初始化node信息
	for _, nid := range queue {
		node := g.Nodes[nid]
		// id和type不变，name以一定概率进行“变异”
		variedGraph.AddNode(nid, VaryString(node.Name), node.Type)
	}
	// 添加诱导子图的边
	for _, edge := range g.Edges {
		if variedGraph.HasNode(edge.Source) && variedGraph.HasNode(edge.Target) {
			// 判断是否dropout
			if dropoutFlag && rand.Float64() < dropout {
				continue
			}

			variedGraph.AddEdge(edge.ID, edge.Type, edge.Source, edge.Target)
		}
	}

	return variedGraph
}
