/* 用于生成随机字符串
 */

package generator

import (
	"math"
	"math/rand"
)

// 字母字典
var Alphabet []byte

var (
	// 字典大小，要求在区间[1,62]中
	alphabetSize int = 10
	// 字符串初始长度
	maxStringLength int = 5
	// 添加一个字符的概率
	addThreshold float64 = 0.005
	// 某个字符被删除的概率
	delThreshold float64 = 0.005
	// 某个字符被修改的概率
	modifyThreshold float64 = 0.01
)

type StringGenerator struct {
	// 最大String长度
	MaxStringLength int
	// 最大不同String的数目
	MaxStringNum int64
	// 是否允许重复的String
	AllowDup bool
	// 用于判断生成的String是否已经存在
	StringMap map[int64]bool
}

func init() {
	// 初始化字母字典：a~z，A～Z，0～9
	// a~z
	for i := 0; i < 26; i++ {
		Alphabet = append(Alphabet, byte(97+i))
	}
	// A~Z
	for i := 0; i < 26; i++ {
		Alphabet = append(Alphabet, byte(65+i))
	}
	// 0~9
	for i := 0; i < 10; i++ {
		Alphabet = append(Alphabet, byte(48+i))
	}
	Alphabet = Alphabet[0:alphabetSize]
}

// @Description: 字符串生成器
// @Param allowDup: 是否允许生成重复字符串
// @Param maxStringLength: 字符串最大长度
func NewStringGenerator(allowDup bool) *StringGenerator {
	ng := &StringGenerator{
		MaxStringLength: maxStringLength,
		MaxStringNum:    int64(math.Pow(float64(alphabetSize), float64(maxStringLength))),
		AllowDup:        allowDup,
		StringMap:       make(map[int64]bool),
	}
	return ng
}

func (g *StringGenerator) CreateNewString() string {
	var r int64
	r = rand.Int63n(g.MaxStringNum)
	if !g.AllowDup {
		for {
			if _, ok := g.StringMap[r]; !ok {
				break
			}
			r = rand.Int63n(g.MaxStringNum)
		}
	}

	g.StringMap[r] = true
	return num2StringWithAlphabet(r)
}

func num2StringWithAlphabet(i int64) string {
	str := ""
	if i == 0 {
		str += string(Alphabet[0])
	} else {
		for i > 0 {
			str += string(Alphabet[i%int64(alphabetSize)])
			i /= int64(alphabetSize)
		}
	}
	return str
}

// @Description: 随机增加/删除/修改输入字符串的字符，以生成一个“变异”的字符串
func VaryString(s string) string {
	t := ""
	for i := range s {
		r := rand.Float64()
		if r <= addThreshold {
			j := rand.Intn(alphabetSize)
			t += string(Alphabet[j])
		}

		r = rand.Float64()
		if r <= delThreshold {
			continue
		} else if r <= delThreshold+modifyThreshold {
			j := rand.Intn(alphabetSize)
			t += string(Alphabet[j])
		} else {
			t += string(s[i])
		}
	}
	return t
}
