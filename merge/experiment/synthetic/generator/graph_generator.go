/* 生成一个随机图，提供ER和BA两种方式
 * 没有将边看作是节点，后续需要另行做格式转换
 */

package generator

import "math/rand"

// @Description: ER法生成随机有向，边从序号小的指向序号大的。保证不会有重边、自环。
// @Param n: 节点数目
// @Param p: 边出现的概率
// @Param nodeTypeNum: 节点类型数目
// @Param edgeTypeNum: 边类型数目
func GenerateGraphER(n int, p float64, nodeTypeNum int, edgeTypeNum int) *VanillaGraph {
	nodeNameGenerator := NewNameGenerator(false)
	nodeTypeGenerator := NewTypeGenerator(nodeTypeNum)
	edgeTypeGenerator := NewTypeGenerator(edgeTypeNum)

	g := NewVanillaGraph(n)

	// 初始化节点信息
	for i := 0; i < n; i++ {
		g.AddNode(i, nodeNameGenerator.CreateNewName(), nodeTypeGenerator.CreateType())
	}

	for i := 0; i < n-1; i++ {
		// 保证连通
		g.AddEdge(g.EdgeNum, edgeTypeGenerator.CreateType(), i, i+1)

		for j := i + 2; j < n; j++ {
			if rand.Float64() < p {
				g.AddEdge(g.EdgeNum, edgeTypeGenerator.CreateType(), i, j)
			}
		}
	}

	return g
}

// @Description: BA法生成随机图，边从序号小的指向序号大的
// @Param n0: 初始节点数目
// @Param m0: 初始边数目，要求n0 - 1 <= m0 <= n0 * (n0 - 1) / 2
// @Param n: 最终节点数目
// @Param q: 要求q <= n0，最终边数目 = m0 + q * (n - n0)
// @Param nodeTypeNum: 节点类型数目
// @Param edgeTypeNum: 边类型数目
func GenerateGraphBA(n0 int, m0 int, n int, q int, nodeTypeNum int, edgeTypeNum int) *VanillaGraph {
	nodeNameGenerator := NewNameGenerator(false)
	nodeTypeGenerator := NewTypeGenerator(nodeTypeNum)
	edgeTypeGenerator := NewTypeGenerator(edgeTypeNum)

	g := NewVanillaGraph(n)

	// 初始化节点信息
	for i := 0; i < n; i++ {
		g.AddNode(i, nodeNameGenerator.CreateNewName(), nodeTypeGenerator.CreateType())
	}

	// 初始化节点数为n0，边数为m0的图
	{
		// 先生成一条链
		for i := 1; i < n0; i++ {
			g.AddEdge(g.EdgeNum, edgeTypeGenerator.CreateType(), i-1, i)
		}
		// 尽量加边直到边的数目等于m0
		for i := 1; i < n0; i++ {
			// j从0到i-1，保证不会有重边
			for j := 0; j < i-1; j++ {
				if g.EdgeNum == m0 {
					break
				}

				g.AddEdge(g.EdgeNum, edgeTypeGenerator.CreateType(), j, i)
			}
		}
	}

	// 节点的度degree
	deg := make([]int, n)
	for _, edge := range g.Edges {
		deg[edge.Source]++
		deg[edge.Target]++
	}

	// 序列为n0+1 ~ n的节点分别有q条边，按照 deg(i)/sum(deg(i)) 的概率选择，具体参考BA生成图的方法
	for i := n0; i < n; i++ {
		cnt := q
		degSum := float64(2 * g.EdgeNum)
		for j := 0; j < i; j++ {
			if i-j == cnt || rand.Float64() < float64(deg[j])/degSum {
				g.AddEdge(g.EdgeNum, edgeTypeGenerator.CreateType(), j, i)
				deg[j]++
				deg[i] = q
				cnt--
			}
		}
	}

	return g
}
