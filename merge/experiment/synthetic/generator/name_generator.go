/* name generator，用于生成node name
 */

package generator

type NameGenerator struct {
	Generator *StringGenerator
}

func NewNameGenerator(allowDup bool) *NameGenerator {
	return &NameGenerator{
		Generator: NewStringGenerator(allowDup),
	}
}

func (g *NameGenerator) CreateNewName() string {
	return g.Generator.CreateNewString()
}
