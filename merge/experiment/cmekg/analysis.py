#! /usr/bin/python

import matplotlib.pyplot as plt

if __name__ == '__main__':
    file = open('./data/totalrelation.txt', 'r', encoding='utf-8')
    
    mp = {}
    lines = file.readlines()
    for i in range(len(lines)):
        r = lines[i].split(" ")
        if r[0] in mp.keys():
            mp[r[0]] += 1
        else:
            mp[r[0]] = 1
        
        if r[3] in mp.keys():
            mp[r[3]] += 1
        else:
            mp[r[3]] = 1
    
    cnt = []
    for k, v in mp.items():
        if v > 1 and v <=300:
            cnt.append(v)
    plt.hist(cnt, bins=200)
    plt.title("Test")
    plt.xlabel("Value")
    plt.ylabel("Frequency")
    plt.show()
    