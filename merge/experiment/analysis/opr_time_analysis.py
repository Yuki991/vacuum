# -*- coding:utf-8 -*-

import os
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from pathlib import Path
from matplotlib.font_manager import FontProperties

# 中文显示
if os.name == 'posix':
    mpl.rcParams['font.sans-serif'] = ['Heiti TC']
elif os.name == 'nt':
    mpl.rcParams['font.sans-serif'] = ['SimHei']
else:
    print("unknown os")

# 图片大小
image_dpi = 500

def single_expr():
    opr_time_path = Path('../experiment_result/operation_time.txt')
    intv = 100
    round = []
    res = []
    maxx = 0
    maxy = 0

    with open(opr_time_path, 'r') as f:
        for line in f.readlines():
            el = line.split(',')
            el = [int(num) for num in el[:-1]]
            round.append(el[0])
            res.append(el[1] / 1000)
            if round[-1] > maxx:
                maxx = round[-1]
            if res[-1] > maxy:
                maxy = res[-1]
    
    label_fontsize = 10
    maxx = (maxx//500 + 1) * 500
    major_locator_y = 0
    minor_locator_y = 0
    if maxy <= 10:
        major_locator_y = 2
    elif maxy <= 50:
        major_locator_y = 5
    else:
        major_locator_y = 20
    minor_locator_y = major_locator_y / 10
    maxy = (maxy//major_locator_y + 1) * major_locator_y

    plt.xlabel("增量操作执行次数", fontsize=label_fontsize)
    plt.ylabel("运行时间(s)", fontsize=label_fontsize)
    plt.axis([0, maxx, 0, maxy])
    # plt.grid()
    ax = plt.gca()
    ax.xaxis.set_major_locator(plt.MultipleLocator(2000))
    ax.xaxis.set_minor_locator(plt.MultipleLocator(500))
    # ax.yaxis.set_major_locator(plt.MultipleLocator(major_locator_y))
    # ax.yaxis.set_minor_locator(plt.MultipleLocator(minor_locator_y))
    ax.yaxis.set_major_locator(plt.MultipleLocator(1.0))
    ax.yaxis.set_minor_locator(plt.MultipleLocator(0.1))
    ax.grid(which='major', axis='y')
    plt.plot(round, res)
    # plt.show()
    plt.savefig('./runtime.jpg', dpi=image_dpi)


def multi_expr():
    expr_num = 4
    expr_names = ['graph_num_20', 'graph_num_30', 'graph_num_50', 'graph_num_100']
    # path = './'
    path = './expr_2500_5_03/'

    intv = 100
    rounds = []
    results = []
    
    for i in range(0, expr_num):
        # expr_info_path = Path(f'./experiment_info{i}.txt')
        # expr_info = {}
        # with open(expr_info_path, 'r') as f:
        #     for line in f.readlines():
        #         el = line.split(',')
        #         expr_info[el[0]] = [int(num) for num in el[1:-1]]

        opr_time_path = Path(path + f'operation_time{i}.txt')
        round = [0]
        # result = [expr_info['build_time'][0] / 1000]
        result = [0]
        maxx = 0
        maxy = 0
        with open(opr_time_path, 'r') as f:
            for line in f.readlines():
                el = line.split(',')
                el = [int(num) for num in el[:-1]]
                round.append(el[0])
                # result.append((el[1] + expr_info['build_time'][0]) / 1000)
                result.append(el[1] / 1000)
                if round[-1] > maxx:
                    maxx = round[-1]
                if result[-1] > maxy:
                    maxy = result[-1]
        rounds.append(round)
        results.append(result)

    label_fontsize = 10
    maxx = (maxx//500 + 1) * 500
    major_locator_y = 0
    minor_locator_y = 0
    if maxy <= 10:
        major_locator_y = 2
    elif maxy <= 50:
        major_locator_y = 5
    else:
        major_locator_y = 20
    minor_locator_y = major_locator_y / 5
    maxy = (maxy//major_locator_y + 1) * major_locator_y

    plt.xlabel("增量操作执行次数", fontsize=label_fontsize)
    plt.ylabel("运行时间(s)", fontsize=label_fontsize)
    # plt.axis([0, maxx, 0, maxy])
    plt.axis([0, maxx, 0, 50])
    ax = plt.gca()
    ax.xaxis.set_major_locator(plt.MultipleLocator(2000))
    ax.xaxis.set_minor_locator(plt.MultipleLocator(500))
    # ax.yaxis.set_major_locator(plt.MultipleLocator(major_locator_y))
    # ax.yaxis.set_minor_locator(plt.MultipleLocator(minor_locator_y))
    ax.yaxis.set_major_locator(plt.MultipleLocator(10))
    ax.yaxis.set_minor_locator(plt.MultipleLocator(2))
    ax.grid(which='major', axis='y')
    for i in range(0, expr_num):
        plt.plot(rounds[i], results[i], label=expr_names[i])
    plt.legend(loc='upper left')
    plt.savefig('./runtime.jpg', dpi=image_dpi)


if __name__ == '__main__':
    single_expr()
    # multi_expr()