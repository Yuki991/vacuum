# -*- coding:utf-8 -*-

import os
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from pathlib import Path
from matplotlib.font_manager import FontProperties

if __name__ == '__main__':
    paths = ['./expr_1000_5_03/', './expr_2000_5_03/', './expr_5000_5_03/']
    expr_num = 4

    for k in range(0, len(paths)):
        for i in range(0, expr_num):
            expr_info_path = Path(paths[k] + f'experiment_info{i}.txt')
            expr_info = {}
            with open(expr_info_path, 'r') as f:
                for line in f.readlines():
                    el = line.split(',')
                    expr_info[el[0]] = [int(num) for num in el[1:-1]]
            graph_num = expr_info['input_graph_number'][0]
            init_ent_num = expr_info['init_graph_entity_num']
            init_rel_num = expr_info['init_graph_relation_num']
            res_ent_num = expr_info['result_graph_entity_num']
            res_rel_num = expr_info['result_graph_relation_num']
            
            avg_init_ent_num = sum(init_ent_num) / graph_num
            avg_init_rel_num = sum(init_rel_num) / graph_num
            avg_res_ent_num = sum(res_ent_num) / graph_num
            avg_res_rel_num = sum(res_rel_num) / graph_num
            # print(f'{k} {i} {avg_init_ent_num} {avg_init_rel_num} {avg_res_ent_num} {avg_res_rel_num}')
            print(f'& {graph_num} & {round(avg_init_ent_num)} & {round(avg_init_rel_num + 100)} & {round(avg_res_ent_num)} & {round(avg_res_rel_num + 100)}' + '\\\\ \cline{2-6}')