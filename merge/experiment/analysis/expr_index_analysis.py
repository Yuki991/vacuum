# -*- coding:utf-8 -*-

import os
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from pathlib import Path
from matplotlib.font_manager import FontProperties

# 中文显示
if os.name == 'posix':
    mpl.rcParams['font.sans-serif'] = ['Heiti TC']
elif os.name == 'nt':
    mpl.rcParams['font.sans-serif'] = ['SimHei']
else:
    print("unknown os")

# 图片大小
image_dpi = 500

def single_expr():
    expr_info_path = Path('../experiment_result/experiment_info.txt')
    expr_result_path = Path('../experiment_result/experiment_result_per_operation.txt')
    expr_info = {}
    expr_result = {}
    with open(expr_info_path, 'r') as f:
        for line in f.readlines():
            el = line.split(',')
            expr_info[el[0]] = [int(num) for num in el[1:-1]]
    with open(expr_result_path, 'r') as f:
        title = f.readline().split(',')[:-1]
        for t in title:
            expr_result[t] = []
        for line in f.readlines():
            nums = line.split(',')[:-1]
            for i in range(0, len(nums)):
                expr_result[title[i]].append(float(nums[i]))
    interval = expr_info['record_interval'][0]
    total_opr_num = expr_info['total_operation_number'][0]
    round = expr_result['round']
    accuracy = expr_result['accuracy']
    average_precision = expr_result['average_precision']
    average_recall = expr_result['average_recall']
    mean_rank = expr_result['mean_rank']

    label_fontsize = 10
    maxx = (round[-1]//500 + 1) * 500
    
    plt.cla()
    plt.xlabel("增量操作执行次数", fontsize=label_fontsize)
    plt.ylabel("精确率", fontsize=label_fontsize)
    plt.axis([0, maxx, 0.8, 1.01])
    # plt.grid(axis='y')
    ax = plt.gca()
    ax.xaxis.set_major_locator(plt.MultipleLocator(2000))
    ax.xaxis.set_minor_locator(plt.MultipleLocator(500))
    ax.yaxis.set_major_locator(plt.MultipleLocator(0.05))
    ax.yaxis.set_minor_locator(plt.MultipleLocator(0.01))
    ax.grid(which='major', axis='y')
    plt.plot(round, average_precision)
    plt.savefig('./precision.jpg', dpi=image_dpi)

    plt.cla()
    plt.xlabel("增量操作执行次数", fontsize=label_fontsize)
    plt.ylabel("召回率", fontsize=label_fontsize)
    plt.axis([0, maxx, 0.8, 1.01])
    # plt.grid(axis='y')
    ax = plt.gca()
    ax.xaxis.set_major_locator(plt.MultipleLocator(2000))
    ax.xaxis.set_minor_locator(plt.MultipleLocator(500))
    ax.yaxis.set_major_locator(plt.MultipleLocator(0.05))
    ax.yaxis.set_minor_locator(plt.MultipleLocator(0.01))
    ax.grid(which='major', axis='y')
    plt.plot(round, average_recall)
    plt.savefig('./recall.jpg', dpi=image_dpi)

    plt.cla()
    plt.xlabel("增量操作执行次数", fontsize=label_fontsize)
    plt.ylabel("熵值平均排名", fontsize=label_fontsize)
    plt.axis([0, maxx, 0, 80])
    ax = plt.gca()
    ax.xaxis.set_major_locator(plt.MultipleLocator(2000))
    ax.xaxis.set_minor_locator(plt.MultipleLocator(500))
    ax.yaxis.set_major_locator(plt.MultipleLocator(20))
    ax.yaxis.set_minor_locator(plt.MultipleLocator(5))
    ax.grid(which='major', axis='y')
    plt.plot(round, mean_rank)
    plt.savefig('./mr.jpg', dpi=image_dpi)

    plt.cla()
    plt.xlabel("增量操作执行次数", fontsize=label_fontsize)
    plt.ylabel("准确率", fontsize=label_fontsize)
    plt.axis([0, maxx, 0.8, 1.01])
    ax = plt.gca()
    ax.xaxis.set_major_locator(plt.MultipleLocator(2000))
    ax.xaxis.set_minor_locator(plt.MultipleLocator(500))
    ax.yaxis.set_major_locator(plt.MultipleLocator(0.05))
    ax.yaxis.set_minor_locator(plt.MultipleLocator(0.01))
    ax.grid(which='major', axis='y')
    plt.plot(round, accuracy)
    plt.savefig('./accuracy.jpg', dpi=image_dpi)


def multi_expr():
    expr_num = 4
    expr_names = ['graph_num_20', 'graph_num_30', 'graph_num_50', 'graph_num_100']
    # path = './'
    path = './expr_5000_5_03/'

    intervals = []
    total_opr_nums = []
    rounds = []
    accuracys = []
    average_precisions = []
    average_recalls = []
    mean_ranks = []

    for i in range(0, expr_num):
        expr_info_path = Path(path + f'experiment_info{i}.txt')
        expr_result_path = Path(path + f'./experiment_result_per_operation{i}.txt')
        expr_info = {}
        expr_result = {}
        with open(expr_info_path, 'r') as f:
            for line in f.readlines():
                el = line.split(',')
                expr_info[el[0]] = [int(num) for num in el[1:-1]]
        with open(expr_result_path, 'r') as f:
            title = f.readline().split(',')[:-1]
            for t in title:
                expr_result[t] = []
            for line in f.readlines():
                nums = line.split(',')[:-1]
                for i in range(0, len(nums)):
                    expr_result[title[i]].append(float(nums[i]))
        interval = expr_info['record_interval'][0]
        total_opr_num = expr_info['total_operation_number'][0]
        round = expr_result['round']
        accuracy = expr_result['accuracy']
        average_precision = expr_result['average_precision']
        average_recall = expr_result['average_recall']
        mean_rank = expr_result['mean_rank']
        intervals.append(interval)
        total_opr_nums.append(total_opr_num)
        rounds.append(round)
        accuracys.append(accuracy)
        average_precisions.append(average_precision)
        average_recalls.append(average_recall)
        mean_ranks.append(mean_rank)

    # for i in range(0, expr_num):
    #     for k in range(0, len(average_precisions[i])):
    #         average_precisions[i][k] -= 0.03

    label_fontsize = 10
    maxx = (rounds[0][-1]//500 + 1) * 500
    
    plt.cla()
    plt.xlabel("增量操作执行次数", fontsize=label_fontsize)
    plt.ylabel("精确率", fontsize=label_fontsize)
    plt.axis([0, maxx, 0.5, 1.0])
    # plt.grid(axis='y')
    ax = plt.gca()
    ax.xaxis.set_major_locator(plt.MultipleLocator(2000))
    ax.xaxis.set_minor_locator(plt.MultipleLocator(500))
    ax.yaxis.set_major_locator(plt.MultipleLocator(0.1))
    ax.yaxis.set_minor_locator(plt.MultipleLocator(0.02))
    ax.grid(which='major', axis='y')
    for i in range(0, expr_num):
        plt.plot(rounds[i], average_precisions[i], label=expr_names[i])
    plt.legend()
    plt.savefig('./precision.jpg', dpi=image_dpi)

    plt.cla()
    plt.xlabel("增量操作执行次数", fontsize=label_fontsize)
    plt.ylabel("召回率", fontsize=label_fontsize)
    plt.axis([0, maxx, 0.5, 1.0])
    # plt.grid(axis='y')
    ax = plt.gca()
    ax.xaxis.set_major_locator(plt.MultipleLocator(2000))
    ax.xaxis.set_minor_locator(plt.MultipleLocator(500))
    ax.yaxis.set_major_locator(plt.MultipleLocator(0.1))
    ax.yaxis.set_minor_locator(plt.MultipleLocator(0.02))
    ax.grid(which='major', axis='y')
    for i in range(0, expr_num):
        plt.plot(rounds[i], average_recalls[i], label=expr_names[i])
    plt.legend()
    plt.savefig('./recall.jpg', dpi=image_dpi)

    plt.cla()
    plt.xlabel("增量操作执行次数", fontsize=label_fontsize)
    plt.ylabel("熵值平均排名", fontsize=label_fontsize)
    plt.axis([0, maxx, 0, 200])
    ax = plt.gca()
    ax.xaxis.set_major_locator(plt.MultipleLocator(2000))
    ax.xaxis.set_minor_locator(plt.MultipleLocator(500))
    ax.yaxis.set_major_locator(plt.MultipleLocator(20))
    ax.yaxis.set_minor_locator(plt.MultipleLocator(5))
    ax.grid(which='major', axis='y')
    for i in range(0, expr_num):
        plt.plot(rounds[i], mean_ranks[i], label=expr_names[i])
    plt.legend()
    plt.savefig('./mr.jpg', dpi=image_dpi)

    plt.cla()
    plt.xlabel("增量操作执行次数", fontsize=label_fontsize)
    plt.ylabel("准确率", fontsize=label_fontsize)
    plt.axis([0, maxx, 0.5, 1.0])
    ax = plt.gca()
    ax.xaxis.set_major_locator(plt.MultipleLocator(2000))
    ax.xaxis.set_minor_locator(plt.MultipleLocator(500))
    ax.yaxis.set_major_locator(plt.MultipleLocator(0.1))
    ax.yaxis.set_minor_locator(plt.MultipleLocator(0.02))
    ax.grid(which='major', axis='y')
    for i in range(0, expr_num):
        plt.plot(rounds[i], accuracys[i], label=expr_names[i])
    plt.legend()
    plt.savefig('./accuracy.jpg', dpi=image_dpi)


if __name__ == '__main__':
    single_expr()
    # multi_expr()
