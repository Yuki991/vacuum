# -*- coding:utf-8 -*-

import os
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from pathlib import Path
from matplotlib.font_manager import FontProperties

# 中文显示
if os.name == 'posix':
    mpl.rcParams['font.sans-serif'] = ['Heiti TC']
elif os.name == 'nt':
    mpl.rcParams['font.sans-serif'] = ['SimHei']
else:
    print("unknown os")

if __name__ == '__main__':
    expr_info_path = Path('../experiment_result/experiment_info.txt')
    expr_result_path = Path('../experiment_result/experiment_result_per_operation.txt')
    expr_info = {}
    expr_result = {}

    with open(expr_info_path, 'r') as f:
        for line in f.readlines():
            el = line.split(',')
            print(el)
            expr_info[el[0]] = [int(num) for num in el[1:-1]]
    print(expr_info)
    
    with open(expr_result_path, 'r') as f:
        title = f.readline().split(',')[:-1]
        for t in title:
            expr_result[t] = []
        
        for line in f.readlines():
            nums = line.split(',')[:-1]
            print(nums)
            for i in range(0, len(nums)):
                expr_result[title[i]].append(float(nums[i]))
    print(expr_result)

    interval = expr_info['record_interval'][0]
    total_opr_num = expr_info['total_operation_number'][0]
    round = expr_result['round']
    accuracy = expr_result['accuracy']
    average_precision = expr_result['average_precision']
    average_recall = expr_result['average_recall']
    opr_time = expr_result['operation_time']

    labelsize = 10
    plt.figure(figsize=(10, 8))

    plt.subplot(2, 2, 1)
    # plt.title("准确率")
    plt.xlabel("运行轮次", fontsize=labelsize)
    plt.ylabel("准确率", fontsize=labelsize)
    plt.axis([0, (total_opr_num//interval + 1)*interval, 0.8, 1.01])
    plt.grid()
    plt.plot(round, accuracy)
    
    plt.subplot(2, 2, 2)
    # plt.title("平均精确率")
    plt.xlabel("运行轮次", fontsize=labelsize)
    plt.ylabel("平均精确率", fontsize=labelsize)
    plt.axis([0, (total_opr_num//interval + 1)*interval, 0.8, 1.01])
    plt.grid()
    plt.plot(round, average_precision)
    
    plt.subplot(2, 2, 3)
    # plt.title("平均召回率")
    plt.xlabel("运行轮次", fontsize=labelsize)
    plt.ylabel("平均召回率", fontsize=labelsize)
    plt.axis([0, (total_opr_num//interval + 1)*interval, 0.8, 1.01])
    plt.grid()
    plt.plot(round, average_recall)
    
    # 现在统计的应该是若干轮操作的总运行时间
    # TODO 自动调整坐标轴区间
    plt.subplot(2, 2, 4)
    # plt.title("运行时间")
    plt.xlabel("运行轮次", fontsize=labelsize)
    plt.ylabel("运行时间", fontsize=labelsize)
    plt.axis([0, (total_opr_num//interval + 1)*interval, 0, 200])
    plt.grid()
    plt.plot(round, opr_time)

    plt.show()

    # sample2
    # interval = expr_info['record_interval'][0]
    # total_opr_num = expr_info['total_operation_number'][0]
    # round = expr_result['round']
    # average_precision = expr_result['average_precision']
    # average_recall = expr_result['average_recall']
    # plt.axis([0, (total_opr_num//interval + 1)*interval, 0.8, 1.01])
    # plt.plot(round, average_precision)
    # plt.plot(round, average_recall)
    # plt.show()

    # sample1
    # values = expr_result['average_recall']
    # labels = range(0, len(values))
    # # 调整坐标轴范围，前两个是x轴的范围，后两个是y轴的范围
    # plt.axis([0, len(values), 0.8, 1.0])
    # # 作图
    # plt.plot(labels, values)
    # # 展示
    # plt.show()
