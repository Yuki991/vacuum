package main

import (
	"flag"
	"iomgm/utils"
	"path/filepath"
)

// `synthetic` or `real`
var mode string

func init() {
	flag.StringVar(&mode, "mode", "synthetic", "experiment mode, please use `synthetic` or `real`")
	flag.Parse()

	// 初始化utils
	utils.InitParam(mode)
}

func main() {
	switch mode {
	case "synthetic":
		experimentConfigPath := filepath.FromSlash("./config/synthetic_experiment_config.ini")
		StartExperiment(experimentConfigPath)
	case "real":
		experimentConfigPath := filepath.FromSlash("./config/real_data_experiment_config.ini")
		StartExperiment(experimentConfigPath)
	default:
		panic("please use `synthetic` or `real` experiment mode")
	}
}
